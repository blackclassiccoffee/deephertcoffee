var gulp = require('gulp');
var browser = require('browser-sync').create();

gulp.task('serve', function() {
    browser.init({
        proxy: 'superdistri.dev',
        port: 4000,
    });

    gulp.watch([
            'application/views/**/*.php',
            'application/controllers/**/*.php'
        ])
        .on('change', browser.reload);
});

gulp.task('default', ['serve']);
