<?php

class m_login extends CI_Model{

	public function toUpdateUser($username, $data){
		$d = $this->db->where('email', $username);
		$d = $this->db->update('users', $data);
		return $d;
	}

	public function cekDataLogin($username, $password){
		$this->db->select('UL.*, TL.id as lab_id, TL.nama_lab, TP.PositionName');
		$this->db->from('users UL');
		$this->db->join('t_karyawan TK', 'UL.karyawan_id = TK.id', 'LEFT');
		$this->db->join('t_position TP', 'TK.IDPosition = TP.id', 'LEFT');
		$this->db->join('t_lab TL', 'TK.lab_id = TL.id', 'LEFT');
		$this->db->where('UL.email', $username);
		$q = $this->db->get();

		if($q->num_rows() > 0){
			if (password_verify($password, $q->row()->password)) {
				return $q->result();
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	
}

?>