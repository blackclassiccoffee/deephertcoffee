<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class m_order extends CI_Model {
	
	public function getLaporanOrder($filter, $limit, $page, $session) {
		
		if($page != 1) {
			$offset = (($page-1) * $limit);
		} else {
			$offset = $page-1;
		}
		
		if($limit != -1) {
			$this->db->limit($limit, $offset);
		}
		
		$this->db->order_by('TR.created_at', 'DESC');
		$this->db->select('TR.*, U.Name, UD.Name as nama_driver, TC.nama_customer, TC.alamat');
		$this->db->from('t_order as TR')
			->join('t_customer TC', 'TR.id_customer = TC.id', 'LEFT')
			->join('users U', 'TR.id_user_maker = U.id', 'LEFT')
			->join('users UD', 'TR.id_driver = UD.id', 'LEFT');
		
		if (isset($filter['customer']) && $filter['customer'] != "") {
			$this->db->where('TC.nama_customer LIKE', '%'.$filter['customer'].'%');
		}
		
		if (isset($filter['status']) && $filter['status'] != "") {
			$this->db->where('TR.status', $filter['status']);
		}
		
		
		if (isset($filter['tanggal_awal']) && $filter['tanggal_awal'] != "") {
			if (isset($filter['tanggal_akhir']) && $filter['tanggal_akhir'] != "") {
				$this->db->where('DATE(TR.created_at) >=', $filter['tanggal_awal']);
			} else {
				$this->db->where('DATE(TR.created_at)', $filter['tanggal_awal']);
			}
		}
		
		if (isset($filter['tanggal_akhir']) && $filter['tanggal_akhir'] != "") {
			$this->db->where('DATE(TR.created_at) <=', $filter['tanggal_akhir']);
		}
		
		$q = $this->db->get();

		return $q->result();
	}
	
	public function record_count($filter) {
		
		$this->db->select('COUNT(*) AS `numrows`');
		$this->db->from('t_order as TR')
			->join('t_customer TC', 'TR.id_customer = TC.id', 'LEFT')
			->join('users U', 'TR.id_user_maker = U.id', 'LEFT')
			->join('users UD', 'TR.id_driver = UD.id', 'LEFT');
		
		if (isset($filter['customer']) && $filter['customer'] != "") {
			$this->db->where('TC.nama_customer LIKE', '%'.$filter['customer'].'%');
		}
		
		if (isset($filter['status']) && $filter['status'] != "") {
			$this->db->where('TR.status', $filter['status']);
		}
		
		
		if (isset($filter['tanggal_awal']) && $filter['tanggal_awal'] != "") {
			if (isset($filter['tanggal_akhir']) && $filter['tanggal_akhir'] != "") {
				$this->db->where('DATE(TR.created_at) >=', $filter['tanggal_awal']);
			} else {
				$this->db->where('DATE(TR.created_at)', $filter['tanggal_awal']);
			}
		}
		
		if (isset($filter['tanggal_akhir']) && $filter['tanggal_akhir'] != "") {
			$this->db->where('DATE(TR.created_at) <=', $filter['tanggal_akhir']);
		}
		
       	return $this->db->get()->row();
    }
	
	public function autoOrderNumber($kantor, $id_perusahaan) {
		
		$this->db->from('t_order');
		$this->db->select('count(*) as total');
		$this->db->where('kantor', $kantor);
		$this->db->where('id_perusahaan', $id_perusahaan);
		$maxOrder = $this->db->get()->row();
		
		$n = $maxOrder->total;
		$n2 = str_pad($n + 1, 5, 0, STR_PAD_LEFT);
		return $kantor.$id_perusahaan.$n2;
    }
	
	public function getTrack($id_user, $id_bongkar) {
		$this->db->select('*');
		$this->db->from('t_users_location_history');
		$this->db->where('id_user', $id_user);
		$this->db->where('id_bongkar', $id_bongkar);
		
		$q = $this->db->get();

		return $q->result();
	}
	
}

?>