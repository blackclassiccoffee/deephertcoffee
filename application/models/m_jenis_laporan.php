<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class m_jenis_laporan extends CI_Model {
	
	public function getData($filter, $limit, $page) {
		
		if($page != 1) {
			$offset = (($page-1) * $limit);
		} else {
			$offset = $page-1;
		}
		
		if($limit != -1) {
			$this->db->limit($limit, $offset);
		}
		
		$this->db->order_by('TL.created_at', 'DESC');
		$this->db->select('TL.*');
		$this->db->from('t_jenis_laporan as TL');
		
		if (isset($filter['jenis_laporan']) && $filter['jenis_laporan'] != "") {
			$this->db->where('TL.jenis_laporan LIKE', '%'.$filter['jenis_laporan'].'%');
		}
		
		if (isset($filter['tanggal_awal']) && $filter['tanggal_awal'] != "") {
			if (isset($filter['tanggal_akhir']) && $filter['tanggal_akhir'] != "") {
				$this->db->where('DATE(TL.created_at) >=', $filter['tanggal_awal']);
			} else {
				$this->db->where('DATE(TL.created_at)', $filter['tanggal_awal']);
			}
		}
		
		if (isset($filter['tanggal_akhir']) && $filter['tanggal_akhir'] != "") {
			$this->db->where('DATE(TL.created_at) <=', $filter['tanggal_akhir']);
		}
		
		$q = $this->db->get();

		return $q->result();
	}
	
	public function record_count($filter) {
		
		$this->db->select('COUNT(*) AS `numrows`');
		$this->db->from('t_jenis_laporan as TL');
		
		if (isset($filter['jenis_laporan']) && $filter['jenis_laporan'] != "") {
			$this->db->where('TL.jenis_laporan LIKE', '%'.$filter['jenis_laporan'].'%');
		}
		
		if (isset($filter['tanggal_awal']) && $filter['tanggal_awal'] != "") {
			if (isset($filter['tanggal_akhir']) && $filter['tanggal_akhir'] != "") {
				$this->db->where('DATE(TL.created_at) >=', $filter['tanggal_awal']);
			} else {
				$this->db->where('DATE(TL.created_at)', $filter['tanggal_awal']);
			}
		}
		
		if (isset($filter['tanggal_akhir']) && $filter['tanggal_akhir'] != "") {
			$this->db->where('DATE(TL.created_at) <=', $filter['tanggal_akhir']);
		}
		
       	return $this->db->get()->row();
    }
	
}

?>