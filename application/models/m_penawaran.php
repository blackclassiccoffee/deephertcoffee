<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class m_penawaran extends CI_Model {

	public function getPenawaran($filter, $limit, $page, $data_session) {
		
		if($page != 1) {
			$offset = (($page-1) * $limit);
		} else {
			$offset = $page-1;
		}
		
		if($limit != -1) {
			$this->db->limit($limit, $offset);
		}
		
		$this->db->order_by('OP.created_at', 'DESC');
		$this->db->select('
			OP.*,
			TP.nama_pelanggan,
			TP.alamat_pelanggan,
			TK.nama_karyawan,
			TL.nama_lab
		');
		$this->db->from('t_order_pelanggan as OP');
		$this->db->join('t_pelanggan as TP', 'OP.pelanggan_id = TP.id');
		$this->db->join('t_karyawan as TK', 'OP.karyawan_id = TK.id');
		$this->db->join('t_lab as TL', 'OP.lab_id = TL.id');
		$this->db->where('OP.karyawan_id', $data_session['karyawan_id']);
		
		if (isset($filter['no_penawaran_src']) && $filter['no_penawaran_src'] != "") {
			$this->db->where('OP.no_penawaran LIKE', '%'.$filter['no_penawaran_src'].'%');
		}
		
		if (isset($filter['pelanggan_src']) && $filter['pelanggan_src'] != "") {
			$this->db->where('TP.nama_pelanggan LIKE', '%'.$filter['pelanggan_src'].'%');
		}
		
		if (isset($filter['status_proved']) && $filter['status_proved'] != "") {
			$this->db->where('TL.status_proved', $filter['status_proved']);
		}
		
		
		if (isset($filter['tanggal_awal']) && $filter['tanggal_awal'] != "") {
			if (isset($filter['tanggal_akhir']) && $filter['tanggal_akhir'] != "") {
				$this->db->where('DATE(OP.tanggal_penawaran) >=', $filter['tanggal_awal']);
			} else {
				$this->db->where('DATE(OP.tanggal_penawaran)', $filter['tanggal_awal']);
			}
		}
		
		if (isset($filter['tanggal_akhir']) && $filter['tanggal_akhir'] != "") {
			$this->db->where('DATE(OP.tanggal_penawaran) <=', $filter['tanggal_akhir']);
		}
		
		$q = $this->db->get();

		return $q->result();
	}
	
	public function record_count($filter, $data_session) {
		
		$this->db->select('COUNT(*) AS `numrows`');
		$this->db->select('
			OP.*,
			TP.nama_pelanggan,
			TP.alamat_pelanggan,
			TK.nama_karyawan,
			TL.nama_lab
		');
		$this->db->from('t_order_pelanggan as OP');
		$this->db->join('t_pelanggan as TP', 'OP.pelanggan_id = TP.id');
		$this->db->join('t_karyawan as TK', 'OP.karyawan_id = TK.id');
		$this->db->join('t_lab as TL', 'OP.lab_id = TL.id');
		$this->db->where('OP.karyawan_id', $data_session['karyawan_id']);
		
		if (isset($filter['no_penawaran_src']) && $filter['no_penawaran_src'] != "") {
			$this->db->where('OP.no_penawaran LIKE', '%'.$filter['no_penawaran_src'].'%');
		}
		
		if (isset($filter['pelanggan_src']) && $filter['pelanggan_src'] != "") {
			$this->db->where('TP.nama_pelanggan LIKE', '%'.$filter['pelanggan_src'].'%');
		}
		
		if (isset($filter['status_proved']) && $filter['status_proved'] != "") {
			$this->db->where('TL.status_proved', $filter['status_proved']);
		}
		
		
		if (isset($filter['tanggal_awal']) && $filter['tanggal_awal'] != "") {
			if (isset($filter['tanggal_akhir']) && $filter['tanggal_akhir'] != "") {
				$this->db->where('DATE(OP.tanggal_penawaran) >=', $filter['tanggal_awal']);
			} else {
				$this->db->where('DATE(OP.tanggal_penawaran)', $filter['tanggal_awal']);
			}
		}
		
		if (isset($filter['tanggal_akhir']) && $filter['tanggal_akhir'] != "") {
			$this->db->where('DATE(OP.tanggal_penawaran) <=', $filter['tanggal_akhir']);
		}
		
       	return $this->db->get()->row();
    }

}

/* End of file m_toko.php */
/* Location: ./application/models/m_toko.php */
