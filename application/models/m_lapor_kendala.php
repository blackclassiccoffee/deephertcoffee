<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class m_lapor_kendala extends CI_Model {

	public function getLaporKendala($filter, $limit, $page) {
		
		if($page != 1) {
			$offset = (($page-1) * $limit);
		} else {
			$offset = $page-1;
		}
		
		if($limit != -1) {
			$this->db->limit($limit, $offset);
		}
		
		$this->db->order_by('TL.status_proved, TL.created_at', 'DESC');
		$this->db->select('TL.*, TJ.jenis_laporan, U.Name, U.id as id_user, UP.Name as NameProved, TA.plat_nomor');
		$this->db->from('t_laporan_kendala as TL')
			->join('t_jenis_laporan TJ', 'TL.id_jenis_laporan = TJ.id', 'LEFT')
			->join('users U', 'TL.id_user = U.id')
			->join('users UP', 'TL.id_user_proved = UP.id', 'LEFT')
			->join('t_armada TA', 'TL.id_user = TA.id_user');
		
		if (isset($filter['pelapor']) && $filter['pelapor'] != "") {
			$this->db->where('U.Name LIKE', '%'.$filter['pelapor'].'%');
		}
		
		if (isset($filter['nopol']) && $filter['nopol'] != "") {
			$this->db->where('TA.plat_nomor LIKE', '%'.$filter['nopol'].'%');
		}
		
		if (isset($filter['jenis_laporan']) && $filter['jenis_laporan'] != "") {
			$this->db->where('TL.id_jenis_laporan', $filter['jenis_laporan']);
		}
		
		if (isset($filter['status_proved']) && $filter['status_proved'] != "") {
			$this->db->where('TL.status_proved', $filter['status_proved']);
		}
		
		
		if (isset($filter['tanggal_awal']) && $filter['tanggal_awal'] != "") {
			if (isset($filter['tanggal_akhir']) && $filter['tanggal_akhir'] != "") {
				$this->db->where('DATE(TL.created_at) >=', $filter['tanggal_awal']);
			} else {
				$this->db->where('DATE(TL.created_at)', $filter['tanggal_awal']);
			}
		}
		
		if (isset($filter['tanggal_akhir']) && $filter['tanggal_akhir'] != "") {
			$this->db->where('DATE(TL.created_at) <=', $filter['tanggal_akhir']);
		}
		
		$q = $this->db->get();

		return $q->result();
	}
	
	public function record_count($filter) {
		
		$this->db->select('COUNT(*) AS `numrows`');
		$this->db->from('t_laporan_kendala as TL')
			->join('t_jenis_laporan TJ', 'TL.id_jenis_laporan = TJ.id', 'LEFT')
			->join('users U', 'TL.id_user = U.id')
			->join('users UP', 'TL.id_user_proved = UP.id', 'LEFT')
			->join('t_armada TA', 'TL.id_user = TA.id_user');
		
		if (isset($filter['pelapor']) && $filter['pelapor'] != "") {
			$this->db->where('U.Name LIKE', '%'.$filter['pelapor'].'%');
		}
		
		if (isset($filter['nopol']) && $filter['nopol'] != "") {
			$this->db->where('TA.plat_nomor LIKE', '%'.$filter['nopol'].'%');
		}
		
		if (isset($filter['jenis_laporan']) && $filter['jenis_laporan'] != "") {
			$this->db->where('TL.id_jenis_laporan', $filter['jenis_laporan']);
		}
		
		if (isset($filter['status_proved']) && $filter['status_proved'] != "") {
			$this->db->where('TL.status_proved', $filter['status_proved']);
		}
		
		
		if (isset($filter['tanggal_awal']) && $filter['tanggal_awal'] != "") {
			if (isset($filter['tanggal_akhir']) && $filter['tanggal_akhir'] != "") {
				$this->db->where('DATE(TL.created_at) >=', $filter['tanggal_awal']);
			} else {
				$this->db->where('DATE(TL.created_at)', $filter['tanggal_awal']);
			}
		}
		
		if (isset($filter['tanggal_akhir']) && $filter['tanggal_akhir'] != "") {
			$this->db->where('DATE(TL.created_at) <=', $filter['tanggal_akhir']);
		}
		
       	return $this->db->get()->row();
    }

}

/* End of file m_toko.php */
/* Location: ./application/models/m_toko.php */
