<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class m_analisa_track extends CI_Model {

	public function getDataTrack() {
		$this->db->select('*');
		$this->db->from('t_users_location_history');
		$q = $this->db->get();

		return $q->result();
	}
	
	public function getPoint() {
		$this->db->select('*');
		$this->db->from('t_trayek_point as TP');
		$this->db->where('TP.id_trayek', 1);
		$q = $this->db->get();

		return $q->result();
	}
	
	public function getOnRoad($id_trayek) {
		$this->db->select('*');
		$this->db->from('t_users_location_history');
		$this->db->where('id_trayek', $id_trayek);
		$this->db->where('DATE(created_at)', '2018-03-26');
		$q = $this->db->get();

		return $q->result();
	}
	
	public function getTrackUser($id_user) {
		$this->db->select('*');
		$this->db->from('t_users_location_history');
		$this->db->where('id_user', $id_user);
		$this->db->where('DATE(created_at)', date('Y-m-d'));
		$q = $this->db->get();

		return $q->result();
	}
	
	public function getTrack($id_user, $date_start = '', $date_end = '') {
		$this->db->select('*');
		$this->db->from('t_users_location_history');
		$this->db->where('id_user', $id_user);
		
		if (isset($date_start) && $date_start != "") {
			if (isset($date_end) && $date_end != "") {
				$this->db->where('DATE(created_at) >=', $date_start);
			} else {
				$this->db->where('DATE(created_at)', $date_start);
			}
		}
		
		if (isset($date_end) && $date_end != "") {
			$this->db->where('DATE(created_at) <=', $date_end);
		}
		
		$q = $this->db->get();

		return $q->result();
	}

}

/* End of file m_toko.php */
/* Location: ./application/models/m_toko.php */
