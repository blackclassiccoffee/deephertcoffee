<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class m_pelanggan extends CI_Model {

	public function getPelanggan($filter, $limit, $page) {
		
		if($page != 1) {
			$offset = (($page-1) * $limit);
		} else {
			$offset = $page-1;
		}
		
		if($limit != -1) {
			$this->db->limit($limit, $offset);
		}
		
		$this->db->order_by('P.id', 'ASC');
		$this->db->select('
			P.*
		');
		$this->db->from('t_pelanggan as P');
		
		if (isset($filter['cari']) && $filter['cari'] != '') {
			$this->db->where('P.nama_pelanggan LIKE', '%'.$filter['cari'].'%');
			$this->db->or_where('P.npwp LIKE', '%'.$filter['cari'].'%');
		}
		
		$q = $this->db->get();

		return $q->result();
	}
	
	public function record_count($filter) {
		
		$this->db->select('COUNT(*) AS `numrows`');
		$this->db->from('t_pelanggan as P');
		
		if (isset($filter['cari']) && $filter['cari'] != '') {
			$this->db->where('P.nama_pelanggan LIKE', '%'.$filter['cari'].'%');
			$this->db->or_where('P.npwp LIKE', '%'.$filter['cari'].'%');
		}
		
       	return $this->db->get()->row();
    }
	
	public function getDetailPelanggan($filter) {
		
		$this->db->select('
			P.*,
			TPROV.nama_provinsi as prov_pelanggan,
			TKAB.nama_kabupaten as kab_pelanggan,
			TPROV_NPWP.nama_provinsi as prov_npwp,
			TKAB_NPWP.nama_kabupaten as kab_npwp
		');
		$this->db->from('t_pelanggan as P');
		$this->db->join('t_geo_kabupaten as TKAB', 'P.kabupaten_pelanggan_id = TKAB.id', 'LEFT');
		$this->db->join('t_geo_provinsi as TPROV', 'TKAB.provinsi_id = TPROV.id', 'LEFT');
		$this->db->join('t_geo_kabupaten as TKAB_NPWP', 'P.kabupaten_npwp_id = TKAB_NPWP.id', 'LEFT');
		$this->db->join('t_geo_provinsi as TPROV_NPWP', 'TKAB_NPWP.provinsi_id = TPROV_NPWP.id', 'LEFT');
		
		if (isset($filter['pelanggan_id']) && $filter['pelanggan_id'] != '') {
			$this->db->where('P.id', $filter['pelanggan_id']);
		}
		
		$q = $this->db->get();

		return $q->row();
	}

}

/* End of file m_toko.php */
/* Location: ./application/models/m_toko.php */
