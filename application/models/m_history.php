<?php 
Class M_history extends CI_Model
{
	public function insert($keterangan) {
		$data_session = $this->session->userdata('login_sess');
		$data['id'] = $data_session['ID'];
		$data['nama'] = $data_session['namaLengkap'];
		$data['kantor'] = $data_session['KANTOR'];

		$data = [
		'keterangan'	=> $keterangan,
		'id_author'		=> $data['id'],
		'author'		=> $data['nama'],
		'created_at'	=> date('Y-m-d H:i:s'),
		'kantor'		=> $data['kantor'],
		];

		$this->db->insert('t_history', $data);

		return $this->db->insert_id();
	}
}