<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class m_tombol_darurat extends CI_Model {

	public function getPanic($filter, $limit, $page) {
		
		if($page != 1) {
			$offset = (($page-1) * $limit);
		} else {
			$offset = $page-1;
		}
		
		if($limit != -1) {
			$this->db->limit($limit, $offset);
		}
		
		$this->db->order_by('TL.status_proved, TL.created_at', 'DESC');
		$this->db->select('TL.*, U.Name, U.id as id_user, UP.Name as NameProved, TA.plat_nomor');
		$this->db->from('t_panic as TL')
			->join('users U', 'TL.id_user = U.id', 'LEFT')
			->join('users UP', 'TL.id_user_proved = UP.id', 'LEFT')
			->join('t_armada TA', 'TL.id_user = TA.id_user');
		
		if (isset($filter['pelapor']) && $filter['pelapor'] != "") {
			$this->db->where('U.Name LIKE', '%'.$filter['pelapor'].'%');
		}
		
		if (isset($filter['nopol']) && $filter['nopol'] != "") {
			$this->db->where('TA.plat_nomor LIKE', '%'.$filter['nopol'].'%');
		}
		
		if (isset($filter['status_proved']) && $filter['status_proved'] != "") {
			$this->db->where('TL.status_proved', $filter['status_proved']);
		}
		
		
		if (isset($filter['tanggal_awal']) && $filter['tanggal_awal'] != "") {
			if (isset($filter['tanggal_akhir']) && $filter['tanggal_akhir'] != "") {
				$this->db->where('DATE(TL.created_at) >=', $filter['tanggal_awal']);
			} else {
				$this->db->where('DATE(TL.created_at)', $filter['tanggal_awal']);
			}
		}
		
		if (isset($filter['tanggal_akhir']) && $filter['tanggal_akhir'] != "") {
			$this->db->where('DATE(TL.created_at) <=', $filter['tanggal_akhir']);
		}
		
		$q = $this->db->get();

		return $q->result();
	}
	
	public function record_count($filter) {
		
		$this->db->order_by('TL.status_proved, TL.created_at', 'ASC');
		$this->db->select('COUNT(*) AS `numrows`');
		$this->db->from('t_panic as TL')
			->join('users U', 'TL.id_user = U.id', 'LEFT')
			->join('t_armada TA', 'TL.id_user = TA.id_user');
		
		if (isset($filter['pelapor']) && $filter['pelapor'] != "") {
			$this->db->where('U.Name LIKE', '%'.$filter['pelapor'].'%');
		}
		
		if (isset($filter['status_proved']) && $filter['status_proved'] != "") {
			$this->db->where('TL.status_proved', $filter['status_proved']);
		}
		
		
		if (isset($filter['tanggal_awal']) && $filter['tanggal_awal'] != "") {
			if (isset($filter['tanggal_akhir']) && $filter['tanggal_akhir'] != "") {
				$this->db->where('DATE(TL.created_at) >=', $filter['tanggal_awal']);
			} else {
				$this->db->where('DATE(TL.created_at)', $filter['tanggal_awal']);
			}
		}
		
		if (isset($filter['tanggal_akhir']) && $filter['tanggal_akhir'] != "") {
			$this->db->where('DATE(TL.created_at) <=', $filter['tanggal_akhir']);
		}
		
       	return $this->db->get()->row();
    }

}

/* End of file m_toko.php */
/* Location: ./application/models/m_toko.php */
