<?php

class m_dashboard extends CI_Model {
	
	public function getDataGraphOmset($dataMonth = "", $dataYear = "") {
		
		$this->db->select('SUM(TM.total) as total');
		$this->db->from('t_bongkar_muat TM');
		
		$this->db->where('MONTH(TM.created_at)', $dataMonth);
		$this->db->where('YEAR(TM.created_at)', $dataYear);
		
		return $this->db->get()->row();
	}
	
	public function getDataGraphBongkarMuat($dataMonth = "", $dataYear = "") {
		
		$this->db->select('COUNT(*) as total');
		$this->db->from('t_bongkar_muat TM');
		
		$this->db->where('MONTH(TM.created_at)', $dataMonth);
		$this->db->where('YEAR(TM.created_at)', $dataYear);
		
		return $this->db->get()->row();
	}
	
	public function getDataGraphPanic($dataMonth = "", $dataYear = "") {
		
		$this->db->select('COUNT(*) as total');
		$this->db->from('t_panic TM');
		
		$this->db->where('MONTH(TM.created_at)', $dataMonth);
		$this->db->where('YEAR(TM.created_at)', $dataYear);
		
		return $this->db->get()->row();
	}
	
	public function getDataGraphKendala($dataMonth = "", $dataYear = "") {
		
		$this->db->select('COUNT(*) as total');
		$this->db->from('t_laporan_kendala TM');
		
		$this->db->where('MONTH(TM.created_at)', $dataMonth);
		$this->db->where('YEAR(TM.created_at)', $dataYear);
		
		return $this->db->get()->row();
	}

}

?>