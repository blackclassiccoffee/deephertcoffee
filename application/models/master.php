<?php

class master extends CI_Model {
	
	function sendNotification($tokens, $message = "", $n) {
    	$url = 'https://fcm.googleapis.com/fcm/send';
		$fields = array(
			 /*'to' 		    => $tokens,*/
			 'registration_ids' => $tokens,
			 'priority'		=> "high",
			 //'notification'	=> $n,
			 'data'			=> $message
		);

		//var_dump($fields);

		$headers = array(
			'Authorization:key = AAAAwLh71-s:APA91bHp4feyq76q7mclkB7Oxl1yo95nnBKFyrXaEc_DD2Ig1dNfg7I76EjquED4g6_1qr7xLu_Ug53WPLHWkWje-mnzJOA8Nbh2WkrdBmn6ynMuF3NQM0qGlIM8RRQ_-exvoiRdhZrI',
			'Content-Type: application/json'
		);

	   $ch = curl_init();
       curl_setopt($ch, CURLOPT_URL, $url);
       curl_setopt($ch, CURLOPT_POST, true);
       curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
       curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
       curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);  
       curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
       curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
       $result = curl_exec($ch);           
       if ($result === FALSE) {
           die('Curl failed: ' . curl_error($ch));
       }
       curl_close($ch);
       return $result;
    }
	
	public function get_hak_akses($segment = "") {
		$data_session = $this->session->userdata('login_sess');
		$data['id'] = $data_session['id'];

		$segment = $this->uri->segment($segment);

		$this->db->where('segment', $segment);
		$menu_id = $this->db->get('t_menu')->row()->id;

		$this->db->where('idUser', $data['id']);
		$this->db->where('idMenu', $menu_id);
		return $this->db->get('t_user_menu')->row();
	}

	public function tipe_customer() {
		return $this->db->get('master_tipe_customer')->result();
	}

	public function kode_kunjungan() {
		return $this->db->get('master_kode_kunjungan')->result();
	}

	public function autoKode($x = "", $table) {
		$q    = $this->db->query("select MAX(id) as code_max from ".$table."");
		$code = "";
		if ($q->num_rows() > 0) {
			foreach ($q->result() as $cd) {
				$tmp  = ((int) $cd->code_max)+1;
				$code = $tmp;
			}
		} else {
			$code = "01";
		}
		return "".$x."/".date('Ymd')."/".$code;
	}


	public function getMenu($id) {
		$q = $this->db->query("SELECT * FROM t_user_menu TU LEFT JOIN t_menu TM ON TU.idMenu = TM.id WHERE idUser = '".$id."'");
		return $q->result();
	}
	public function getJabatan() {
		$this->db->select('*');
		return $this->db->from('t_position')->get()->result();

	}
	public function getSales() {
		$this->db->select('*');
		return $this->db->from('t_sales')->get()->result();

	}
	public function getRole() {
		$this->db->select('*');
		return $this->db->from('t_roles')->get()->result();

	}
	public function getDistributor() {
		$this->db->select('*');
		return $this->db->from('t_distributor')->get()->result();

	}
	public function getSupplier() {
		$this->db->select('*');
		return $this->db->from('t_supplier')->get()->result();

	}
	
	public function getKantor() {
		$this->db->select('*');
		return $this->db->from('t_kanca')->get()->result();

	}
	
	public function getLab() {
		$this->db->select('*');
		return $this->db->from('t_lab')->get()->result();

	}
	
	public function getCustomer($id_perusahaan) {
		$this->db->select('*');
		$this->db->where('id_perusahaan', $id_perusahaan);
		return $this->db->from('t_customer')->get()->result();

	}
	public function getDriver() {
		$this->db->select('*');
		return $this->db->from('t_driver')->get()->result();

	}
	public function getAksi() {
		$this->db->select('*');
		return $this->db->from('t_aksi')->get()->result();

	}
	public function getJenisBarang() {
		$this->db->select('*');
		return $this->db->from('master_jenis_barang')->get()->result();

	}
	public function getJenisBarangXX($id = '0') {
		$this->db->select('*');
		$this->db->from('master_jenis_barang');
		$this->db->where('id', $id);
		return $this->db->get()->result()[0]->nama_jenis;
	}
	public function getUserXX($id, $toko) {
		$this->db->select('du.namaLengkap as nama, ul.ID as id_user, ul.LEVEL');
		$this->db->from('user_login ul');
		$this->db->join('detailuser du', 'ul.ID = du.ID');
		$this->db->where('ul.LEVEL', $id);
		$this->db->where('ul.KANTOR', $toko);
		return $this->db->get()->result();

	}
	public function insertHistory($data) {
		return $this->db->insert('t_history', $data);
	}

	public function menu($parent = 0) {
		$x            = "";
		$session_data = $this->session->userdata('login_sess');
		$query        = $this->db->query("SELECT
			T.`id`,
			T.`link`,
			T.`nama_menu`,
			T.icon,
			T.parent,
			xx.Count,
			T.js_evenClick
			FROM
			t_user_menu TU
			LEFT JOIN t_menu T
			ON TU.`idMenu` = T.`id`
			LEFT OUTER JOIN
			(SELECT
			COUNT(*) AS COUNT,
			parent
			FROM
			t_menu
			GROUP BY parent) xx
			ON T.`id` = xx.parent
			WHERE idUser = '".$session_data['id']."'
			AND T.`parent` = '".$parent."' AND T.flag_aktif = 1 ORDER BY T.sort asc");
		
		foreach ($query->result_array() as $data) {
			if ($data['Count'] == 0 || $data['Count'] == NULL) {
				if($data['js_evenClick'] != ""){
					$evenClick = 'onclick="'.$data['js_evenClick'].'"';
				} else {
					$evenClick = '';
				}

				if($data['link'] != ""){
					$link = base_url()."".$data['link'];
				} else {
					$link = "javascript:void(0)";
				}

				$x .= "<li class='nav-item'>
						 <a href='".$link."' $evenClick class='nav-link'>
						 	<i class='nav-icon ".$data['icon']."'></i>
							<p>".$data['nama_menu']."</p>
						 </a>
					  </li>";
			} else {
				if ($data['parent'] != 0) {

					if($data['js_evenClick'] != ""){
						$evenClick = 'onclick="'.$data['js_evenClick'].'"';
					} else {
						$evenClick = '';
					}

					if($data['link'] != ""){
						$link = base_url()."".$data['link'];
					} else {
						$link = $data['link'];
					}

					$x .= "
								<li class='nav-item'>
									<a href='".$link."' $evenClick class='nav-link'>
										<i class='nav-icon ".$data['icon']."'></i>
										<p>
										".$data['nama_menu']."
										<i class='right fa fa-angle-left'></i>
										</p>
									</a>
									<ul class='nav nav-treeview'>
										".$this->menu($data['id'])."
									</ul>
								</li>
							";
				} else {
		
					if($data['js_evenClick'] != ""){
						$evenClick = 'onclick="'.$data['js_evenClick'].'"';
					} else {
						$evenClick = '';
					}

					if($data['link'] != ""){
						$link = base_url()."".$data['link'];
					} else {
						$link = $data['link'];
					}

					$x .= "
							<li class='nav-item has-treeview'>
								<a href='".$link."' $evenClick class='nav-link'>
									<i class='nav-icon ".$data['icon']."'></i>
									<p>
									".$data['nama_menu']."
									<i class='right fa fa-angle-left'></i>
									</p>
								</a>
								<ul class='nav nav-treeview'>
									".$this->menu($data['id'])."
								</ul>
							</li>
							";
			}

		}
	}

	return $x;
}

	public function menuToEdit($parent = 0, $idU) {
		$x            = "";
		$session_data = $this->session->userdata('login_sess');
		$query        = $this->db->query("SELECT
			T.`id`,
			T.`link`,
			T.`nama_menu`,
			T.icon,
			xx.Count
			FROM
			t_menu T
			LEFT OUTER JOIN
			(SELECT
			COUNT(*) AS COUNT,
			parent
			FROM
			t_menu
			GROUP BY parent) xx
			ON T.`id` = xx.parent
			WHERE T.`parent` = '".$parent."' AND T.flag_aktif = 1 ORDER BY T.sort ASC");
			//echo "<ul style='list-style-type: none;'>";

		$arr_menu = array();
		foreach ($query->result_array() as $data) {

			$query   = $this->db->query("SELECT * FROM t_user_menu TUM LEFT JOIN t_menu TM ON TUM.idMenu = TM.id WHERE TUM.idMenu = '".$data['id']."' AND TUM.idUser = '".$idU."'");
			$theData = $query->row();

			$dataId = $query->num_rows > 0 ? $theData->id : "";

			$arr_menu[] = $data['id'];
			if (in_array($dataId, $arr_menu)) {
				$isChecked = "checked='checked'";
				if($theData->role_create == 1) {
					$role_create = "checked='checked'";
				} else {
					$role_create = "";
				}

				if($theData->role_read == 1) {
					$role_read = "checked='checked'";
				} else {
					$role_read = "";
				}

				if($theData->role_update == 1) {
					$role_update = "checked='checked'";
				} else {
					$role_update = "";
				}

				if($theData->role_delete == 1) {
					$role_delete = "checked='checked'";
				} else {
					$role_delete = "";
				}

				if($theData->role_download == 1) {
					$role_download = "checked='checked'";
				} else {
					$role_download = "";
				}
			} else {
				$isChecked   = "";
				$role_create = "";
				$role_read	 = "";
				$role_update = "";
				$role_delete = "";
				$role_download = "";
			}


			if ($data['Count'] == 0 || $data['Count'] == NULL) {

				if($data['id'] == 1) {
					$x .= "<li>
						<input type='checkbox' name='menu[]' id='x' value='".$data['id']."' readonly='readonly' $isChecked>
						<i class='" .$data['nama_menu']."'></i>
						<input type='hidden' name='hideMenu[]' value='".$data['id']."'/>
						".$data['nama_menu']."
						<br>
						<fieldset style='border: 1px solid #d9d9d9;'>
							<legend style='margin-left:10px; padding-left:10px; border: 1px solid #d9d9d9; width:100px; margin-bottom:0px; font-size:12px;'>Role :</legend>
							<div style='margin-left: 25px; margin-top: 8px; margin-bottom: 8px;'>
							
								<span style='margin-right: 10px;'>
									<input type='checkbox' id='x' name='role_create[]' class='r_create_".$data['id']."' $role_create value=".$data['id']."> Create
									<input type='hidden' name='hideRole_create[]' value='".$data['id']."'/>
								</span>
								
								<span style='margin-right: 10px;'>
									<input type='checkbox' id='x' name='role_read[]' class='r_read_".$data['id']."' ".$role_read." value=".$data['id']."> Read
									<input type='hidden' name='hideRole_read[]' value='".$data['id']."'/>
								</span>

								<span style='margin-right: 10px;'>
									<input type='checkbox' id='x' name='role_up[]' class='r_update_".$data['id']."' ".$role_update." value=".$data['id']."> Update
									<input type='hidden' name='hideRole_up[]' value='".$data['id']."'/>
								</span>
								
								<span style='margin-right: 10px;'>
									<input type='checkbox' id='x' name='role_delete[]' class='r_delete_".$data['id']."' ".$role_delete." value=".$data['id']."> Delete
									<input type='hidden' name='hideRole_delete[]' value='".$data['id']."'/>
								</span>
								
								<span style='margin-right: 10px;'>
									<input type='checkbox' id='x' name='role_download[]' class='r_download_".$data['id']."' ".$role_download." value=".$data['id']."> Download
									<input type='hidden' name='hideRole_download[]' value='".$data['id']."'/>
								</span>
								
							</div>
						</fieldset>
					</li>";
				} else {

					$x .= "<li>
						<input type='checkbox' name='menu[]' id='x' value='".$data['id']."' $isChecked>
						<i class='" .$data['nama_menu']."'></i>
						<input type='hidden' name='hideMenu[]' value='".$data['id']."'/>
						".$data['nama_menu']."
						<br>
						<fieldset style='border: 1px solid #d9d9d9;'>
							<legend style='margin-left:10px; padding-left:10px; border: 1px solid #d9d9d9; width:100px; margin-bottom:0px; font-size:12px;'>Role :</legend>
							<div style='margin-left: 25px; margin-top: 8px; margin-bottom: 8px;'>
							
								<span style='margin-right: 10px;'>
									<input type='checkbox' id='x' name='role_create[]' class='r_create_".$data['id']."' $role_create value=".$data['id']."> Create
									<input type='hidden' name='hideRole_create[]' value='".$data['id']."'/>
								</span>
								
								<span style='margin-right: 10px;'>
									<input type='checkbox' id='x' name='role_read[]' class='r_read_".$data['id']."' ".$role_read." value=".$data['id']."> Read
									<input type='hidden' name='hideRole_read[]' value='".$data['id']."'/>
								</span>

								<span style='margin-right: 10px;'>
									<input type='checkbox' id='x' name='role_up[]' class='r_update_".$data['id']."' ".$role_update." value=".$data['id']."> Update
									<input type='hidden' name='hideRole_up[]' value='".$data['id']."'/>
								</span>
								
								<span style='margin-right: 10px;'>
									<input type='checkbox' id='x' name='role_delete[]' class='r_delete_".$data['id']."' ".$role_delete." value=".$data['id']."> Delete
									<input type='hidden' name='hideRole_delete[]' value='".$data['id']."'/>
								</span>
								
								<span style='margin-right: 10px;'>
									<input type='checkbox' id='x' name='role_download[]' class='r_download_".$data['id']."' ".$role_download." value=".$data['id']."> Download
									<input type='hidden' name='hideRole_download[]' value='".$data['id']."'/>
								</span>
								
							</div>
						</fieldset>
					</li>";
				}
			} else {
				$x .= "<li>
					<input type='checkbox' name='menu[]' id='x' value='".$data['id']."' $isChecked>
					<i class='" .$data['nama_menu']."'></i>
					".$data['nama_menu']."
					<ul>
						<input type='hidden' name='hideMenu[]' value='".$data['id']."'/>
						".$this->menuToEdit($data['id'], $idU)."
					</ul>
				</li>";

			}
		}
		//echo "</ul>";
		return $x;
	}
	
	public function menuToAdd($parent = 0) {
		$x            = "";
		$session_data = $this->session->userdata('login_sess');
		$query        = $this->db->query("SELECT
			T.`id`,
			T.`link`,
			T.`nama_menu`,
			T.icon,
			xx.Count
			FROM
			t_menu T
			LEFT OUTER JOIN
			(SELECT
			COUNT(*) AS COUNT,
			parent
			FROM
			t_menu
			GROUP BY parent) xx
			ON T.`id` = xx.parent
			WHERE T.`parent` = '".$parent."' AND T.flag_aktif = 1 ORDER BY T.sort ASC");
			//echo "<ul style='list-style-type: none;'>";

		$arr_menu = array();
		foreach ($query->result_array() as $data) {


			if ($data['Count'] == 0 || $data['Count'] == NULL) {

				if($data['id'] == 1) {
					$x .= "<li>
						<input type='checkbox' name='menu[]' id='x' value='".$data['id']."' readonly='readonly'>
						".$data['nama_menu']."
						<br>
						<fieldset style='border: 1px solid #d9d9d9;'>
							<legend style='margin-left:10px; padding-left:10px; border: 1px solid #d9d9d9; width:100px; margin-bottom:0px; font-size:12px;'>Role :</legend>
							<div style='margin-left: 25px; margin-top: 8px; margin-bottom: 8px;'>
								
								<span style='margin-right: 10px;'>
									<input type='checkbox' id='x' name='role_create[]' class='r_create_".$data['id']."' value=".$data['id']."> Create
								</span>
								
								<span style='margin-right: 10px;'>
									<input type='checkbox' id='x' name='role_read[]' class='r_read_".$data['id']."' value=".$data['id']."> Read
								</span>
								
								<span style='margin-right: 10px;'>
									<input type='checkbox' id='x' name='role_up[]' class='r_update_".$data['id']."' value=".$data['id']."> Update
								</span>
								
								<span style='margin-right: 10px;'>
									<input type='checkbox' id='x' name='role_delete[]' class='r_delete_".$data['id']."' value=".$data['id']."> Delete
								</span>
								
								<span style='margin-right: 10px;'>
									<input type='checkbox' id='x' name='role_download[]' class='r_download_".$data['id']."' value=".$data['id']."> Download
								</span>
							</div>
						</fieldset>
					</li>";
				} else {

					$x .= "<li class='child'>
						<input type='checkbox' name='menu[]' id='x' value='".$data['id']."'>
						<i class='" .$data['nama_menu']."'></i>
						<input type='hidden' name='hideMenu[]' value='".$data['id']."'/>
						".$data['nama_menu']."
						<br>
						<fieldset style='border: 1px solid #d9d9d9;'>
							<legend style='margin-left:10px; padding-left:10px; border: 1px solid #d9d9d9; width:100px; margin-bottom:0px; font-size:12px;'>Role :</legend>
							<div style='margin-left: 25px; margin-top: 8px; margin-bottom: 8px;'>

								<span style='margin-right: 10px;'>
									<input type='checkbox' id='x' name='role_create[]' class='r_create_".$data['id']."' value=".$data['id']."> Create
								</span>
								
								<span style='margin-right: 10px;'>
									<input type='checkbox' id='x' name='role_read[]' class='r_read_".$data['id']."' value=".$data['id']."> Read
								</span>
								
								<span style='margin-right: 10px;'>
									<input type='checkbox' id='x' name='role_up[]' class='r_update_".$data['id']."' value=".$data['id']."> Update
								</span>
								
								<span style='margin-right: 10px;'>
									<input type='checkbox' id='x' name='role_delete[]' class='r_delete_".$data['id']."' value=".$data['id']."> Delete
								</span>
								
								<span style='margin-right: 10px;'>
									<input type='checkbox' id='x' name='role_download[]' class='r_download_".$data['id']."' value=".$data['id']."> Download
								</span>
							</div>
						</fieldset>
					</li>";
				}
			} else {
				$x .= "<li class='childx'>
					<input type='checkbox' name='menu[]' id='x' value='".$data['id']."' >
					<i class='" .$data['nama_menu']."'></i>
					".$data['nama_menu']."
					<ul>
						<input type='hidden' name='hideMenu[]' value='".$data['id']."'/>
						".$this->menuToAdd($data['id'])."
					</ul>
				</li>";

			}
		}
		//echo "</ul>";
		return $x;
	}
	
public function getBulan() {
	$this->db->select('*');
	return $this->db->from('bulan')->get()->result();

}
public function getNamaBulan($id) {
	$this->db->select('bulan');
	$this->db->from('bulan');
	$this->db->where('id', $id);
	return $this->db->get()->row()->bulan;
}

public function get_toko() {
	return $this->db->get('master_toko')->result();
}

public function get_jabatan() {
	return $this->db->get('master_jabatan_karyawan')->result();
}

public function get_jenis_kelamin() {
	return $this->db->get('master_jenis_kelamin')->result();
}

public function get_agama() {
	return $this->db->get('master_agama')->result();
}

public function totalrequest() {
	$toko = $this->session->userdata('login_sess')['KANTOR'];
	$this->db->where('id_toko', $toko);
	$this->db->where('status_hp', 7);
	$this->db->where('status_request !=', 0);
	return $this->db->count_all_results('master_hp');
}

public function totalrequest_aksesoris() {
	$toko = $this->session->userdata('login_sess')['KANTOR'];
	$this->db->where('id_toko', $toko);
	$this->db->where('status_req', 1);
	return $this->db->count_all_results('master_stok_accessoris_toko');
}

public function totalrequest_perdana() {
	$toko = $this->session->userdata('login_sess')['KANTOR'];
	$this->db->where('id_toko', $toko);
	$this->db->where('status_transaksi', 7);
	return $this->db->count_all_results('master_perdana');
}

public function convert_time($datetime, $full = false){
	$now = new DateTime;
	$ago = new DateTime($datetime);
	$diff = $now->diff($ago);

	$diff->w = floor($diff->d / 7);
	$diff->d -= $diff->w * 7;

	$string = array(
		'y' => 'Year',
		'm' => 'Month',
		'w' => 'Week',
		'd' => 'Day',
		'h' => 'Hour',
		'i' => 'Minute',
		's' => 'Second',
		);
	foreach ($string as $k => &$v) {
		if ($diff->$k) {
			$v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
		} else {
			unset($string[$k]);
		}
	}

	if (!$full) $string = array_slice($string, 0, 1);
	return $string ? implode(', ', $string) . ' ago' : 'Just now';

}

}

?>
