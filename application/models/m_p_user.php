<?php

class m_p_user extends CI_Model{
	public function insertUser($data){
		$this->db->insert('user_login', $data);
	}

	public function insertDetailUser($data){
		$this->db->insert('detailuser', $data);
	}

	public function toUpdateUser($id, $data){
		$d = $this->db->where('ID', $id);
		$d = $this->db->update('user_login', $data);
		return $d;
	}
	
	public function toUpdateRole($id, $user, $data){
		$d = $this->db->where('idMenu', $id);
		$d = $this->db->where('idUser', $user);
		$d = $this->db->update('t_user_menu', $data);
		return $d;
	}

	public function toUpdateDetailUser($id, $data){
		$d = $this->db->where('ID', $id);
		$d = $this->db->update('detailuser', $data);
		return $d;
	}

	public function autoUser(){
		$q = $this->db->query("select MAX(id_detail) as code_max from detailuser");
        $code = "";
        if($q->num_rows()>0){
            foreach($q->result() as $cd){
                $tmp = ((int)$cd->code_max)+1;
                $code = $tmp;
            }
        }else{
            $code = "01";
        }
        return "U".$code;
	}
	  /*public function autoKode(){
        $q = $this->db->query("select MAX(id) as code_max from master_accessoris_utama");
        $code = "";
        if($q->num_rows()>0){
            foreach($q->result() as $cd){
                $tmp = ((int)$cd->code_max)+1;
                $code = $tmp;
            }
        }else{
            $code = "01";
        }
        return "ACC/".date('YmdHi')."/".$code;
    }*/

	public function toDeleteUser($id){
		$d = $this->db->where('id', $id);
		$d = $this->db->delete('users');
		return $d;
	}

	public function getJabatan(){
		$this->db->select('*');
		$this->db->from('master_jabatan');
		$q = $this->db->get();

		return $q->result();
	}

	public function getUser($filter, $limit, $page, $session){
		
		if($page != 1) {
			$offset = (($page-1) * $limit);
		} else {
			$offset = $page-1;
		}
		
		if($limit != -1) {
			$this->db->limit($limit, $offset);
		}
				
		$this->db->select('
			UL.*, 
			TC.nama_kanca, 
			TL.id as lab_id, 
			TL.nama_lab, 
			TP.PositionName
		');
		$this->db->from('users UL');
		$this->db->join('t_karyawan TK', 'UL.karyawan_id = TK.id', 'LEFT');
		$this->db->join('t_position TP', 'TK.IDPosition = TP.id', 'LEFT');
		$this->db->join('t_lab TL', 'TK.lab_id = TL.id', 'LEFT');
		$this->db->join('t_kanca TC', 'TL.kanca_id = TC.id', 'LEFT');
		$this->db->order_by('UL.id', 'DESC');
		
		if (isset($filter['cari'])) {
			if (isset($filter['kantor_src']) && $filter['kantor_src'] != "") {
				$this->db->where('TC.id', $filter['kantor_src']);
			}
			
			if (isset($filter['karyawan_src']) && $filter['karyawan_src'] != "") {
				$this->db->where('UL.Name LIKE', '%'.$filter['karyawan_src'].'%');
			}
			
			if (isset($filter['jabatan_src']) && $filter['jabatan_src'] != "") {
				$this->db->where('TP.id', $filter['jabatan_src']);
			}
			
			if (isset($filter['lab_src']) && $filter['lab_src'] != "") {
				$this->db->where('TL.id', $filter['lab_src']);
			}
		}
		
		$q = $this->db->get();

		return $q->result_array();
	}
	
	public function record_count($filter, $session) {
        $this->db->select('COUNT(*) AS `numrows`');
		$this->db->from('users UL');
		$this->db->join('t_karyawan TK', 'UL.karyawan_id = TK.id', 'LEFT');
		$this->db->join('t_position TP', 'TK.IDPosition = TP.id', 'LEFT');
		$this->db->join('t_lab TL', 'TK.lab_id = TL.id', 'LEFT');
		$this->db->join('t_kanca TC', 'TL.kanca_id = TC.id', 'LEFT');
		$this->db->order_by('UL.id', 'DESC');
		
		if (isset($filter['cari'])) {
			if (isset($filter['kantor_src']) && $filter['kantor_src'] != "") {
				$this->db->where('TC.id', $filter['kantor_src']);
			}
			
			if (isset($filter['karyawan_src']) && $filter['karyawan_src'] != "") {
				$this->db->where('UL.Name LIKE', '%'.$filter['karyawan_src'].'%');
			}
			
			if (isset($filter['jabatan_src']) && $filter['jabatan_src'] != "") {
				$this->db->where('TP.id', $filter['jabatan_src']);
			}
			
			if (isset($filter['lab_src']) && $filter['lab_src'] != "") {
				$this->db->where('TL.id', $filter['lab_src']);
			}
		}
		
       	return $this->db->get()->row();
    }

	public function getUserToEdit($id){
		$this->db->select('UL.*, TC.id as kanca_id, TL.id as lab_id');
		$this->db->from('users UL');
		$this->db->join('t_karyawan TK', 'UL.karyawan_id = TK.id', 'LEFT');
		$this->db->join('t_lab TL', 'TK.lab_id = TL.id', 'LEFT');
		$this->db->join('t_kanca TC', 'TL.kanca_id = TC.id', 'LEFT');
		$this->db->where('UL.id', $id);
		$q = $this->db->get();

		return $q->row();
	}

	public function insertToMenu($data){
		$this->db->insert('t_user_menu', $data);
	}

	public function deleteToMenu($idUser, $idMenu){
		$d = $this->db->where('idUser', $idUser);
		$d = $this->db->where('idMenu', $idMenu);
		$d = $this->db->delete('t_user_menu');
		return $d;
	}

	public function checked_user($user = ''){
		$this->db->select('*');
		$this->db->from('users UL');
		$this->db->where('UL.email', $user);
		$q = $this->db->get();

		return $q->row();
	}
}

?>