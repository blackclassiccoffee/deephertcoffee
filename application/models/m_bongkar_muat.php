<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class m_bongkar_muat extends CI_Model {

	public function getLaporBongkar($filter, $limit, $page) {
		
		if($page != 1) {
			$offset = (($page-1) * $limit);
		} else {
			$offset = $page-1;
		}
		
		if($limit != -1) {
			$this->db->limit($limit, $offset);
		}
		
		$this->db->order_by('TL.created_at', 'DESC');
		$this->db->select('TL.*, U.Name, TA.plat_nomor, TD.nama_distributor, TUL.lat, TUL.lng');
		$this->db->from('t_bongkar_muat as TL')
			->join('users U', 'TL.id_user = U.id')
			->join('t_armada TA', 'TL.id_user = TA.id_user')
			->join('t_distributor TD', 'TL.id_distributor = TD.id')
			->join('t_users_location TUL', 'TL.id_user = TUL.id_user');
		
		if (isset($filter['pelapor']) && $filter['pelapor'] != "") {
			$this->db->where('U.Name LIKE', '%'.$filter['pelapor'].'%');
		}
		
		if (isset($filter['nopol']) && $filter['nopol'] != "") {
			$this->db->where('TA.plat_nomor LIKE', '%'.$filter['nopol'].'%');
		}
		
		if (isset($filter['id_distributor']) && $filter['id_distributor'] != "") {
			$this->db->where('TL.id_distributor LIKE', '%'.$filter['id_distributor'].'%');
		}
		
		if (isset($filter['nospj']) && $filter['nospj'] != "") {
			$this->db->where('TL.no_spj LIKE', '%'.$filter['nospj'].'%');
		}
		
		if (isset($filter['asal']) && $filter['asal'] != "") {
			$this->db->where('TL.asal LIKE', '%'.$filter['asal'].'%');
		}
		
		if (isset($filter['tujuan']) && $filter['tujuan'] != "") {
			$this->db->where('TL.tujuan LIKE', '%'.$filter['tujuan'].'%');
		}
		
		if (isset($filter['status_proved']) && $filter['status_proved'] != "") {
			if ($filter['status_proved'] == "1") {
				$this->db->where('TL.tonase_bongkar !=', 0);
				$this->db->where('TL.uang_jalan !=', 0);
			} else {
				$this->db->where('TL.tonase_bongkar', 0);
				$this->db->or_where('TL.uang_jalan', 0); 
			}
		}
		
		
		if (isset($filter['tanggal_awal']) && $filter['tanggal_awal'] != "") {
			if (isset($filter['tanggal_akhir']) && $filter['tanggal_akhir'] != "") {
				$this->db->where('DATE(TL.created_at) >=', $filter['tanggal_awal']);
			} else {
				$this->db->where('DATE(TL.created_at)', $filter['tanggal_awal']);
			}
		}
		
		if (isset($filter['tanggal_akhir']) && $filter['tanggal_akhir'] != "") {
			$this->db->where('DATE(TL.created_at) <=', $filter['tanggal_akhir']);
		}
		
		$q = $this->db->get();

		return $q->result();
	}
	
	public function record_count($filter) {
		
		$this->db->select('COUNT(*) AS `numrows`');
		$this->db->from('t_bongkar_muat as TL')
			->join('users U', 'TL.id_user = U.id')
			->join('t_armada TA', 'TL.id_user = TA.id_user');
		
		if (isset($filter['pelapor']) && $filter['pelapor'] != "") {
			$this->db->where('U.Name LIKE', '%'.$filter['pelapor'].'%');
		}
		
		if (isset($filter['nopol']) && $filter['nopol'] != "") {
			$this->db->where('TA.plat_nomor LIKE', '%'.$filter['nopol'].'%');
		}
		
		if (isset($filter['id_distributor']) && $filter['id_distributor'] != "") {
			$this->db->where('TL.id_distributor LIKE', '%'.$filter['id_distributor'].'%');
		}
		
		if (isset($filter['nospj']) && $filter['nospj'] != "") {
			$this->db->where('TL.no_spj LIKE', '%'.$filter['nospj'].'%');
		}
		
		if (isset($filter['asal']) && $filter['asal'] != "") {
			$this->db->where('TL.asal LIKE', '%'.$filter['asal'].'%');
		}
		
		if (isset($filter['tujuan']) && $filter['tujuan'] != "") {
			$this->db->where('TL.tujuan LIKE', '%'.$filter['tujuan'].'%');
		}
		
		if (isset($filter['status_proved']) && $filter['status_proved'] != "") {
			if ($filter['status_proved'] == "1") {
				$this->db->where('TL.tonase_bongkar !=', 0);
				$this->db->where('TL.uang_jalan !=', 0);
			} else {
				$this->db->where('TL.tonase_bongkar', 0);
				$this->db->or_where('TL.uang_jalan', 0); 
			}
		}
		
		if (isset($filter['tanggal_awal']) && $filter['tanggal_awal'] != "") {
			if (isset($filter['tanggal_akhir']) && $filter['tanggal_akhir'] != "") {
				$this->db->where('DATE(TL.created_at) >=', $filter['tanggal_awal']);
			} else {
				$this->db->where('DATE(TL.created_at)', $filter['tanggal_awal']);
			}
		}
		
		if (isset($filter['tanggal_akhir']) && $filter['tanggal_akhir'] != "") {
			$this->db->where('DATE(TL.created_at) <=', $filter['tanggal_akhir']);
		}
		
       	return $this->db->get()->row();
    }
	
	public function getDataGraph() {
		
		$this->db->select('U.Name, SUM(TM.total) as total');
		$this->db->from('users U')
			->join('t_armada TA', 'U.id = TA.id_user', 'LEFT')
			->join('t_bongkar_muat TM', 'U.id = TM.id_user', 'LEFT');
		
		$this->db->group_by('TA.id_user');
		$this->db->order_by('total', 'DESC');
		
		return $this->db->get()->result();
	}

}

/* End of file m_toko.php */
/* Location: ./application/models/m_toko.php */
