<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class m_customer extends CI_Model {
	
	public function getData($filter, $limit, $page, $session) {
		
		if($page != 1) {
			$offset = (($page-1) * $limit);
		} else {
			$offset = $page-1;
		}
		
		if($limit != -1) {
			$this->db->limit($limit, $offset);
		}
		
		$this->db->order_by('TL.created_at', 'DESC');
		$this->db->select('TL.*');
		$this->db->from('t_customer as TL');
		$this->db->where('TL.id_perusahaan', $session['id_perusahaan']);
		
		if (isset($filter['nama_customer']) && $filter['nama_customer'] != "") {
			$this->db->where('TL.nama_customer LIKE', '%'.$filter['nama_customer'].'%');
		}
		
		if (isset($filter['tanggal_awal']) && $filter['tanggal_awal'] != "") {
			if (isset($filter['tanggal_akhir']) && $filter['tanggal_akhir'] != "") {
				$this->db->where('DATE(TL.created_at) >=', $filter['tanggal_awal']);
			} else {
				$this->db->where('DATE(TL.created_at)', $filter['tanggal_awal']);
			}
		}
		
		if (isset($filter['tanggal_akhir']) && $filter['tanggal_akhir'] != "") {
			$this->db->where('DATE(TL.created_at) <=', $filter['tanggal_akhir']);
		}
		
		$q = $this->db->get();

		return $q->result();
	}
	
	public function record_count($filter, $session) {
		
		$this->db->select('COUNT(*) AS `numrows`');
		$this->db->from('t_customer as TL');
		$this->db->where('TL.id_perusahaan', $session['id_perusahaan']);
		
		if (isset($filter['nama_customer']) && $filter['nama_customer'] != "") {
			$this->db->where('TL.nama_customer LIKE', '%'.$filter['nama_customer'].'%');
		}
		
		if (isset($filter['tanggal_awal']) && $filter['tanggal_awal'] != "") {
			if (isset($filter['tanggal_akhir']) && $filter['tanggal_akhir'] != "") {
				$this->db->where('DATE(TL.created_at) >=', $filter['tanggal_awal']);
			} else {
				$this->db->where('DATE(TL.created_at)', $filter['tanggal_awal']);
			}
		}
		
		if (isset($filter['tanggal_akhir']) && $filter['tanggal_akhir'] != "") {
			$this->db->where('DATE(TL.created_at) <=', $filter['tanggal_akhir']);
		}
		
       	return $this->db->get()->row();
    }
	
}

?>