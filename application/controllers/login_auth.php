<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class login_auth extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('master');
        $this->load->model('m_login');
        date_default_timezone_set("Asia/Jakarta");
    }

    public function index()
    {

        $q = $this->m_login->cekDataLogin($this->input->post('username'), $this->input->post('password'));
        if ($q) {
            $dataIns['remember_token']    = $this->input->post('session_data');
            //$dataIns['status_online'] = 1;

            $this->m_login->toUpdateUser($this->input->post('username'), $dataIns);
            foreach ($q as $dataUser) {
                $data_session = array(
                    'id'             => $dataUser->id,
                    'username'       => $dataUser->email,
                    'namaLengkap'    => $dataUser->Name,
					'avatar'		 => $dataUser->Picture,
                    'lab_id'         => $dataUser->lab_id,
                    'nama_lab'     	 => $dataUser->nama_lab,
                    'namaJabatan'    => $dataUser->PositionName,
                    'karyawan_id'    => $dataUser->karyawan_id,
                    'last_login'     => $dataUser->updated_at,
                    'tanggal_daftar' => $dataUser->created_at
                );
				
				$data_log = array(
					'keterangan' => $dataUser->Name.' telah login pada waktu '.date('Y-m-d H:i:s'),
					'kantor'	 => $dataUser->lab_id,
					'author'	 => $dataUser->Name,
					'created_at' => date('Y-m-d H:i:s')
				);
				
				$this->db->insert('t_history', $data_log);

                $this->session->set_userdata('login_sess', $data_session);
                $last_url = $this->session->userdata('last_url') ?: site_url('dashboard');
                redirect($last_url);
            }
        } else {
            $this->session->set_flashdata('Gagal', 'Email/Password Salah!');
            redirect(base_url('login'));
        }
    }

    public function logout()
    {

        //$data['status_online'] = '0';

        //$this->db->where('id', $id);
        //$this->db->update('users', $data);
		
		/*$data_log = array(
						'keterangan' => $un.' telah logout/keluar pada waktu '.date('Y-m-d H:i:s'),
						'kantor'	 => $kt,
						'author'	 => $nl,
						'created_at' => date('Y-m-d H:i:s')
					);
		$this->db->insert('t_history', $data_log);*/

        $this->session->sess_destroy();
        redirect(base_url('login'));
    }

}
