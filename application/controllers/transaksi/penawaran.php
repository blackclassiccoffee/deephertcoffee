<?php

class penawaran extends CI_Controller {

	function __construct(){
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");       
        $this->load->model('master');
        $this->load->model('m_p_user');
		$this->load->model('m_history');
		$this->load->model('m_penawaran');
		$this->load->model('m_pelanggan');
        $this->load->helper('url');
		//$this->load->library('guzzle');
    }

	public function v_data($id = ""){

		if($this->session->userdata('login_sess') == ""){
			$this->load->view('login');
		} else {
			$data_session = $this->session->userdata('login_sess');
			$data = array('menu'  => $this->master->menu(0));
			
			$filter = $this->input->get();
			
			if($_SERVER['QUERY_STRING'] != "") {
				$q_string = "?".$_SERVER['QUERY_STRING'];
			} else {
				$q_string = "";
			}
			
			$per_page = 10;
			
			if($this->uri->segment(4) != ""){
				$page = ($this->uri->segment(4)) ;
			} else {
				$page = 1;
			}
			
			$config = array();
			$config["base_url"] 		= base_url() . "/transaksi/penawaran/v_data/";
			$config["suffix"] 			= '/'.$q_string;
			$config["first_url"] 		= base_url() . "/transaksi/penawaran/v_data/1/".$q_string;
			$config["per_page"] 		= $per_page;
			$config["total_rows"] 		= $this->m_penawaran->record_count($filter, $data_session)->numrows;
			$config["uri_segment"] 		= 4;
			$choice = $config["total_rows"] / $config["per_page"];
			$config["num_links"] 		= 2;//round($choice);
			$config['use_page_numbers'] = TRUE;
			$config['full_tag_open']  	= '<ul class="pagination pagination-sm m-0 float-right">';
			$config['full_tag_close'] 	= '</ul><!--pagination-->';

			$config['first_link']      = '&laquo; First';
			$config['first_tag_open']  = '<li class="page-item page-link">';
			$config['first_tag_close'] = '</li>';

			$config['last_link']      = 'Last &raquo;';
			$config['last_tag_open']  = '<li class="page-item page-link">';
			$config['last_tag_close'] = '</li>';

			$config['next_link']      = '&raquo;';
			$config['next_tag_open']  = '<li class="page-item page-link">';
			$config['next_tag_close'] = '</li>';

			$config['prev_link']      = '&laquo;';
			$config['prev_tag_open']  = '<li class="page-item page-link">';
			$config['prev_tag_close'] = '</li>';

			$config['cur_tag_open']  = '<li class="page-item active"><a class="page-link" href="">';
			$config['cur_tag_close'] = '</a></li>';

			$config['num_tag_open']  = '<li class="page-item page-link">';
			$config['num_tag_close'] = '</li>';
			
			$this->pagination->initialize($config);

			$data['namaLengkap']    = $data_session['namaLengkap'];
			$data['jabatan'] 	    = $data_session['namaJabatan'];
			$data['tanggal_daftar'] = $data_session['tanggal_daftar'];
			$data['avatar'] 		= $data_session['avatar'];
			$data['kantor'] 		= $data_session['nama_lab'];
			$data['last_login'] 	= $data_session['last_login'];
			$data['username'] 		= $data_session['username'];
			$data['id_lab'] 		= $data_session['lab_id'];
			$data['id'] 			= $data_session['id'];
			$data['hak_akses']		= $this->master->get_hak_akses('2');
			$data['kantor']			= $this->master->getKantor();
			$data['nama_menu']		= "Penawaran Order";
			
			$this->load->view('head', $data);
			$this->load->view('header', $data);


			$q = $this->master->getMenu($data_session['id']);
			$d = array();
			foreach ($q as $theData) {
				$d[] = $theData->segment;
			}

			if(!in_array($this->uri->segment('2'), $d)){
				redirect(site_url('dashboard'));
			}
			
			$dataMaster = $this->m_penawaran->getPenawaran($filter, $config["per_page"], $page, $data_session);
			$data['onLaporan'] = $dataMaster;
			
			// Setting Pagination -------------------------
			$cur_page = $page;
			if ($cur_page == 0) {
				$cur_page = 1;
			} else {
				$cur_page = $page;
			}

			if($cur_page == 1) {
				$start = $cur_page;
			} else {
				if ($config["total_rows"] == 1) {
					$start = 1;
				} else {
					$start = (($cur_page - 1) * $this->pagination->per_page) + 1;
				}
			}

			if($cur_page * $this->pagination->per_page > $config["total_rows"]) {
				$end = $config["total_rows"];
			} else {
				if ($config["total_rows"] == 1) {
					$end = 1;
				} else {
					$end = (($cur_page * $this->pagination->per_page));
				}
			}
			
			
			if(isset($_GET['cari'])) {	
				if(count($dataMaster) == 10) {
					$data["links"] = $this->pagination->create_links();
				} else if($this->uri->segment(4) != ""){
					if($this->uri->segment(4) > 1){
						$data["links"] = $this->pagination->create_links();
					}
				} 
				if(count($dataMaster) == 0) {
					$data["links"] = "";
					$data['pagination_info'] = "Data Tidak Ditemukan!";
				} else {

					$data["links"] = $this->pagination->create_links();
					$data['pagination_info'] = "Menampilkan ".$start." s/d ".$end." dari ".$config["total_rows"]." total data";
				}
			} else {
				if(count($dataMaster) != 0) {
					$data["links"] = $this->pagination->create_links();
					$data['pagination_info'] = "Menampilkan ".$start." s/d ".$end." dari ".$config["total_rows"]." total data";
				} else {
					$data["links"] = "";
					$data['pagination_info'] = "";
				}
			}

			$this->load->view('transaksi/penawaran/v_index_data', $data);
			$this->load->view('footer');
		}
	}
	
	public function create() {
		
		if($this->session->userdata('login_sess') == ""){
			$this->load->view('login');
		} else {
			$data_session = $this->session->userdata('login_sess');
			$data = array('menu'  => $this->master->menu(0));
			
			$data['namaLengkap']    = $data_session['namaLengkap'];
			$data['jabatan'] 	    = $data_session['namaJabatan'];
			$data['tanggal_daftar'] = $data_session['tanggal_daftar'];
			$data['avatar'] 		= $data_session['avatar'];
			$data['kantor'] 		= $data_session['nama_lab'];
			$data['last_login'] 	= $data_session['last_login'];
			$data['username'] 		= $data_session['username'];
			$data['id_lab'] 		= $data_session['lab_id'];
			$data['id'] 			= $data_session['id'];
			$data['hak_akses']		= $this->master->get_hak_akses('2');
			$data['kantor']			= $this->master->getKantor();
			$data['nama_menu']		= "Penawaran Order";

			$this->load->view('head', $data);
			$this->load->view('header', $data);
			
			$q = $this->master->getMenu($data_session['id']);
			$d = array();
			foreach ($q as $theData) {
				$d[] = $theData->segment;
			}

			if(!in_array($this->uri->segment('2'), $d)){
				redirect(site_url('dashboard'));
			}
			
			$data['v_step1'] = $this->load->view('transaksi/penawaran/v_step1', NULL, TRUE);

			$this->load->view('transaksi/penawaran/v_create', $data);
			$this->load->view('footer');
		}
	}
	
	public function lookupPelanggan() {
		$this->load->view('transaksi/penawaran/v_lihat_pelanggan');
	}
	
	public function getDataPelanggan() {
		
		$filter = $_GET;
			
		if($_SERVER['QUERY_STRING'] != "") {
			$q_string = "?".$_SERVER['QUERY_STRING'];
		} else {
			$q_string = "";
		}

		$per_page = 10;

		if($this->uri->segment(4) != ""){
			$page = ($this->uri->segment(4)) ;
			$poss = (($page-1) * $per_page);
		} else {
			$page = 1;
			$poss = 0;
		}

		$config = array();
		$config["base_url"] 		= base_url() . "/transaksi/penawaran/getDataPelanggan/";
		$config["suffix"] 			= '/'.$q_string;
		$config["first_url"] 		= base_url() . "/transaksi/penawaran/getDataPelanggan/1/".$q_string;
		$config["per_page"] 		= $per_page;
		$config["total_rows"] 		= $this->m_pelanggan->record_count($filter)->numrows;
		$config["uri_segment"] 		= 4;
		$choice = $config["total_rows"] / $config["per_page"];
		$config["num_links"] 		= 2;//round($choice);
		$config['use_page_numbers'] = TRUE;
		$config['anchor_class'] 	= 'class="number" ';
		$config['full_tag_open']  	= '<ul class="pagination pagination-sm m-0 float-right">';
		$config['full_tag_close'] 	= '</ul><!--pagination-->';

		$config['first_link']      = '&laquo; First';
		$config['first_tag_open']  = '<li class="page-item page-link">';
		$config['first_tag_close'] = '</li>';

		$config['last_link']      = 'Last &raquo;';
		$config['last_tag_open']  = '<li class="page-item page-link">';
		$config['last_tag_close'] = '</li>';

		$config['next_link']      = '&raquo;';
		$config['next_tag_open']  = '<li class="page-item page-link onGo">';
		$config['next_tag_close'] = '</li>';

		$config['prev_link']      = '&laquo;';
		$config['prev_tag_open']  = '<li class="page-item page-link onGo">';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open']  = '<li class="page-item active"><a class="page-link" href="">';
		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open']  = '<li class="page-item page-link onGo">';
		$config['num_tag_close'] = '</li>';

		$this->pagination->initialize($config);
		
		$dataMaster = $this->m_pelanggan->getPelanggan($filter, $config["per_page"], $page);

		// Setting Pagination -------------------------
		$cur_page = $page;
		if ($cur_page == 0) {
			$cur_page = 1;
		} else {
			$cur_page = $page;
		}

		if($cur_page == 1) {
			$start = $cur_page;
		} else {
			if ($config["total_rows"] == 1) {
				$start = 1;
			} else {
				$start = (($cur_page - 1) * $this->pagination->per_page) + 1;
			}
		}

		if($cur_page * $this->pagination->per_page > $config["total_rows"]) {
			$end = $config["total_rows"];
		} else {
			if ($config["total_rows"] == 1) {
				$end = 1;
			} else {
				$end = (($cur_page * $this->pagination->per_page));
			}
		}


		if(isset($_GET['cari'])) {	
			if(count($dataMaster) == 10) {
				$data["links"] = $this->pagination->create_links();
			} else if($this->uri->segment(4) != ""){
				if($this->uri->segment(4) > 1){
					$data["links"] = $this->pagination->create_links();
				}
			} 
			if(count($dataMaster) == 0) {
				$data["links"] = "";
				$data['pagination_info'] = "Data Tidak Ditemukan!";
			} else {

				$data["links"] = $this->pagination->create_links();
				$data['pagination_info'] = "Menampilkan ".$start." s/d ".$end." dari ".$config["total_rows"]." total data";
			}
		} else {
			if(count($dataMaster) != 0) {
				$data["links"] = $this->pagination->create_links();
				$data['pagination_info'] = "Menampilkan ".$start." s/d ".$end." dari ".$config["total_rows"]." total data";
			} else {
				$data["links"] = "";
				$data['pagination_info'] = "";
			}
		}
		
		$data_arr = array();
		$q = $dataMaster;
		
		$data_arr = [
			'onRow' => $poss,
			'onPagination' => $data,
			'onLaporan' => $q
		];
		
		echo json_encode($data_arr, JSON_PRETTY_PRINT);
	}
	
	public function getDataDetailPelanggan() {
		
		$filter = $_GET;
		
		$data_arr = array();
		$data_arr = $this->m_pelanggan->getDetailPelanggan($filter);
		
		echo json_encode($data_arr, JSON_PRETTY_PRINT);
	}
	
	public function addKomoditi() {
		$this->load->view('transaksi/penawaran/v_add_komoditi');
	}
	
	public function excel()
    {
    	$data_session = $this->session->userdata('login_sess'); 
		$kantor       =  $data_session['KANTOR'];
        $filter = $this->input->get();
				
		if(isset($_GET['tampil_data'])) {
			$per_page = $_GET['tampil_data'];
		} else {
			$per_page = -1;
		}
		
		if($this->uri->segment(4) != ""){
			$page = ($this->uri->segment(4)) ;
		} else {
			$page = 1;
		}
		
        $rawData = $this->m_lapor_kendala->getLaporKendala($filter, $per_page, $page);

        // Pengganti Foreach
        $data = array_map(function ($val) {
            return array(
                $val->Name,
				$val->plat_nomor,
                $val->jenis_laporan,
                $val->keterangan,
                date('d-m-Y', strtotime($val->created_at)),
				$val->status_proved == 1 ? $val->keterangan_proved : "Pending",
				$val->status_proved == 1 ? $val->NameProved : "Pending",
				$val->status_proved == 1 ? date('d-m-Y', strtotime($val->date_proved)) : "Pending",
            );
        }, $rawData);

        // print_r($rawData); 
        // die();

        $excel = array(
            'filename' => 'lapor_kendala',
            'header'   => 'Rekap Lapor Kendala',
            'field'    => ['Pelapor', 'No. Polisi', 'Jenis Laporan', 'Keterangan', 'Tanggal', 'Konfirmasi Admin', 'Admin', 'Tanggal Konfirmasi'],
            'data'     => $data,
        );

        $this->load->view('v_excel_base', $excel);
    }
}

?>