<?php if (!defined('BASEPATH')) {exit('No direct script access allowed');
}

class Order extends CI_Controller {

	public $title = "";

	function __construct() {
		parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
		$this->load->model('master');
		$this->load->model('m_dashboard');
		$this->load->model('m_barang');
		$this->load->model('m_order');
	}

	public function v_data() {

		if ($this->session->userdata('login_sess') == "") {
			$this->load->view('login');
		} else {

			$data['menu']  = $this->master->menu(0);

			$data_session           = $this->session->userdata('login_sess');
			$data['namaLengkap']    = $data_session['namaLengkap'];
			$data['jabatan'] 	    = $data_session['namaJabatan'];
			$data['tanggal_daftar'] = $data_session['tanggal_daftar'];
			$data['avatar'] 		= $data_session['avatar'];
			$data['kantor'] 		= $data_session['namaKantor'];
			$data['last_login'] 	= $data_session['last_login'];
			$data['USERNAME'] 		= $data_session['USERNAME'];
			$data['id_kantor'] 		= $data_session['KANTOR'];
			$data['hak_akses']		= $this->master->get_hak_akses('2');
			$data['kantor']			= $this->master->getKantor($data_session['id_perusahaan']);
			$data['ID'] 			= $data_session['ID'];
			$data['nama_menu']		= "Pengiriman Barang";
			

			$this->load->view('head', $data);
			$this->load->view('header', $data);

			$q = $this->master->getMenu($data_session['ID']);
			foreach ($q as $theData) {
				$d[] = $theData->segment;
			}

			$data['customer'] = $this->master->getCustomer($data_session['id_perusahaan']);
			$data['barang']	  = $this->m_barang->get_barang_ajax($data_session['KANTOR'], $data_session['id_perusahaan'], "");
			$data['driver']   = $this->db->where('IDPosition', 4)
										 ->where('id_perusahaan', $data_session['id_perusahaan'])
										 ->from('users')
										 ->get()
										 ->result();
			$data['autoOrderNumber'] = $this->m_order->autoOrderNumber($data_session['KANTOR'], $data_session['id_perusahaan']);

			$this->load->view('transaksi/order/v_index_data', $data);
			$this->load->view('footer');

		}
	}
	
	public function simpan_order() {
		
		$data_session = $this->session->userdata('login_sess');
		
		$dataOrder = [
			'no_order' 		=> $_POST['no_faktur'],
			'id_user_maker' => $data_session['ID'],
			'id_driver' 	=> $_POST['id_driver'],
			'id_customer' 	=> $_POST['id_customer'],
			'kantor' 		=> $_POST['kantor'],
			'id_perusahaan' => $data_session['id_perusahaan'],
			'created_at' 	=> $_POST['tanggal_trans']
		];
	
		$this->db->insert("t_order", $dataOrder);
		$id_order = $this->db->insert_id();
		
		for ($i = 0; $i<count($_POST['detail_barang']); $i++) {
			$dataDetailOrder = [
				"id_order" 	  => $id_order,
				"kode_barang" => $_POST['detail_barang'][$i]['kode_barang'],
				"jumlah" 	  => $_POST['detail_barang'][$i]['jumlah'],
				"jumlah_pcs"  => $_POST['detail_barang'][$i]['jumlah_pcs'],
				"satuan" 	  => $_POST['detail_barang'][$i]['satuan'],
				"kantor" 	  => $_POST['kantor'],
				"id_perusahaan" => $data_session['id_perusahaan'],
				"created_at"  => $_POST['tanggal_trans']
			];
			$this->db->insert("t_order_detail", $dataDetailOrder);
		}
		
		$dataToken = array();
		$dataUser = $this->db->from('users')->where('id', $_POST['id_driver'])->get()->result();
		foreach($dataUser as $data) {
			$dataToken[] = $data->FirebaseToken;
		}
		
		$dataCountOrder = $this->db->from('t_order')
								   ->select('count(*) as total')
								   ->where('id_driver', $_POST['id_driver'])
								   ->where('status !=', 'Terkirim')
								   ->get()->row();
		
		$dataMsg   = array(
			"body"  => $dataCountOrder->total." daftar pengiriman barang",
			"title" => "TrakTruk",
			"sound" => "default",
			"modul" => "order",
			"targetClass" => "igdevelop.trackmobile_driver.MainPage.MainTab.Home.OrderList.OrderList"
		);

		$dataNotf   = array(
			"body"  => $dataCountOrder->total." daftar pengiriman barang",
			"title" => "TrakTruk",
			"sound" => "default",
			"modul" => "order",
			"targetClass" => "igdevelop.trackmobile_driver.MainPage.MainTab.Home.OrderList.OrderList"
		);

		$msg = $this->master->sendNotification($dataToken, $dataMsg, $dataNotf);
		
		echo json_encode("oke");
	}

}
