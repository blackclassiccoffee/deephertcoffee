<?php if (!defined('BASEPATH')) {exit('No direct script access allowed');
}

class Chat extends CI_Controller {

	public $title = "";

	function __construct() {
		parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
		$this->load->model('master');
		$this->load->model('m_dashboard');
	}

	public function v_data() {

		if ($this->session->userdata('login_sess') == "") {
			$this->load->view('login');
		} else {

			$data['menu']  = $this->master->menu(0);

			$data_session           = $this->session->userdata('login_sess');
			$data['namaLengkap']    = $data_session['namaLengkap'];
			$data['jabatan'] 	    = $data_session['namaJabatan'];
			$data['tanggal_daftar'] = $data_session['tanggal_daftar'];
			$data['avatar'] 		= $data_session['avatar'];
			$data['kantor'] 		= $data_session['namaKantor'];
			$data['last_login'] 	= $data_session['last_login'];
			$data['USERNAME'] 		= $data_session['USERNAME'];
			$data['id_kantor'] 		= $data_session['KANTOR'];
			$data['ID'] 			= $data_session['ID'];
			$data['nama_menu']		= "Chat";
			

			$this->load->view('head', $data);
			$this->load->view('header', $data);

			$q = $this->master->getMenu($data_session['ID']);
			foreach ($q as $theData) {
				$d[] = $theData->segment;
			}

			$data_dash = array(
				'Hallo' => 'Hallo'
			);

			$this->load->view('app/chat/v_index', $data);
			$this->load->view('footer');

		}
	}
	

}
