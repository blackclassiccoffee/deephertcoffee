<?php

class locator_armada extends CI_Controller {

	function __construct(){
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");       
        $this->load->model('master');
        $this->load->model('m_p_user');
		$this->load->model('m_history');
		$this->load->model('m_info');
		$this->load->model('m_analisa_track');
        $this->load->helper('url');
		//$this->load->library('guzzle');
    }
	
	public function v_data($id = ""){
		if($this->session->userdata('login_sess') == ""){
			$this->load->view('login');
		} else {
			$data_session = $this->session->userdata('login_sess');
			$data = array('menu'  => $this->master->menu(0));
			
			$filter = $this->input->get();
			
			$data['namaLengkap']    = $data_session['namaLengkap'];
			$data['jabatan'] 	    = $data_session['namaJabatan'];
			$data['tanggal_daftar'] = $data_session['tanggal_daftar'];
			$data['avatar'] 		= $data_session['avatar'];
			$data['kantor'] 		= $data_session['namaKantor'];
			$data['last_login'] 	= $data_session['last_login'];
			$data['USERNAME'] 		= $data_session['USERNAME'];
			$data['KANTOR'] 		= $data_session['KANTOR'];
			$data['id_kantor'] 		= $data_session['KANTOR'];
			$data['ID'] 			= $data_session['ID'];
			$data['LEVEL'] 			= $data_session['LEVEL'];
			$data['id_perusahaan'] 	= $data_session['id_perusahaan'];
			$data['hak_akses']		= $this->master->get_hak_akses('2');
			$data['kantor']			= $this->master->getKantor($data_session['id_perusahaan']);
			$data['jenis_laporan']  = $this->db->get('t_jenis_laporan')->result();
			$data['nama_menu']		= "Locator Armada";
			
			$this->load->view('head', $data);
			$this->load->view('header', $data);


			$q = $this->master->getMenu($data_session['ID']);
			$d = array();
			foreach ($q as $theData) {
				$d[] = $theData->segment;
			}

			if(!in_array($this->uri->segment('2'), $d)){
				redirect(site_url('dashboard'));
			}
			
			$data['Users'] = $this->db->where('IDPosition', 4)->get('users')->result();
			$this->load->view('app/locator_armada/v_index_data', $data);
			$this->load->view('footer');
			
		}
	}
	
	public function lihat_track() {
		$data['lat'] = $_GET['lat'];
		$data['lng'] = $_GET['lng'];
		$data['id_user'] = $_GET['id_user'];
		//$data['track'] = $this->readData($_GET['id_user']);
		$this->load->view('app/locator_armada/v_lihat_track', $data);
	}
	
	/*public function readData($id_user){
		
		$dataPoint = $this->m_analisa_track->getTrackUser($id_user);
		
		foreach ($dataPoint as $dataP) {
			$datax[] = array('lat' => $dataP->lat, 'lng' => $dataP->lng);
		}
			
		$i = 0;
		$dataOnDes = array();
		$dataDest = array();
		$dataAdd = array();
		foreach ($dataPoint as $dataP) {
			$onUrl = "";
			if ($i + 1 < count($datax)) {
				
				if ($i == 0) {
					$client = new GuzzleHttp\Client();
					$onUrl = 'https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins='.$dataP->lat.','.$dataP->lng.'&destinations='.$datax[$i+1]['lat'].','.$datax[$i+1]['lng'].'&key=AIzaSyDZGwxW7V88rFuwhVcPTW5TdvMG5JKeOuo';
					$response = $client->request('POST', $onUrl);
				} else {
					$client = new GuzzleHttp\Client();
					$onUrl = 'https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins='.$dataP->lat.','.$dataP->lng.'&destinations='.$datax[$i+1]['lat'].','.$datax[$i+1]['lng'].'&key=AIzaSyDZGwxW7V88rFuwhVcPTW5TdvMG5JKeOuo';
					$response = $client->request('POST', $onUrl);
				}
				
			}
			
			if (isset($response)) {
				$dataMap = json_decode($response->getBody());


				foreach ($dataMap->rows as $dataRow) {
					foreach($dataRow->elements as $dataElm) {
						$dataAdd[] = array(
							"origin" => $dataMap->origin_addresses[0], 
							"dest"   => $dataMap->destination_addresses[0],
							"km"	 => $dataElm->distance->value
						);
					}
				}
				
				$dataOnDes = $dataAdd;
			} else {
				$dataOnDes = [];
			}
			
			$i++;
		}
		
		return $dataOnDes;
	}*/
	
	public function getTrack() {
		$dataPoint = $this->m_analisa_track->getTrack($_POST['id_user'], $_POST['tanggal_awal'], $_POST['tanggal_akhir']);
		
		$dataD = [];
		foreach ($dataPoint as $dataP) {
			$dataD[] = array(
				'lat' => $dataP->lat, 
				'lng' => $dataP->lng,
				'created_at' => $dataP->created_at
			);
		}
		
		$dataRes = [];
		$i = 0;
		foreach ($dataPoint as $dataT) {
			
			$i++;
			
			if ($i < count($dataD)) {
				
				if ($dataT->lat != $dataD[$i]['lat'] && $dataT->lng != $dataD[$i]['lng']) {
			
					$origin_date = new DateTime($dataT->created_at);
					$dest_date = new DateTime($dataD[$i]['created_at']);
					$interval = $origin_date->diff($dest_date);
					
					$run_time = "";
					$per_hari = 24; // 1 hari 24 Jam
					$per_jam  = 60; // 1 Jam 60 Menit
					$in_hour  = 0;
					if ($interval->days != 0) {
						if ($interval->h != 0) {
							if ($interval->i != 0) {
								$run_time = $interval->days." hari ".$interval->h." jam ".$interval->i." menit";
								$in_hour  = ($interval->days * $per_hari) + $interval->h + ($interval->i / $per_jam);
							}
						}
					} else {
						if ($interval->h != 0) {
							if ($interval->i != 0) {
								$run_time = $interval->h." jam ".$interval->i." menit";
								$in_hour  = $interval->h + ($interval->i / $per_jam);
							}
						} else {
							if ($interval->i != 0) {
								$run_time = $interval->i." menit";
								$in_hour  = ($interval->i / $per_jam);
							}
						}
					}
					
					$dataRes[] = array(
						'origin_lat' => $dataT->lat,
						'origin_lng' => $dataT->lng,
						'origin_date' => $dataT->created_at,
						'dest_lat'	=> $dataD[$i]['lat'],
						'dest_lng'  => $dataD[$i]['lng'],
						'dest_date' => $dataD[$i]['created_at'],
						'run_time' => $run_time,
						'in_hour' => $in_hour
					);
					
				}
			}
		}
		
		echo json_encode($dataRes, JSON_PRETTY_PRINT);
	}
	
	public function getTrackLocPath() {
		$this->db->from('t_users_location_history as TH');
		$this->db->where('TH.id_user', $_POST['id_user']);
		
		if (isset($_POST['dateTrack1']) && $_POST['dateTrack1'] != "") {
			if (isset($_POST['dateTrack2']) && $_POST['dateTrack2'] != "") {
				$this->db->where('DATE(TH.created_at) >=', $_POST['dateTrack1']);
			} else {
				$this->db->where('DATE(TH.created_at)', $_POST['dateTrack1']);
			}
		}
		
		if (isset($_POST['dateTrack2']) && $_POST['dateTrack2'] != "") {
			$this->db->where('DATE(TH.created_at) <=', $_POST['dateTrack2']);
		}
		
		$q = $this->db->get()->result_array();
		
		echo json_encode($q, JSON_PRETTY_PRINT);
	}
	
	public function getArmadaPos() {
		$this->db->select('
			TL.*,
			(SELECT TH.lat FROM t_users_location_history TH WHERE TH.id_user = TL.id_user ORDER BY TH.id DESC LIMIT 1) as lat_new,
			(SELECT TH.lng FROM t_users_location_history TH WHERE TH.id_user = TL.id_user ORDER BY TH.id DESC LIMIT 1) as lng_new,
			U.Name, 
			TA.no_polisi
		');
		$this->db->from('t_users_location TL')
			->join('users U', 'TL.id_user = U.id', 'LEFT')
			->join('t_armada TA', 'TL.id_user = TA.id_user', 'LEFT');
		
		$this->db->where('U.IDPosition', 4);
		if (isset($_POST['id_user']) && $_POST['id_user'] != "") {
			$this->db->where('U.id', $_POST['id_user']);
		}
		
		$q = $this->db->get()->result_array();
		
		echo json_encode($q, JSON_PRETTY_PRINT);
	}
}

?>