<?php if (!defined('BASEPATH')) {exit('No direct script access allowed');
}

class Dashboard extends CI_Controller {

	public $title = "";

	function __construct() {
		parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
		$this->load->model('master');
		$this->load->model('m_dashboard');
	}

	public function index() {

		if ($this->session->userdata('login_sess') == "") {
			$this->load->view('login');
		} else {

			$data['menu']  = $this->master->menu(0);

			$data_session           = $this->session->userdata('login_sess');
			$data['namaLengkap']    = $data_session['namaLengkap'];
			$data['jabatan'] 	    = $data_session['namaJabatan'];
			$data['tanggal_daftar'] = $data_session['tanggal_daftar'];
			$data['avatar'] 		= $data_session['avatar'];
			$data['kantor'] 		= $data_session['nama_lab'];
			$data['last_login'] 	= $data_session['last_login'];
			$data['username'] 		= $data_session['username'];
			$data['id_lab'] 		= $data_session['lab_id'];
			$data['id'] 			= $data_session['id'];
			$data['nama_menu']		= "Dashboard";
			

			$this->load->view('head', $data);
			$this->load->view('header', $data);

			$q = $this->master->getMenu($data_session['id']);
			foreach ($q as $theData) {
				$d[] = $theData->segment;
			}

			$data_dash = array(
				'Hallo' => 'Hallo'
			);

			$this->load->view('home/dashboard', $data);
			$this->load->view('footer');

		}
	}
	
	public function getOmsetBulanan() {
		
		$dataOmset = [];
		for ($i = 1; $i <= 12; $i++) {
			$data = $this->m_dashboard->getDataGraphOmset($i, $_POST['tahun']);
			$dataOmset[] = array(
				'total' => $data->total == null ? 0 : $data->total
			);
		}
		echo json_encode($dataOmset, JSON_PRETTY_PRINT);
		
	}
	
	public function getLaporanBulanan() {
		
		$data[] = [
			'name'  => 'Bongkar/Muat',
			'dataTotal' => $this->m_dashboard->getDataGraphBongkarMuat($_POST['bulan'], $_POST['tahun'])
		];
		$data[] = [
			'name' => 'Tombol Darurat',
			'dataTotal' => $this->m_dashboard->getDataGraphPanic($_POST['bulan'], $_POST['tahun'])
		];
		$data[] = [
			'name' => 'Kendala',
			'dataTotal' => $this->m_dashboard->getDataGraphKendala($_POST['bulan'], $_POST['tahun'])
		];
		
		echo json_encode($data, JSON_PRETTY_PRINT);
		
	}

}
