<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class login extends CI_Controller {
	
	public $title = "";
	
	function __construct(){
        parent::__construct();  
        date_default_timezone_set("Asia/Jakarta");     
        $this->load->model('master');
        $this->load->model('m_login');
    }

	public function index(){
		$this->load->view('login');
		//$this->session->sess_destroy();
		
	}

	
}
