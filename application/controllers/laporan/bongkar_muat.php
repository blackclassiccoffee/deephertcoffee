<?php

class bongkar_muat extends CI_Controller {

	function __construct(){
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");       
        $this->load->model('master');
        $this->load->model('m_p_user');
		$this->load->model('m_history');
		$this->load->model('m_bongkar_muat');
		$this->load->model('m_analisa_track');
        $this->load->helper('url');
    }

	public function v_data($id = ""){

		if($this->session->userdata('login_sess') == ""){
			$this->load->view('login');
		} else {
			$data_session = $this->session->userdata('login_sess');
			$data = array('menu'  => $this->master->menu(0));
			
			$filter = $this->input->get();
			
			if($_SERVER['QUERY_STRING'] != "") {
				$q_string = "?".$_SERVER['QUERY_STRING'];
			} else {
				$q_string = "";
			}
			
			$per_page = 10;
			
			if($this->uri->segment(4) != ""){
				$page = ($this->uri->segment(4)) ;
			} else {
				$page = 1;
			}
			
			$config = array();
			$config["base_url"] 		= base_url() . "/laporan/bongkar_muat/v_data/";
			$config["suffix"] 			= '/'.$q_string;
			$config["first_url"] 		= base_url() . "/laporan/bongkar_muat/v_data/1/".$q_string;
			$config["per_page"] 		= $per_page;
			$config["total_rows"] 		= $this->m_bongkar_muat->record_count($filter)->numrows;
			$config["uri_segment"] 		= 4;
			$choice = $config["total_rows"] / $config["per_page"];
			$config["num_links"] 		= 2;//round($choice);
			$config['use_page_numbers'] = TRUE;
			$config['full_tag_open']  	= '<ul class="pagination pagination-sm m-0 float-right">';
			$config['full_tag_close'] 	= '</ul><!--pagination-->';

			$config['first_link']      = '&laquo; First';
			$config['first_tag_open']  = '<li class="page-item page-link">';
			$config['first_tag_close'] = '</li>';

			$config['last_link']      = 'Last &raquo;';
			$config['last_tag_open']  = '<li class="page-item page-link">';
			$config['last_tag_close'] = '</li>';

			$config['next_link']      = '&raquo;';
			$config['next_tag_open']  = '<li class="page-item page-link">';
			$config['next_tag_close'] = '</li>';

			$config['prev_link']      = '&laquo;';
			$config['prev_tag_open']  = '<li class="page-item page-link">';
			$config['prev_tag_close'] = '</li>';

			$config['cur_tag_open']  = '<li class="page-item active"><a class="page-link" href="">';
			$config['cur_tag_close'] = '</a></li>';

			$config['num_tag_open']  = '<li class="page-item page-link">';
			$config['num_tag_close'] = '</li>';
			
			$this->pagination->initialize($config);

			$data['namaLengkap']    = $data_session['namaLengkap'];
			$data['jabatan'] 	    = $data_session['namaJabatan'];
			$data['tanggal_daftar'] = $data_session['tanggal_daftar'];
			$data['avatar'] 		= $data_session['avatar'];
			$data['kantor'] 		= $data_session['namaKantor'];
			$data['last_login'] 	= $data_session['last_login'];
			$data['USERNAME'] 		= $data_session['USERNAME'];
			$data['KANTOR'] 		= $data_session['KANTOR'];
			$data['id_kantor'] 		= $data_session['KANTOR'];
			$data['ID'] 			= $data_session['ID'];
			$data['LEVEL'] 			= $data_session['LEVEL'];
			$data['hak_akses']		= $this->master->get_hak_akses('2');
			$data['kantor']			= $this->master->getKantor();
			$data['jenis_laporan']  = $this->db->get('t_jenis_laporan')->result();
			$data['distributor']    = $this->db->get('t_distributor')->result();
			$data['nama_menu']		= "Laporan Bongkar/Muat";
			
			$this->load->view('head', $data);
			$this->load->view('header', $data);


			$q = $this->master->getMenu($data_session['ID']);
			$d = array();
			foreach ($q as $theData) {
				$d[] = $theData->segment;
			}

			if(!in_array($this->uri->segment('2'), $d)){
				redirect(site_url('dashboard'));
			}
			
			$dataMaster = $this->m_bongkar_muat->getLaporBongkar($filter, $config["per_page"], $page);
			$data['onLaporan'] = $dataMaster;
			
			// Setting Pagination -------------------------
			$cur_page = $page;
			if ($cur_page == 0) {
				$cur_page = 1;
			} else {
				$cur_page = $page;
			}

			if($cur_page == 1) {
				$start = $cur_page;
			} else {
				if ($config["total_rows"] == 1) {
					$start = 1;
				} else {
					$start = (($cur_page - 1) * $this->pagination->per_page) + 1;
				}
			}

			if($cur_page * $this->pagination->per_page > $config["total_rows"]) {
				$end = $config["total_rows"];
			} else {
				if ($config["total_rows"] == 1) {
					$end = 1;
				} else {
					$end = (($cur_page * $this->pagination->per_page));
				}
			}
			
			
			if(isset($_GET['cari'])) {	
				if(count($dataMaster) == 10) {
					$data["links"] = $this->pagination->create_links();
				} else if($this->uri->segment(4) != ""){
					if($this->uri->segment(4) > 1){
						$data["links"] = $this->pagination->create_links();
					}
				} 
				if(count($dataMaster) == 0) {
					$data["links"] = "";
					$data['pagination_info'] = "Data Tidak Ditemukan!";
				} else {

					$data["links"] = $this->pagination->create_links();
					$data['pagination_info'] = "Menampilkan ".$start." s/d ".$end." dari ".$config["total_rows"]." total data";
				}
			} else {
				if(count($dataMaster) != 0) {
					$data["links"] = $this->pagination->create_links();
					$data['pagination_info'] = "Menampilkan ".$start." s/d ".$end." dari ".$config["total_rows"]." total data";
				} else {
					$data["links"] = "";
					$data['pagination_info'] = "";
				}
			}

			$this->load->view('laporan/bongkar_muat/v_index_data', $data);
			$this->load->view('footer');
		}
	}
	
	public function lihat_peta() {
		$data['lat'] = $_GET['lat'];
		$data['lng'] = $_GET['lng'];
		$data['id_bongkar'] = $_GET['id_bongkar'];
		$data['id_user'] = $_GET['id_user'];
		$this->load->view('laporan/bongkar_muat/v_lihat_peta', $data);
	}
	
	public function lihat_grafik() {
		$data['url'] = $_GET;
		$this->load->view('laporan/bongkar_muat/v_lihat_grafik', $data);
	}
	
	public function getOmset() {
		
		$data = $this->m_bongkar_muat->getDataGraph();
		echo json_encode($data, JSON_PRETTY_PRINT);
		
	}
	
	public function konfirmasi_hapus() {
		$this->load->view('laporan/bongkar_muat/v_hapus');
	}
	
	public function deleteLaporan(){
		
		$this->db->where('id', $_POST['id']);
		$this->db->delete('t_bongkar_muat');
		
		if ($_POST['act'] == "reject") {
		
			$dataUser = $this->db->from('users')->where('id', $_POST['id_user'])->get()->result();
			$dataToken = array();
			foreach($dataUser as $data) {
				$dataToken[] = $data->FirebaseToken;
			}


			$dataMsg   = array(
				"body"  => "Laporan muatan anda dengan Nomor SPJ : ".$_POST['no_spj']." di tolak oleh Admin! dikarenakan ".$_POST['keterangan']."\n\nSilahkan untuk membuat laporan yang baru.",
				"title" => "Reject Muatan",
				"act"	=> "reject",
				"modul" => "bongkar_muat",
				"targetClass" => "igdevelop.trackmobile_driver.MainPage.MainTab.Home.LaporBongkarMuat.LaporBongkarMuat"
			);

			$dataNotf   = array(
				"body"  => "Laporan muatan anda dengan Nomor SPJ : ".$_POST['no_spj']." di tolak oleh Admin! dikarenakan ".$_POST['keterangan']."\n\nSilahkan untuk membuat laporan yang baru.",
				"title" => "Reject Muatan",
				"act"	=> "reject",
				"modul" => "bongkar_muat",
				"targetClass" => "igdevelop.trackmobile_driver.MainPage.MainTab.Home.LaporBongkarMuat.LaporBongkarMuat"
			);

			$msg = $this->master->sendNotification($dataToken, $dataMsg, $dataNotf);
			
		}

		$this->session->set_flashdata('oke', 'Data berhasil dihapus!');
		$data = [
			'status' => true
		];
		echo json_encode($data, JSON_PRETTY_PRINT);
	}
	
	public function approve_laporan(){
		
		$dataUser = $this->db->from('users')->where('id', $_POST['id_user'])->get()->result();
		$dataToken = array();
		foreach($dataUser as $data) {
			$dataToken[] = $data->FirebaseToken;
		}

		$dataMsg   = array(
			"body"  => "Laporan muatan anda dengan Nomor SPJ : ".$_POST['no_spj']." Telah disetujui oleh Admin!",
			"title" => "Konfirmasi Muatan",
			"act"	=> "approve",
			"modul" => "bongkar_muat",
			"targetClass" => "igdevelop.trackmobile_driver.MainPage.MainTab.Home.LaporBongkarMuat.LaporBongkarMuat"
		);

		$dataNotf   = array(
			"body"  => "Laporan muatan anda dengan Nomor SPJ : ".$_POST['no_spj']." Telah disetujui oleh Admin!",
			"title" => "Konfirmasi Muatan",
			"act"	=> "approve",
			"modul" => "bongkar_muat",
			"targetClass" => "igdevelop.trackmobile_driver.MainPage.MainTab.Home.LaporBongkarMuat.LaporBongkarMuat"
		);

		$msg = $this->master->sendNotification($dataToken, $dataMsg, $dataNotf);

		$data = [
			'status' => true
		];
		echo json_encode($data, JSON_PRETTY_PRINT);
	}
	
	public function konfirmasi() {
		
		$this->db->select('TL.*, U.Name, TA.plat_nomor, TUL.lat, TUL.lng');
		$this->db->from('t_bongkar_muat as TL')
			->join('users U', 'TL.id_user = U.id')
			->join('t_armada TA', 'TL.id_user = TA.id_user')
			->join('t_users_location TUL', 'TL.id_user = TUL.id_user');
		$this->db->where('TL.id', $_GET['id']);
		$q = $this->db->get()->row();
		
		$data['onLap'] = $q;
		$this->load->view('laporan/bongkar_muat/v_konfirmasi', $data);
	}
	
	public function edit_data() {
		
		$this->db->select('TL.*, U.Name, TA.plat_nomor, TUL.lat, TUL.lng');
		$this->db->from('t_bongkar_muat as TL')
			->join('users U', 'TL.id_user = U.id')
			->join('t_armada TA', 'TL.id_user = TA.id_user')
			->join('t_users_location TUL', 'TL.id_user = TUL.id_user');
		$this->db->where('TL.id', $_GET['id']);
		$q = $this->db->get()->row();
		
		$data['onLap'] = $q;
		$this->load->view('laporan/bongkar_muat/v_konfirmasi_edit', $data);
	}
	
	public function lihat_data() {
		
		$this->db->select('TL.*, U.Name, TA.plat_nomor, TUL.lat, TUL.lng');
		$this->db->from('t_bongkar_muat as TL')
			->join('users U', 'TL.id_user = U.id')
			->join('t_armada TA', 'TL.id_user = TA.id_user')
			->join('t_users_location TUL', 'TL.id_user = TUL.id_user');
		$this->db->where('TL.id', $_GET['id']);
		$q = $this->db->get()->row();
		
		$data['onLap'] = $q;
		$this->load->view('laporan/bongkar_muat/v_lihat_data', $data);
	}

	public function simpan_konfirmasi() {
		
		$data_session = $this->session->userdata('login_sess');
		
		$data = [
			'ongkos_angkut' => (int) str_replace('.', '', $_POST['ongkos_angkut']),
			'total'			=> (( $_POST['tonase_bongkar'] * str_replace('.', '', $_POST['ongkos_angkut']) ) - $_POST['uang_jalan'])
		];
		
		$this->db->where('id', $_POST['id']);
        $this->db->update('t_bongkar_muat', $data);
		
		echo json_encode('sukses');
	}
	
	public function getTrackLocPath() {
		$this->db->from('t_users_location_history as TH');
		$this->db->where('TH.id_user', $_POST['id_user']);
		$this->db->where('TH.id_bongkar', $_POST['id_bongkar']);
		$q = $this->db->get()->result_array();
		
		echo json_encode($q, JSON_PRETTY_PRINT);
	}
	
	public function excel()
    {
    	$data_session = $this->session->userdata('login_sess'); 
		$kantor       =  $data_session['KANTOR'];
        $filter = $this->input->get();
				
		if(isset($_GET['tampil_data'])) {
			$per_page = $_GET['tampil_data'];
		} else {
			$per_page = -1;
		}
		
		if($this->uri->segment(4) != ""){
			$page = ($this->uri->segment(4)) ;
		} else {
			$page = 1;
		}
		
        $rawData = $this->m_bongkar_muat->getLaporBongkar($filter, $per_page, $page);

        // Pengganti Foreach
        $data = array_map(function ($val) {
            return array(
				date('d-m-Y', strtotime($val->created_at)),
                $val->Name,
				$val->plat_nomor,
				$val->nama_distributor,
				$val->no_spj,
				$val->tonase_muat,
				$val->tonase_bongkar,
				$val->asal,
				$val->tujuan,
				$val->uang_jalan,
				$val->ongkos_angkut,
				(( $val->tonase_bongkar * $val->ongkos_angkut ) - $val->uang_jalan)
            );
        }, $rawData);

        // print_r($rawData); 
        // die();

        $excel = array(
            'filename' => 'bongkar_muat',
            'header'   => 'Rekap Bongkar Muat',
            'field'    => ['Tanggal', 'Nama Sopir', 'No. Polisi', 'Distributor', 'No. SPJ', 'Tonase Muat', 'Tonase Bongkar', 'Asal', 'Tujuan', 'Uang Jalan', 'Ongkos Angkut', 'Total'],
            'data'     => $data,
        );

        $this->load->view('v_excel_base', $excel);
    }
}

?>