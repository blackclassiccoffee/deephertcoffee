<?php

class lapor_n extends CI_Controller {

	function __construct(){
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");       
        $this->load->model('master');
        $this->load->model('m_p_user');
		$this->load->model('m_history');
		$this->load->model('m_lapor_kendala');
        $this->load->helper('url');
		//$this->load->library('guzzle');
    }

	public function getNotif() {
		$dataPanic = $this->db->select('COUNT(*) as total')
			->from('t_panic')
			->where('status_readed', 0)
			->where('status_proved', 0)
			->get()
			->row();
		
		$dataKendala = $this->db->select('COUNT(*) as total')
			->from('t_laporan_kendala')
			->where('status_readed', 0)
			->where('status_proved', 0)
			->get()
			->row();
		
		$dataBongkar = $this->db->select('COUNT(*) as total')
			->from('t_bongkar_muat')
			->where('status_readed', 0)
			->get()
			->row();
		
		
		$data = [
			'dataPanic'   => $dataPanic->total,
			'dataKendala' => $dataKendala->total,
			'dataBongkar' => $dataBongkar->total
		];
		
		echo json_encode($data, JSON_PRETTY_PRINT);
	}
	
	public function getPanic() {
		
		$this->db->select('*');
		$this->db->from('t_panic TP');
		$this->db->where('TP.status_notif', 0);
		$q = $this->db->get()->result();
		
		echo json_encode($q, JSON_PRETTY_PRINT);
		
	}
	
	public function updatePanicReaded() {
		$data_p = [
			'status_notif' => 1
		];
		
		$this->db->where('status_notif', 0);
		$this->db->update('t_panic', $data_p);
		
		$data_p = [
			'status_readed' => 1
		];
		
		$this->db->where('status_readed', 0);
		$this->db->update('t_panic', $data_p);
	}
	
	public function getBongkarMuat() {
		
		$this->db->select('*');
		$this->db->from('t_bongkar_muat TP');
		$this->db->where('TP.status_notif', 0);
		$q = $this->db->get()->result();
		
		echo json_encode($q, JSON_PRETTY_PRINT);
		
	}
	
	public function updateBongkarReaded() {
		
		$data_p = [
			'status_notif' => 1
		];
		
		$this->db->where('status_notif', 0);
		$this->db->update('t_bongkar_muat', $data_p);
		
		$data_p = [
			'status_readed' => 1
		];
		
		$this->db->where('status_readed', 0);
		$this->db->update('t_bongkar_muat', $data_p);
	}
	
	public function updateKendalaReaded() {
		
		$data_p = [
			'status_readed' => 1
		];
		
		$this->db->where('status_readed', 0);
		$this->db->update('t_laporan_kendala', $data_p);
	}
	
}

?>