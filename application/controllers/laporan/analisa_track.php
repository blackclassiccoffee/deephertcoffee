<?php

class analisa_track extends CI_Controller {

	function __construct(){
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");       
        $this->load->model('master');
        $this->load->model('m_p_user');
		$this->load->model('m_history');
		$this->load->model('m_analisa_track');
        $this->load->helper('url');
		$this->load->library('guzzle');
    }

	public function v_data($id = ""){

		if($this->session->userdata('login_sess') == ""){
			$this->load->view('login');
		} else {
			$data_session = $this->session->userdata('login_sess');
			$data = array('menu'  => $this->master->menu(0));
			
			$filter = $this->input->get();

			$data['namaLengkap']    = $data_session['namaLengkap'];
			$data['jabatan'] 	    = $data_session['namaJabatan'];
			$data['tanggal_daftar'] = $data_session['tanggal_daftar'];
			$data['avatar'] 		= $data_session['avatar'];
			$data['kantor'] 		= $data_session['namaKantor'];
			$data['last_login'] 	= $data_session['last_login'];
			$data['USERNAME'] 		= $data_session['USERNAME'];
			$data['KANTOR'] 		= $data_session['KANTOR'];
			$data['id_kantor'] 		= $data_session['KANTOR'];
			$data['ID'] 			= $data_session['ID'];
			$data['LEVEL'] 			= $data_session['LEVEL'];
			$data['hak_akses']		= $this->master->get_hak_akses('2');
			$data['nama_menu']		= "Analisa Track";
			
			$this->load->view('head', $data);
			$this->load->view('header', $data);


			$q = $this->master->getMenu($data_session['ID']);
			$d = array();
			foreach ($q as $theData) {
				$d[] = $theData->segment;
			}

			if(!in_array($this->uri->segment('2'), $d)){
				redirect(site_url('dashboard'));
			}
			
			
			$data['dataDes'] = $this->readData();
			$data['onTrayek'] = $this->db->from('t_users_location_history')
								->where('id_trayek', 1)
								->where('id_point !=', 0)
								->get()
								->result();

			$this->load->view('laporan/analisa_track/v_index_analisa', $data);
			$this->load->view('footer');
		}
	}
	
	public function readData(){
		
		
		$dataPoint = $this->m_analisa_track->getPoint();
		
		foreach ($dataPoint as $dataP) {
			$datax[] = array('lat' => $dataP->lat, 'lng' => $dataP->lng);
		}
			
		$i = 0;
		$dataOnDes = array();
		$dataDest = array();
		foreach ($dataPoint as $dataP) {
			$onUrl = "";
			if ($i + 1 < count($datax)) {
				
				if ($i == 0) {
					$client = new GuzzleHttp\Client();
					$onUrl = 'https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins='.$dataP->lat.','.$dataP->lng.'&destinations='.$datax[$i+1]['lat'].','.$datax[$i+1]['lng'].'&key=AIzaSyDZGwxW7V88rFuwhVcPTW5TdvMG5JKeOuo';
					$response = $client->request('POST', $onUrl);
				} else {
					$client = new GuzzleHttp\Client();
					$onUrl = 'https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins='.$dataP->lat.','.$dataP->lng.'&destinations='.$datax[$i+1]['lat'].','.$datax[$i+1]['lng'].'&key=AIzaSyDZGwxW7V88rFuwhVcPTW5TdvMG5JKeOuo';
					$response = $client->request('POST', $onUrl);
				}
				
			}
			
			$dataMap = json_decode($response->getBody());
			
			foreach ($dataMap->rows as $dataRow) {
				foreach($dataRow->elements as $dataElm) {
					$dataDest[] = $dataElm->distance->value;
				}
			}
			
			$dataOnRoad = $this->m_analisa_track->getOnRoad($dataP->id_trayek);
			$dataOnTime = array();
			foreach($dataOnRoad as $dOnRoad) {
				$dataOnTime[] = array(
					'timeArr'  => date('H:i:s', strtotime($dOnRoad->created_at)),
					'id_point' => $dOnRoad->id_point
				);
			}
			
			$dataOnDes = array(
				'dataDest' => $dataDest,
				'dataTime' => $dataOnTime
			);
			
			$i++;
		}
		
		return $dataOnDes;
	}

}

?>