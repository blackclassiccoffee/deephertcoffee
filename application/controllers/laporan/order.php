<?php

class order extends CI_Controller {

	function __construct(){
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");       
        $this->load->model('master');
        $this->load->model('m_p_user');
		$this->load->model('m_history');
		$this->load->model('m_order');
        $this->load->helper('url');
		//$this->load->library('guzzle');
    }

	public function v_data($id = ""){

		if($this->session->userdata('login_sess') == ""){
			$this->load->view('login');
		} else {
			$data_session = $this->session->userdata('login_sess');
			$data = array('menu'  => $this->master->menu(0));
			
			$filter = $this->input->get();
			
			if($_SERVER['QUERY_STRING'] != "") {
				$q_string = "?".$_SERVER['QUERY_STRING'];
			} else {
				$q_string = "";
			}
			
			$per_page = 10;
			
			if($this->uri->segment(4) != ""){
				$page = ($this->uri->segment(4)) ;
			} else {
				$page = 1;
			}
			
			$config = array();
			$config["base_url"] 		= base_url() . "/laporan/order/v_data/";
			$config["suffix"] 			= '/'.$q_string;
			$config["first_url"] 		= base_url() . "/laporan/order/v_data/1/".$q_string;
			$config["per_page"] 		= $per_page;
			$config["total_rows"] 		= $this->m_order->record_count($filter)->numrows;
			$config["uri_segment"] 		= 4;
			$choice = $config["total_rows"] / $config["per_page"];
			$config["num_links"] 		= 2;//round($choice);
			$config['use_page_numbers'] = TRUE;
			$config['full_tag_open']  	= '<ul class="pagination pagination-sm m-0 float-right">';
			$config['full_tag_close'] 	= '</ul><!--pagination-->';

			$config['first_link']      = '&laquo; First';
			$config['first_tag_open']  = '<li class="page-item page-link">';
			$config['first_tag_close'] = '</li>';

			$config['last_link']      = 'Last &raquo;';
			$config['last_tag_open']  = '<li class="page-item page-link">';
			$config['last_tag_close'] = '</li>';

			$config['next_link']      = '&raquo;';
			$config['next_tag_open']  = '<li class="page-item page-link">';
			$config['next_tag_close'] = '</li>';

			$config['prev_link']      = '&laquo;';
			$config['prev_tag_open']  = '<li class="page-item page-link">';
			$config['prev_tag_close'] = '</li>';

			$config['cur_tag_open']  = '<li class="page-item active"><a class="page-link" href="">';
			$config['cur_tag_close'] = '</a></li>';

			$config['num_tag_open']  = '<li class="page-item page-link">';
			$config['num_tag_close'] = '</li>';
			
			$this->pagination->initialize($config);

			$data['namaLengkap']    = $data_session['namaLengkap'];
			$data['jabatan'] 	    = $data_session['namaJabatan'];
			$data['tanggal_daftar'] = $data_session['tanggal_daftar'];
			$data['avatar'] 		= $data_session['avatar'];
			$data['kantor'] 		= $data_session['namaKantor'];
			$data['last_login'] 	= $data_session['last_login'];
			$data['USERNAME'] 		= $data_session['USERNAME'];
			$data['KANTOR'] 		= $data_session['KANTOR'];
			$data['id_kantor'] 		= $data_session['KANTOR'];
			$data['ID'] 			= $data_session['ID'];
			$data['LEVEL'] 			= $data_session['LEVEL'];
			$data['id_perusahaan'] 	= $data_session['id_perusahaan'];
			$data['hak_akses']		= $this->master->get_hak_akses('2');
			$data['kantor']			= $this->master->getKantor($data_session['id_perusahaan']);
			$data['jenis_laporan']  = $this->db->get('t_jenis_laporan')->result();
			$data['nama_menu']		= "Laporan Pengiriman Barang";
			
			$this->load->view('head', $data);
			$this->load->view('header', $data);


			$q = $this->master->getMenu($data_session['ID']);
			$d = array();
			foreach ($q as $theData) {
				$d[] = $theData->segment;
			}

			if(!in_array($this->uri->segment('2'), $d)){
				redirect(site_url('dashboard'));
			}
			
			$dataMaster = $this->m_order->getLaporanOrder($filter, $config["per_page"], $page, $data_session['id_perusahaan']);
			$data['onLaporan'] = $dataMaster;
			
			// Setting Pagination -------------------------
			$cur_page = $page;
			if ($cur_page == 0) {
				$cur_page = 1;
			} else {
				$cur_page = $page;
			}

			if($cur_page == 1) {
				$start = $cur_page;
			} else {
				if ($config["total_rows"] == 1) {
					$start = 1;
				} else {
					$start = (($cur_page - 1) * $this->pagination->per_page) + 1;
				}
			}

			if($cur_page * $this->pagination->per_page > $config["total_rows"]) {
				$end = $config["total_rows"];
			} else {
				if ($config["total_rows"] == 1) {
					$end = 1;
				} else {
					$end = (($cur_page * $this->pagination->per_page));
				}
			}
			
			
			if(isset($_GET['cari'])) {	
				if(count($dataMaster) == 10) {
					$data["links"] = $this->pagination->create_links();
				} else if($this->uri->segment(4) != ""){
					if($this->uri->segment(4) > 1){
						$data["links"] = $this->pagination->create_links();
					}
				} 
				if(count($dataMaster) == 0) {
					$data["links"] = "";
					$data['pagination_info'] = "Data Tidak Ditemukan!";
				} else {

					$data["links"] = $this->pagination->create_links();
					$data['pagination_info'] = "Menampilkan ".$start." s/d ".$end." dari ".$config["total_rows"]." total data";
				}
			} else {
				if(count($dataMaster) != 0) {
					$data["links"] = $this->pagination->create_links();
					$data['pagination_info'] = "Menampilkan ".$start." s/d ".$end." dari ".$config["total_rows"]." total data";
				} else {
					$data["links"] = "";
					$data['pagination_info'] = "";
				}
			}

			$this->load->view('laporan/order/v_index_data', $data);
			$this->load->view('footer');
		}
	}
	
	public function lihat_track() {
		$data['lat'] = $_GET['lat'];
		$data['lng'] = $_GET['lng'];
		$data['id_user'] = $_GET['id_user'];
		$data['id_bongkar'] = $_GET['id_bongkar'];
		//$data['track'] = $this->readData($_GET['id_user']);
		$this->load->view('laporan/order/v_lihat_track', $data);
	}
	
	public function getTrack() {
		$dataPoint = $this->m_order->getTrack($_POST['id_user'], $_POST['id_bongkar']);
		
		$dataD = [];
		foreach ($dataPoint as $dataP) {
			$dataD[] = array(
				'lat' => $dataP->lat, 
				'lng' => $dataP->lng,
				'created_at' => $dataP->created_at
			);
		}
		
		$dataRes = [];
		$i = 0;
		foreach ($dataPoint as $dataT) {
			
			$i++;
			
			if ($i < count($dataD)) {
				
				if ($dataT->lat != $dataD[$i]['lat'] && $dataT->lng != $dataD[$i]['lng']) {
			
					$origin_date = new DateTime($dataT->created_at);
					$dest_date = new DateTime($dataD[$i]['created_at']);
					$interval = $origin_date->diff($dest_date);
					
					$run_time = "";
					$per_hari = 24; // 1 hari 24 Jam
					$per_jam  = 60; // 1 Jam 60 Menit
					$in_hour  = 0;
					if ($interval->days != 0) {
						if ($interval->h != 0) {
							if ($interval->i != 0) {
								$run_time = $interval->days." hari ".$interval->h." jam ".$interval->i." menit";
								$in_hour  = ($interval->days * $per_hari) + $interval->h + ($interval->i / $per_jam);
							}
						}
					} else {
						if ($interval->h != 0) {
							if ($interval->i != 0) {
								$run_time = $interval->h." jam ".$interval->i." menit";
								$in_hour  = $interval->h + ($interval->i / $per_jam);
							}
						} else {
							if ($interval->i != 0) {
								$run_time = $interval->i." menit";
								$in_hour  = ($interval->i / $per_jam);
							}
						}
					}
					
					$dataRes[] = array(
						'origin_lat' => $dataT->lat,
						'origin_lng' => $dataT->lng,
						'origin_date' => $dataT->created_at,
						'dest_lat'	=> $dataD[$i]['lat'],
						'dest_lng'  => $dataD[$i]['lng'],
						'dest_date' => $dataD[$i]['created_at'],
						'run_time' => $run_time,
						'in_hour' => $in_hour
					);
					
				}
			}
		}
		
		echo json_encode($dataRes, JSON_PRETTY_PRINT);
	}
	
	public function getTrackLocPath() {
		$this->db->from('t_users_location_history as TH');
		$this->db->where('TH.id_user', $_POST['id_user']);
		$this->db->where('TH.id_bongkar', $_POST['id_bongkar']);
		
		if (isset($_POST['dateTrack1']) && $_POST['dateTrack1'] != "") {
			if (isset($_POST['dateTrack2']) && $_POST['dateTrack2'] != "") {
				$this->db->where('DATE(TH.created_at) >=', $_POST['dateTrack1']);
			} else {
				$this->db->where('DATE(TH.created_at)', $_POST['dateTrack1']);
			}
		}
		
		if (isset($_POST['dateTrack2']) && $_POST['dateTrack2'] != "") {
			$this->db->where('DATE(TH.created_at) <=', $_POST['dateTrack2']);
		}
		
		$q = $this->db->get()->result_array();
		
		echo json_encode($q, JSON_PRETTY_PRINT);
	}
	
	public function getArmadaPos() {
		$this->db->select('
			TL.*,
			(SELECT TH.lat FROM t_users_location_history TH WHERE TH.id_user = TL.id_user ORDER BY TH.id DESC LIMIT 1) as lat_new,
			(SELECT TH.lng FROM t_users_location_history TH WHERE TH.id_user = TL.id_user ORDER BY TH.id DESC LIMIT 1) as lng_new,
			U.Name, 
			TA.no_polisi
		');
		$this->db->from('t_users_location TL')
			->join('users U', 'TL.id_user = U.id', 'LEFT')
			->join('t_armada TA', 'TL.id_user = TA.id_user', 'LEFT');
		
		$this->db->where('U.IDPosition', 4);
		if (isset($_POST['id_user']) && $_POST['id_user'] != "") {
			$this->db->where('U.id', $_POST['id_user']);
		}
		
		$q = $this->db->get()->result_array();
		
		echo json_encode($q, JSON_PRETTY_PRINT);
	}
	
	public function lihat_data() {
		
		$this->db->select('
			TOR.*, 
			UD.Name as nama_driver, 
			CS.nama_customer,
			CS.alamat
		');
		$this->db->from('t_order as TOR');
		$this->db->join('users UD', 'TOR.id_driver = UD.id', 'LEFT');
		$this->db->join('t_customer CS', 'TOR.id_customer = CS.id', 'LEFT');
		$this->db->where('TOR.id', $_GET['id']);
		$q = $this->db->get()->row();
		
		$data['onLap'] = $q;
		
		$this->db->select('
			TOR.*,
			TB.nama_barang,
			TS.satuan as nama_satuan
		');
		$this->db->from('t_order_detail as TOR');
		$this->db->join('t_barang TB', 'TOR.kode_barang = TB.id', 'LEFT');
		$this->db->join('t_satuan TS', 'TOR.satuan = TS.id', 'LEFT');
		$this->db->where('TOR.id_order', $_GET['id']);
		$qDetail = $this->db->get()->result();
		
		$data['onLapDetail'] = $qDetail;
		$this->load->view('laporan/order/v_lihat_data', $data);
	}
	
	public function deleteLaporan(){
		
		$this->db->where('id', $_POST['id']);
		$this->db->delete('t_laporan_kendala');

		$this->session->set_flashdata('oke', 'Data berhasil dihapus!');
		$data = [
			'status' => true
		];
		echo json_encode($data, JSON_PRETTY_PRINT);
	}
	
	public function excel()
    {
    	$data_session = $this->session->userdata('login_sess'); 
		$kantor       =  $data_session['KANTOR'];
        $filter = $this->input->get();
				
		if(isset($_GET['tampil_data'])) {
			$per_page = $_GET['tampil_data'];
		} else {
			$per_page = -1;
		}
		
		if($this->uri->segment(4) != ""){
			$page = ($this->uri->segment(4)) ;
		} else {
			$page = 1;
		}
		
        $rawData = $this->m_lapor_kendala->getLaporKendala($filter, $per_page, $page);

        // Pengganti Foreach
        $data = array_map(function ($val) {
            return array(
                $val->Name,
				$val->plat_nomor,
                $val->jenis_laporan,
                $val->keterangan,
                date('d-m-Y', strtotime($val->created_at)),
				$val->status_proved == 1 ? $val->keterangan_proved : "Pending",
				$val->status_proved == 1 ? $val->NameProved : "Pending",
				$val->status_proved == 1 ? date('d-m-Y', strtotime($val->date_proved)) : "Pending",
            );
        }, $rawData);

        // print_r($rawData); 
        // die();

        $excel = array(
            'filename' => 'lapor_kendala',
            'header'   => 'Rekap Lapor Kendala',
            'field'    => ['Pelapor', 'No. Polisi', 'Jenis Laporan', 'Keterangan', 'Tanggal', 'Konfirmasi Admin', 'Admin', 'Tanggal Konfirmasi'],
            'data'     => $data,
        );

        $this->load->view('v_excel_base', $excel);
    }
}

?>