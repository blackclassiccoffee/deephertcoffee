<?php

class tombol_darurat extends CI_Controller {

	function __construct(){
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");       
        $this->load->model('master');
        $this->load->model('m_p_user');
		$this->load->model('m_history');
		$this->load->model('m_lapor_kendala');
		$this->load->model('m_tombol_darurat');
        $this->load->helper('url');
		//$this->load->library('guzzle');
    }

	public function v_data($id = ""){

		if($this->session->userdata('login_sess') == ""){
			$this->load->view('login');
		} else {
			$data_session = $this->session->userdata('login_sess');
			$data = array('menu'  => $this->master->menu(0));
			
			$filter = $this->input->get();
			
			if($_SERVER['QUERY_STRING'] != "") {
				$q_string = "?".$_SERVER['QUERY_STRING'];
			} else {
				$q_string = "";
			}
			
			$per_page = 10;
			
			if($this->uri->segment(4) != ""){
				$page = ($this->uri->segment(4)) ;
			} else {
				$page = 1;
			}
			
			$config = array();
			$config["base_url"] 		= base_url() . "/laporan/tombol_darurat/v_data/";
			$config["suffix"] 			= '/'.$q_string;
			$config["first_url"] 		= base_url() . "/laporan/tombol_darurat/v_data/1/".$q_string;
			$config["per_page"] 		= $per_page;
			$config["total_rows"] 		= $this->m_tombol_darurat->record_count($filter)->numrows;
			$config["uri_segment"] 		= 4;
			$choice = $config["total_rows"] / $config["per_page"];
			$config["num_links"] 		= 2;//round($choice);
			$config['use_page_numbers'] = TRUE;
			$config['full_tag_open']  	= '<ul class="pagination pagination-sm m-0 float-right">';
			$config['full_tag_close'] 	= '</ul><!--pagination-->';

			$config['first_link']      = '&laquo; First';
			$config['first_tag_open']  = '<li class="page-item page-link">';
			$config['first_tag_close'] = '</li>';

			$config['last_link']      = 'Last &raquo;';
			$config['last_tag_open']  = '<li class="page-item page-link">';
			$config['last_tag_close'] = '</li>';

			$config['next_link']      = '&raquo;';
			$config['next_tag_open']  = '<li class="page-item page-link">';
			$config['next_tag_close'] = '</li>';

			$config['prev_link']      = '&laquo;';
			$config['prev_tag_open']  = '<li class="page-item page-link">';
			$config['prev_tag_close'] = '</li>';

			$config['cur_tag_open']  = '<li class="page-item active"><a class="page-link" href="">';
			$config['cur_tag_close'] = '</a></li>';

			$config['num_tag_open']  = '<li class="page-item page-link">';
			$config['num_tag_close'] = '</li>';
			
			$this->pagination->initialize($config);


			$data['namaLengkap']    = $data_session['namaLengkap'];
			$data['jabatan'] 	    = $data_session['namaJabatan'];
			$data['tanggal_daftar'] = $data_session['tanggal_daftar'];
			$data['avatar'] 		= $data_session['avatar'];
			$data['kantor'] 		= $data_session['namaKantor'];
			$data['last_login'] 	= $data_session['last_login'];
			$data['USERNAME'] 		= $data_session['USERNAME'];
			$data['KANTOR'] 		= $data_session['KANTOR'];
			$data['id_kantor'] 		= $data_session['KANTOR'];
			$data['ID'] 			= $data_session['ID'];
			$data['LEVEL'] 			= $data_session['LEVEL'];
			$data['hak_akses']		= $this->master->get_hak_akses('2');
			$data['nama_menu']		= "Tombol Darurat";
			
			$this->load->view('head', $data);
			$this->load->view('header', $data);


			$q = $this->master->getMenu($data_session['ID']);
			$d = array();
			foreach ($q as $theData) {
				$d[] = $theData->segment;
			}

			if(!in_array($this->uri->segment('2'), $d)){
				redirect(site_url('dashboard'));
			}
			
			$dataMaster = $this->m_tombol_darurat->getPanic($filter, $config["per_page"], $page);
			$data['onLaporan'] = $dataMaster;
			
			// Setting Pagination -------------------------
			$cur_page = $page;
			if ($cur_page == 0) {
				$cur_page = 1;
			} else {
				$cur_page = $page;
			}

			if($cur_page == 1) {
				$start = $cur_page;
			} else {
				if ($config["total_rows"] == 1) {
					$start = 1;
				} else {
					$start = (($cur_page - 1) * $this->pagination->per_page) + 1;
				}
			}

			if($cur_page * $this->pagination->per_page > $config["total_rows"]) {
				$end = $config["total_rows"];
			} else {
				if ($config["total_rows"] == 1) {
					$end = 1;
				} else {
					$end = (($cur_page * $this->pagination->per_page));
				}
			}
			
			
			if(isset($_GET['cari'])) {	
				if(count($dataMaster) == 10) {
					$data["links"] = $this->pagination->create_links();
				} else if($this->uri->segment(4) != ""){
					if($this->uri->segment(4) > 1){
						$data["links"] = $this->pagination->create_links();
					}
				} 
				if(count($dataMaster) == 0) {
					$data["links"] = "";
					$data['pagination_info'] = "Data Tidak Ditemukan!";
				} else {

					$data["links"] = $this->pagination->create_links();
					$data['pagination_info'] = "Menampilkan ".$start." s/d ".$end." dari ".$config["total_rows"]." total data";
				}
			} else {
				if(count($dataMaster) != 0) {
					$data["links"] = $this->pagination->create_links();
					$data['pagination_info'] = "Menampilkan ".$start." s/d ".$end." dari ".$config["total_rows"]." total data";
				} else {
					$data["links"] = "";
					$data['pagination_info'] = "";
				}
			}

			$this->load->view('laporan/tombol_darurat/v_index_data', $data);
			$this->load->view('footer');
		}
	}
	
	public function lihat_peta() {
		$data['lat'] = $_GET['lat'];
		$data['lng'] = $_GET['lng'];
		$this->load->view('laporan/tombol_darurat/v_lihat_peta', $data);
	}
	
	public function konfirmasi() {
		
		$this->db->select('TL.*, U.Name, TA.plat_nomor');
		$this->db->from('t_panic as TL')
			->join('users U', 'TL.id_user = U.id')
			->join('t_armada TA', 'TL.id_user = TA.id_user');
		$this->db->where('TL.id', $_GET['id']);
		$q = $this->db->get()->row();
		
		$data['onLap'] = $q;
		$this->load->view('laporan/tombol_darurat/v_konfirmasi', $data);
	}

	public function simpan_konfirmasi() {
		
		$data_session = $this->session->userdata('login_sess');
		
		$data = [
			'status_proved' 	 => 1,
			'keterangan_proved'  => $_POST['keterangan'],
			'id_user_proved'     => $data_session['ID'],
			'date_proved'	     => date('Y-m-d H:i:s')
		];
		
		$this->db->where('id', $_POST['id']);
        $this->db->update('t_panic', $data);
		
		$dataUser = $this->db->from('users')->where('id', $_POST['id_user'])->get()->result();
		$dataToken = array();
		foreach($dataUser as $data) {
			$dataToken[] = $data->FirebaseToken;
		}

		$dataMsg   = array(
			"body"  => $_POST['keterangan'],
			"title" => "Tombol Darurat | Konfirmasi",
			"modul" => "info",
			"targetClass" => "igdevelop.trackmobile_driver.MainActivity"
		);

		$dataNotf  = array(
			"body"  => $_POST['keterangan'],
			"title" => "Tombol Darurat | Konfirmasi",
			"modul" => "info",
			"targetClass" => "igdevelop.trackmobile_driver.MainActivity"
		);

		$msg = $this->master->sendNotification($dataToken, $dataMsg, $dataNotf);
		
		echo json_encode('sukses');
	}
	
	public function lihat_data() {
		
		$this->db->select('TL.*, U.Name, UP.Name as NameProved, TA.plat_nomor');
		$this->db->from('t_panic as TL')
			->join('users U', 'TL.id_user = U.id')
			->join('users UP', 'TL.id_user_proved = UP.id', 'LEFT')
			->join('t_armada TA', 'TL.id_user = TA.id_user');
		$this->db->where('TL.id', $_GET['id']);
		$q = $this->db->get()->row();
		
		$data['onLap'] = $q;
		$this->load->view('laporan/tombol_darurat/v_lihat_data', $data);
	}
	
	public function deleteLaporan(){
		
		$this->db->where('id', $_POST['id']);
		$this->db->delete('t_panic');

		$this->session->set_flashdata('oke', 'Data berhasil dihapus!');
		$data = [
			'status' => true
		];
		echo json_encode($data, JSON_PRETTY_PRINT);
	}
	
	public function excel()
    {
    	$data_session = $this->session->userdata('login_sess'); 
		$kantor       =  $data_session['KANTOR'];
        $filter = $this->input->get();
				
		if(isset($_GET['tampil_data'])) {
			$per_page = $_GET['tampil_data'];
		} else {
			$per_page = -1;
		}
		
		if($this->uri->segment(4) != ""){
			$page = ($this->uri->segment(4)) ;
		} else {
			$page = 1;
		}
		
        $rawData = $this->m_tombol_darurat->getPanic($filter, $per_page, $page);

        // Pengganti Foreach
        $data = array_map(function ($val) {
            return array(
                $val->Name,
				$val->plat_nomor,
                date('d-m-Y H:i', strtotime($val->created_at)),
				$val->status_proved == 1 ? $val->keterangan_proved : "Pending",
				$val->status_proved == 1 ? $val->NameProved : "Pending",
				$val->status_proved == 1 ? date('d-m-Y H:i', strtotime($val->date_proved)) : "Pending",
            );
        }, $rawData);

        // print_r($rawData); 
        // die();

        $excel = array(
            'filename' => 'tombol_darurat',
            'header'   => 'Rekap Tombol Darurat',
            'field'    => ['Pelapor', 'No. Polisi', 'Tanggal Kirim', 'Konfirmasi Admin', 'Admin', 'Tanggal Konfirmasi'],
            'data'     => $data,
        );

        $this->load->view('v_excel_base', $excel);
    }
	
//	public function checkNotif() {
//		$this->db->from('t_panic')
//			->where('')
//	}
}

?>