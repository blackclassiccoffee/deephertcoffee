<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class c_akuntan_buku_besar extends CI_Controller {
	
	public $title = "";
	
	function __construct(){
        parent::__construct();  
        date_default_timezone_set("Asia/Jakarta");     
        $this->load->model('master');
        $this->load->model('m_login');
		$this->load->model('m_akuntansi_buku_besar');
		$this->load->model('m_akuntansi');
		$this->load->helper("url");
    }

	public function index(){
		
		if($this->session->userdata('login_sess') == "") {
			$this->load->view('login');
		} else {
			
			$data_session = $this->session->userdata('login_sess');
			
			$data = array( 'menu'     => $this->master->menu(0));
			$data['namaLengkap']    = $data_session['namaLengkap'];
			$data['jabatan'] 	    = $data_session['namaJabatan'];
			$data['tanggal_daftar'] = $data_session['tanggal_daftar'];
			$data['avatar'] 		= $data_session['avatar'];
			$data['kantor'] 		= $data_session['namaKantor'];
			$data['last_login'] 	= $data_session['last_login'];
			$data['USERNAME'] 		= $data_session['USERNAME'];
			$data['KANTOR'] 		= $data_session['KANTOR'];
			$data['id_kantor'] 		= $data_session['KANTOR'];
			$data['ID'] 			= $data_session['ID'];
			$data['LEVEL'] 			= $data_session['LEVEL'];
			$data['toko']			= $this->master->getToko();
			$data['hak_akses']		= $this->master->get_hak_akses('1');
			$data['nama_menu']		= "Buku Besar";
			
			$this->load->view('head', $data);
			$this->load->view('header', $data);

			$q = $this->master->getMenu($data_session['ID']);
			$d = array();
			foreach ($q as $theData) {
				$d[] = $theData->segment;
			}

			if(!in_array($this->uri->segment('1'), $d)){
				redirect(site_url('dashboard'));
			}
			
			$filter = $_GET;
			$buku_besar = $this->m_akuntansi_buku_besar->getBukuBesarAll($filter);
			$data['dataBukuAll'] = $buku_besar;
			$inx = 0;
			foreach($buku_besar as $onData) {
				$data['dataBuku'][$inx]    = $this->m_akuntansi_buku_besar->getBukuBesar($onData->nomor_akun);
				$inx++;
			}
			$data['data_coa']    = $this->m_akuntansi->on_akun_group_all(0, "", $filter);
			$this->load->view('akuntansi/buku_besar/v_buku_besar', $data);
			
			$this->load->view('footer');
		}
		
	}
}
