<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class c_master_coa extends CI_Controller {
	
	public $title = "";
	
	function __construct(){
        parent::__construct();  
        date_default_timezone_set("Asia/Jakarta");     
        $this->load->model('master');
        $this->load->model('m_login');
		$this->load->model('m_akuntansi');
		$this->load->helper("url");
    }

	public function index(){
		
		if($this->session->userdata('login_sess') == "") {
			$this->load->view('login');
		} else {
			
			$data_session = $this->session->userdata('login_sess');
			
			$data = array( 'menu'     => $this->master->menu(0));
			$data['namaLengkap']    = $data_session['namaLengkap'];
			$data['jabatan'] 	    = $data_session['namaJabatan'];
			$data['tanggal_daftar'] = $data_session['tanggal_daftar'];
			$data['avatar'] 		= $data_session['avatar'];
			$data['kantor'] 		= $data_session['namaKantor'];
			$data['last_login'] 	= $data_session['last_login'];
			$data['USERNAME'] 		= $data_session['USERNAME'];
			$data['KANTOR'] 		= $data_session['KANTOR'];
			$data['id_kantor'] 		= $data_session['KANTOR'];
			$data['ID'] 			= $data_session['ID'];
			$data['LEVEL'] 			= $data_session['LEVEL'];
			$data['toko']			= $this->master->getToko();
			$data['hak_akses']		= $this->master->get_hak_akses('1');
			$data['nama_menu']		= "Master Akun";
			
			$this->load->view('head', $data);
			$this->load->view('header', $data);

			$q = $this->master->getMenu($data_session['ID']);
			$d = array();
			foreach ($q as $theData) {
				$d[] = $theData->segment;
			}

			if(!in_array($this->uri->segment('1'), $d)){
				redirect(site_url('dashboard'));
			}
			$data = array('data_coa' => $this->m_akuntansi->on_akun_table(0));
			$this->load->view('akuntansi/master_akun/v_master_akun', $data);
			
			$this->load->view('footer');
		}
		
	}
	
	public function tambah_akun() {

    	$data_session = $this->session->userdata('login_sess');
		$data['toko']			= $this->master->getToko();
		$data['KANTOR'] 		= $data_session['KANTOR'];
		$data['LEVEL'] 			= $data_session['LEVEL'];
		$data = array('data_coa' => $this->m_akuntansi->on_akun_group(0));
		$this->load->view('akuntansi/master_akun/v_tambah_akun', $data);
		
	}

	public function save_akun() {
		
		$kode_akun = explode('#', $_POST['group']);
		
		$data = array(
			'kode_akun'    => $kode_akun[1],
			'nomor_akun'   => $_POST['nomor_akun'],
			'nama_akun'    => $_POST['nama_akun'],
			'jenis_saldo'  => isset($_POST['is_group']) ? 0 : $_POST['jenis_saldo'],
			'is_bank_cash' => isset($_POST['is_bank_cash']) ? 1 : 0,
			'jenis_cost'   => $kode_akun[1] == 6 ? $_POST['jenis_cost'] : 0,
			'is_group'     => isset($_POST['is_group']) ? 1 : 0,
			'parent'	   => $kode_akun[2],
			'created_at'   => date('Y-m-d H:i:s'),
			'updated_at'   => date('Y-m-d H:i:s')
		);
		
		$this->db->insert('t_akuntansi_akun', $data);
		$this->session->set_flashdata('oke', 'Data berhasil disimpan!');
		echo json_encode('sukses');
	}
	
	public function delete_akun() {
		$this->db->where('id', $_POST['id']);
		$this->db->where('nomor_akun', $_POST['nomor_akun']);
		$this->db->delete('t_akuntansi_akun');
		
		$this->session->set_flashdata('oke', 'Data berhasil dihapus!');
		echo json_encode('sukses');
	}
	
	public function edit_akun() {

    	$data_session = $this->session->userdata('login_sess');
		$data['toko']			= $this->master->getToko();
		$data['KANTOR'] 		= $data_session['KANTOR'];
		$data['LEVEL'] 			= $data_session['LEVEL'];
		$data = array('data_coa' => $this->m_akuntansi->on_akun_group(0));
		$this->load->view('akuntansi/master_akun/v_edit_akun', $data);
		
	}
}
