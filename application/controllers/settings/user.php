<?php

class user extends CI_Controller {

	function __construct(){
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");       
        $this->load->model('master');
        $this->load->model('m_p_user');
        $this->load->helper('url');
    }

	public function v_data($id = ""){

		if($this->session->userdata('login_sess') == ""){
			$this->load->view('login');
		} else {
			$data_session = $this->session->userdata('login_sess');
			$data = array('menu'  => $this->master->menu(0));
			
			$filter = $this->input->get();
			
			if($_SERVER['QUERY_STRING'] != "") {
				$q_string = "?".$_SERVER['QUERY_STRING'];
			} else {
				$q_string = "";
			}
			
			if(isset($_GET['tampil_data'])) {
				$per_page = $_GET['tampil_data'];
			} else {
				$per_page = 10;
			}
			
			$config = array();
			$config["base_url"] 		= base_url() . "/settings/user/v_data/";
			$config["suffix"] 			= '/'.$q_string;
			$config["first_url"] 		= base_url() . "/settings/user/v_data/1/".$q_string;
			$config["per_page"] 		= $per_page;
			$config["total_rows"] 		= $this->m_p_user->record_count($filter, $data_session)->numrows;
			$config["uri_segment"] 		= 4;
			$choice = $config["total_rows"] / $config["per_page"];
			$config["num_links"] 		= 2;//round($choice);
			$config['use_page_numbers'] = TRUE;
			$config['full_tag_open']  	= '<ul class="pagination pagination-sm m-0 float-right">';
			$config['full_tag_close'] 	= '</ul><!--pagination-->';

			$config['first_link']      = '&laquo; First';
			$config['first_tag_open']  = '<li class="page-item page-link">';
			$config['first_tag_close'] = '</li>';

			$config['last_link']      = 'Last &raquo;';
			$config['last_tag_open']  = '<li class="page-item page-link">';
			$config['last_tag_close'] = '</li>';

			$config['next_link']      = '&raquo;';
			$config['next_tag_open']  = '<li class="page-item page-link">';
			$config['next_tag_close'] = '</li>';

			$config['prev_link']      = '&laquo;';
			$config['prev_tag_open']  = '<li class="page-item page-link">';
			$config['prev_tag_close'] = '</li>';

			$config['cur_tag_open']  = '<li class="page-item active"><a class="page-link" href="">';
			$config['cur_tag_close'] = '</a></li>';

			$config['num_tag_open']  = '<li class="page-item page-link">';
			$config['num_tag_close'] = '</li>';
			
			$this->pagination->initialize($config);
			
			if($this->uri->segment(4) != ""){
				$page = ($this->uri->segment(4)) ;
			} else {
				$page = 1;
			}
			
			
			$data['namaLengkap']    = $data_session['namaLengkap'];
			$data['jabatan'] 	    = $data_session['namaJabatan'];
			$data['tanggal_daftar'] = $data_session['tanggal_daftar'];
			$data['avatar'] 		= $data_session['avatar'];
			$data['kantor'] 		= $data_session['nama_lab'];
			$data['last_login'] 	= $data_session['last_login'];
			$data['username'] 		= $data_session['username'];
			$data['id_lab'] 		= $data_session['lab_id'];
			$data['id'] 			= $data_session['id'];
			$data['hak_akses']		= $this->master->get_hak_akses('2');
			$data['kantor']	  		= $this->master->getKantor();
			$data['jabatan']  		= $this->master->getJabatan();
			$data['nama_menu']		= "Pengaturan User/Pengguna";
			
			$this->load->view('head', $data);
			$this->load->view('header', $data);
			
			$dataMaster = $this->m_p_user->getUser($filter, $config["per_page"], $page, $data_session);
			$data['dataUser'] = $dataMaster;
			
			// Setting Pagination -------------------------
			$cur_page = $page;
			if ($cur_page == 0) {
				$cur_page = 1;
			} else {
				$cur_page = $page;
			}

			if($cur_page == 1) {
				$start = $cur_page;
			} else {
				if ($config["total_rows"] == 1) {
					$start = 1;
				} else {
					$start = (($cur_page - 1) * $this->pagination->per_page) + 1;
				}
			}

			if($cur_page * $this->pagination->per_page > $config["total_rows"]) {
				$end = $config["total_rows"];
			} else {
				if ($config["total_rows"] == 1) {
					$end = 1;
				} else {
					$end = (($cur_page * $this->pagination->per_page));
				}
			}
			
			
			if(isset($_GET['cari'])) {	
				if(count($dataMaster) == 10) {
					$data["links"] = $this->pagination->create_links();
				} else if($this->uri->segment(4) != ""){
					if($this->uri->segment(4) > 1){
						$data["links"] = $this->pagination->create_links();
					}
				} 
				if(count($dataMaster) == 0) {
					$data["links"] = "";
					$data['pagination_info'] = "Data Tidak Ditemukan!";
				} else {

					$data["links"] = $this->pagination->create_links();
					$data['pagination_info'] = "Menampilkan ".$start." s/d ".$end." dari ".$config["total_rows"]." total data";
				}
			} else {
				if(count($dataMaster) != 0) {
					$data["links"] = $this->pagination->create_links();
					$data['pagination_info'] = "Menampilkan ".$start." s/d ".$end." dari ".$config["total_rows"]." total data";
				} else {
					$data["links"] = "";
					$data['pagination_info'] = "";
				}
			}


			$q = $this->master->getMenu($data_session['id']);
			$d = array();
			foreach ($q as $theData) {
				$d[] = $theData->segment;
			}

			if(!in_array($this->uri->segment('2'), $d)){
				redirect(site_url('dashboard'));
			}
			
			$data['jabatan'] = $this->master->getJabatan();

			$this->load->view('user/userManagemen', $data);
			$this->load->view('footer');
		}
	}
	
	public function tambah_user() {
		
		$data_session = $this->session->userdata('login_sess');
		
		$data['kantor']	  = $this->master->getKantor();
		$data['jabatan']  = $this->master->getJabatan();
		$data['menu'] 	  = $this->master->menuToAdd(0);
		
		$this->load->view('user/v_tambah_user', $data);
	}

	public function changeUser($id){
		
		$data_session = $this->session->userdata('login_sess');
		
		$data['kantor']		 = $this->master->getKantor();
		$data['data_user']   = $this->m_p_user->getUserToEdit($id);
		$data['menu'] = $this->master->menuToEdit(0, $id);
		
		$this->load->view('user/v_ubah_user', $data);
	}

	public function insertUser(){
		
		$nmfile = "";
		if (isset($_POST['info']['Picture'])) {
			if ($_POST['info']['Picture'] != "") {
				$realFile = str_replace(' ', '_', $_POST['info']['username']);
				$nmfile = "user_pic_".$realFile."_".uniqid().".png";
				$upload_path = './assets/uploads/user_pic';
	
				$img = $_POST['info']['Picture']; // Your data 'data:image/png;base64,AAAFBfj42Pj4';
				$tipeImage = explode(',', $img);
				// $tipeImage[0] = data:image/png;base64,
				$img = str_replace($tipeImage[0].',', '', $img);
				$img = str_replace(' ', '+', $img);
				$data = base64_decode($img);
				file_put_contents($upload_path.'/'.$nmfile, $data);
			}
		}
		
		$kantor       = $_POST['info']['kantor'];
		$data_user = [
			'Name'		  => $_POST['info']['Name'],
			'email'	   	  => $_POST['info']['username'],
			'password' 	  => password_hash($_POST['info']['password'], PASSWORD_DEFAULT),
			'karyawan_id' => $_POST['info']['karyawan_id'],
			'foto'	  	  => $nmfile,
			'created_at'  => date('Y-m-d H:i:s')
		];
		
		$this->db->insert('users', $data_user);
		$id = $this->db->insert_id();
		
		if (isset($_POST['menu'])) {
			for ($i = 0; $i<count($_POST['menu']); $i++) {
				$dataToMenu = array(
					'idUser' 		=> $id, 
					'idMenu' 		=> $_POST['menu'][$i]['idMenu'],
					'role_create'   => $_POST['menu'][$i]['role_create'],
					'role_read'		=> $_POST['menu'][$i]['role_read'],
					'role_update' 	=> $_POST['menu'][$i]['role_update'],
					'role_delete' 	=> $_POST['menu'][$i]['role_delete'],
					'role_download' => $_POST['menu'][$i]['role_download']
				);

				$this->m_p_user->insertToMenu($dataToMenu);
			}
		}

		$data = [
			'status' => true
		];
		echo json_encode($data, JSON_PRETTY_PRINT);
	}

	public function updateUser(){
		
		$nmfile = "";
        if (isset($_POST['info']['Picture']) && $_POST['info']['Picture'] != "") {
            $realFile = str_replace(' ', '_', $_POST['info']['username']);
            $nmfile = "user_pic_".$realFile."_".uniqid().".png";
            $upload_path = './assets/uploads/user_pic';

            $img = $_POST['info']['Picture']; // Your data 'data:image/png;base64,AAAFBfj42Pj4';
            $tipeImage = explode(',', $img);
            // $tipeImage[0] = data:image/png;base64,
            $img = str_replace($tipeImage[0].',', '', $img);
            $img = str_replace(' ', '+', $img);
            $data = base64_decode($img);
            file_put_contents($upload_path.'/'.$nmfile, $data);
        } else {
			$nmfile = $_POST['info']['Picture_lama'];
		}
		
		$kantor       = $_POST['info']['kantor'];
		$data_user = [
			'Name'		  => $_POST['info']['Name'],
			'email'	   	  => $_POST['info']['username'],
			'karyawan_id' => $_POST['info']['karyawan_id'],
			'foto'	  	  => $nmfile,
			'created_at'  => date('Y-m-d H:i:s')
		];
		
		if ($_POST['info']['password'] != "") {
			$data_user['password'] = password_hash($_POST['info']['password'], PASSWORD_DEFAULT);
		}
		
		$this->db->where('id', $_POST['id']);
		$this->db->update('users', $data_user);
		
		$q = $this->master->getMenu($_POST['id']);
		$arra_menu = array();
		foreach ($q as $dataMenu) {
			$arra_menu[] = $dataMenu->idMenu;
		}
		
		for ($i=0; $i <count($_POST['hideRole_up']); $i++) { 
			if (!empty($_POST['role_up'])) {
				if(!in_array($_POST['hideRole_up'][$i], $_POST['role_up'])){
					$dataRole = array('role_update' => 0);
					$this->m_p_user->toUpdateRole($_POST['hideRole_up'][$i], $_POST['id'], $dataRole);
				}
			}
		}
		
		for ($i=0; $i <count($_POST['hideRole_create']); $i++) { 
			if (!empty($_POST['role_create'])) {
				if(!in_array($_POST['hideRole_create'][$i], $_POST['role_create'])){
					$dataRole = array('role_create' => 0);
					$this->m_p_user->toUpdateRole($_POST['hideRole_create'][$i], $_POST['id'], $dataRole);
				}
			}
		}
		
		for ($i=0; $i <count($_POST['hideRole_read']); $i++) { 
			if (!empty($_POST['role_read'])) {
				if(!in_array($_POST['hideRole_read'][$i], $_POST['role_read'])){
					$dataRole = array('role_read' => 0);
					$this->m_p_user->toUpdateRole($_POST['hideRole_read'][$i], $_POST['id'], $dataRole);
				}
			}
		}
		
		for ($i=0; $i <count($_POST['hideRole_delete']); $i++) { 
			if (!empty($_POST['role_delete'])) {
				if(!in_array($_POST['hideRole_delete'][$i], $_POST['role_delete'])){
					$dataRole = array('role_delete' => 0);
					$this->m_p_user->toUpdateRole($_POST['hideRole_delete'][$i], $_POST['id'], $dataRole);
				}
			}
		}
		
		for ($i=0; $i <count($_POST['hideRole_download']); $i++) { 
			if (!empty($_POST['role_download'])) {
				if(!in_array($_POST['hideRole_download'][$i], $_POST['role_download'])){
					$dataRole = array('role_download' => 0);
					$this->m_p_user->toUpdateRole($_POST['hideRole_download'][$i], $_POST['id'], $dataRole);
				}
			}
		}
		
		if (isset($_POST['menuFull'])) {
			for ($i = 0; $i<count($_POST['menuFull']); $i++) {
				if(!in_array($_POST['menuFull'][$i]['idMenu'], $arra_menu)){

					$dataToMenu = array(
						'idUser' 		=> $_POST['id'], 
						'idMenu' 		=> $_POST['menuFull'][$i]['idMenu'],
						'role_create'   => $_POST['menuFull'][$i]['role_create'],
						'role_read'		=> $_POST['menuFull'][$i]['role_read'],
						'role_update' 	=> $_POST['menuFull'][$i]['role_update'],
						'role_delete' 	=> $_POST['menuFull'][$i]['role_delete'],
						'role_download' => $_POST['menuFull'][$i]['role_download']
					);

					$this->m_p_user->insertToMenu($dataToMenu);

				}
			}
		}
		
		if (isset($_POST['menu'])) {
			for ($i=0; $i <count($_POST['hideMenu']); $i++) { 

				if(!in_array($_POST['hideMenu'][$i], $_POST['menu'])){
					$this->m_p_user->deleteToMenu($_POST['id'], $_POST['hideMenu'][$i]);
				}
			}
		}
		
		if($arra_menu){
			
			for ($i=0; $i <count($_POST['role_up']); $i++) { 
				
				$dataRole = array('role_update' => 1);
				$this->m_p_user->toUpdateRole($_POST['role_up'][$i], $_POST['id'], $dataRole);
			}
			
			for ($i=0; $i <count($_POST['role_create']); $i++) {
				$dataRole = array('role_create' => 1);
				$this->m_p_user->toUpdateRole($_POST['role_create'][$i], $_POST['id'], $dataRole);
			}
			
			for ($i=0; $i <count($_POST['role_read']); $i++) {
				$dataRole = array('role_read' => 1);
				$this->m_p_user->toUpdateRole($_POST['role_read'][$i], $_POST['id'], $dataRole);
			}
						
			
			for ($i=0; $i <count($_POST['role_delete']); $i++) { 
				$dataRole = array('role_delete' => 1);
				$this->m_p_user->toUpdateRole($_POST['role_delete'][$i], $_POST['id'], $dataRole);
			}
			
			for ($i=0; $i <count($_POST['role_download']); $i++) { 
				$dataRole = array('role_download' => 1);
				$this->m_p_user->toUpdateRole($_POST['role_download'][$i], $_POST['id'], $dataRole);
			}
			
		}
		
		$data = [
			'status' => true
		];
		
		echo json_encode($data, JSON_PRETTY_PRINT);
		
	}

	public function deleteUser(){
		$this->m_p_user->toDeleteUser($_POST['id']);
		
		$this->db->where('idUser', $_POST['id']);
		$this->db->delete('t_user_menu');

		$this->session->set_flashdata('oke', 'Data berhasil dihapus!');
		$data = [
			'status' => true
		];
		echo json_encode($data, JSON_PRETTY_PRINT);
	}

	function deleteUserInSelected(){
		$this->m_p_user->toDeleteUser($this->input->post('idUser'));
		$this->m_p_user->toDeleteUserDetail($this->input->post('idUser'));
		
		$this->db->where('idUser', $this->input->post('idUser'));
		$this->db->delete('t_user_menu');

		$this->session->set_flashdata('oke', $this->input->post('idUser'));
		redirect(site_url()."/user");
	}

	function ubah_menu($id){
		$data['menu'] = $this->master->menuToEdit(0, $id);

		$q = $this->m_p_user->getUserToEdit($id);
		//$data['nama_user'] = $q->namaLengkap;

		$this->load->view('head');
		$this->load->view('user/ubah_menu', $data);
	}

	function save_menu(){
		$q = $this->master->getMenu($this->input->post('idUser'));
		$arra_menu = array();
		foreach ($q as $dataMenu) {
			$arra_menu[] = $dataMenu->idMenu;
		}
		
		//print_r($this->input->post('menu'));
		//exit();
		
		for ($i=0; $i <count($this->input->post('hideRole_up')); $i++) { 
			
			if(!in_array($this->input->post('hideRole_up')[$i], $this->input->post('role_up'))){
				$dataRole = array('role_update' => 0);
				$this->m_p_user->toUpdateRole($this->input->post('hideRole_up')[$i], $this->input->post('idUser'), $dataRole);
			}
		}
		
		for ($i=0; $i <count($this->input->post('hideRole_read')); $i++) { 

			if(!in_array($this->input->post('hideRole_create')[$i], $this->input->post('role_create'))){
				$dataRole = array('role_create' => 0);
				$this->m_p_user->toUpdateRole($this->input->post('hideRole_create')[$i], $this->input->post('idUser'), $dataRole);
			}
		}
		
		for ($i=0; $i <count($this->input->post('hideRole_read')); $i++) { 

			if(!in_array($this->input->post('hideRole_read')[$i], $this->input->post('role_read'))){
				$dataRole = array('role_read' => 0);
				$this->m_p_user->toUpdateRole($this->input->post('hideRole_read')[$i], $this->input->post('idUser'), $dataRole);
			}
		}
		
		for ($i=0; $i <count($this->input->post('hideRole_delete')); $i++) { 
			if(!in_array($this->input->post('hideRole_delete')[$i], $this->input->post('role_delete'))){
				$dataRole = array('role_delete' => 0);
				$this->m_p_user->toUpdateRole($this->input->post('hideRole_delete')[$i], $this->input->post('idUser'), $dataRole);
			}
		}
		
		for ($i=0; $i <count($this->input->post('hideRole_download')); $i++) { 
			if(!in_array($this->input->post('hideRole_download')[$i], $this->input->post('role_download'))){
				$dataRole = array('role_download' => 0);
				$this->m_p_user->toUpdateRole($this->input->post('hideRole_download')[$i], $this->input->post('idUser'), $dataRole);
			}
		}
		
		for ($i=0; $i <count($this->input->post('menu')); $i++) { 
			if(!in_array($this->input->post('menu')[$i], $arra_menu)){
				
				$dataToMenu = array(
					'idUser' => $this->input->post('idUser'), 
					'idMenu' => $this->input->post('menu')[$i]
			  	);

				$this->m_p_user->insertToMenu($dataToMenu);
				
			} 
		}
		
		for ($i=0; $i <count($this->input->post('hideMenu')); $i++) { 

			if(!in_array($this->input->post('hideMenu')[$i], $this->input->post('menu'))){
				$this->m_p_user->deleteToMenu($this->input->post('idUser'), $this->input->post('hideMenu')[$i]);
			}
		}
		
		if($arra_menu){
		
			for ($i=0; $i <count($this->input->post('role_up')); $i++) { 
				
				$dataRole = array('role_update' => 1);
				$this->m_p_user->toUpdateRole($this->input->post('role_up')[$i], $this->input->post('idUser'), $dataRole);
			}
			
		}
		
		//exit();
		
		if($arra_menu){
			for ($i=0; $i <count($this->input->post('role_create')); $i++) {
				$dataRole = array('role_create' => 1);
				$this->m_p_user->toUpdateRole($this->input->post('role_create')[$i], $this->input->post('idUser'), $dataRole);
			}
			for ($i=0; $i <count($this->input->post('role_read')); $i++) {
				$dataRole = array('role_read' => 1);
				$this->m_p_user->toUpdateRole($this->input->post('role_read')[$i], $this->input->post('idUser'), $dataRole);
			}
						
			
			for ($i=0; $i <count($this->input->post('role_delete')); $i++) { 
				$dataRole = array('role_delete' => 1);
				$this->m_p_user->toUpdateRole($this->input->post('role_delete')[$i], $this->input->post('idUser'), $dataRole);
			}
			
			for ($i=0; $i <count($this->input->post('role_download')); $i++) { 
				$dataRole = array('role_download' => 1);
				$this->m_p_user->toUpdateRole($this->input->post('role_download')[$i], $this->input->post('idUser'), $dataRole);
			}
			
		} 
		
		
		$this->session->set_flashdata('oke', 'Data berhasil disimpan!');
		redirect(site_url('settings/user/ubah_menu')."/".$this->input->post('idUser'));
	}

	public function checked_username(){
		$data_arr = array();
		$q = $this->m_p_user->checked_user($this->input->post('username'));
		foreach ($q as $data) {
			$data_arr[] = $data;
		}
		echo json_encode($data_arr, JSON_PRETTY_PRINT);
	}
	
	public function getLab(){
		$data_arr = array();
		$q = $this->db->select('*')->from('t_lab')->where('kanca_id', $_POST['kanca_id'])->get()->result();
		foreach ($q as $data) {
			$data_arr[] = $data;
		}
		echo json_encode($data_arr, JSON_PRETTY_PRINT);
	}
	
	public function getKaryawan(){
		$data_arr = array();
		$q = $this->db->select('*')->from('t_karyawan')->where('lab_id', $_POST['lab_id'])->get()->result();
		foreach ($q as $data) {
			$data_arr[] = $data;
		}
		echo json_encode($data_arr, JSON_PRETTY_PRINT);
	}

	public function excel()
    {
    	$data_session = $this->session->userdata('login_sess'); 
		$kantor       =  $data_session['KANTOR'];
        $filter = $this->input->get();
				
		if(isset($_GET['tampil_data'])) {
			$per_page = $_GET['tampil_data'];
		} else {
			$per_page = -1;
		}
		
		if($this->uri->segment(4) != ""){
			$page = ($this->uri->segment(4)) ;
		} else {
			$page = 1;
		}
		
        $rawData = $this->m_p_user->getUser($filter, $per_page, $page);

        // Pengganti Foreach
        $data = array_map(function ($val) {
            return array(
				$val['namaLengkap'],
				$val['role'],
				$val['nama_jabatan'],
				$val['username'],
				$val['nama_kantor'],
				$val['StatusActive'],
				date('d-m-Y', strtotime($val['created_at']))
            );
        }, $rawData);

        // print_r($rawData); 
        // die();

        $excel = array(
            'filename' => 'data_pengguna',
            'header'   => 'Rekap Data Pengguna',
            'field'    => ['Nama Pengguna', 'Role', 'Jabatan', 'Username', 'Kantor', 'Status', 'Tanggal'],
            'data'     => $data,
        );

        $this->load->view('v_excel_base', $excel);
    }
}

?>