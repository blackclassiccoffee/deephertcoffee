<?php
function dd($var)
{
    echo '<pre>' . json_encode($var, JSON_PRETTY_PRINT) . '</pre>';
}

function dmy($date)
{
    return date('d-m-Y', strtotime($date));
}

function persen($multipersen)
{
    return str_replace('+', '% + ', $multipersen) . '%';
}

function rp($angka)
{
    return number_format($angka, 0, ',', '.');
}

function terbilang($x)
{
    $x     = abs($x);
    $angka = array('', 'satu', 'dua', 'tiga', 'empat', 'lima',
        'enam', 'tujuh', 'delapan', 'sembilan', 'sepuluh', 'sebelas');
    $temp = '';
    if ($x < 12) {
        $temp = ' ' . $angka[$x];
    } else if ($x < 20) {
        $temp = terbilang($x - 10) . ' belas';
    } else if ($x < 100) {
        $temp = terbilang($x / 10) . ' puluh' . terbilang($x % 10);
    } else if ($x < 200) {
        $temp = ' seratus' . terbilang($x - 100);
    } else if ($x < 1000) {
        $temp = terbilang($x / 100) . ' ratus' . terbilang($x % 100);
    } else if ($x < 2000) {
        $temp = ' seribu' . terbilang($x - 1000);
    } else if ($x < 1000000) {
        $temp = terbilang($x / 1000) . ' ribu' . terbilang($x % 1000);
    } else if ($x < 1000000000) {
        $temp = terbilang($x / 1000000) . ' juta' . terbilang($x % 1000000);
    } else if ($x < 1000000000000) {
        $temp = terbilang($x / 1000000000) . ' milyar' . terbilang(fmod($x, 1000000000));
    } else if ($x < 1000000000000000) {
        $temp = terbilang($x / 1000000000000) . ' trilyun' . terbilang(fmod($x, 1000000000000));
    }
    return ucwords($temp);
}

function satuan($jumlah, $kode_barang)
{
    $CI = &get_instance();
    $CI->load->model('m_barang_satuan');

    $satuan = $CI->m_barang_satuan->getByKodeBarang($kode_barang);
    $temp   = [];
    $x      = $jumlah;

    foreach ($satuan as $i => $sat) {
        $temp[] = rp(floor($x / $sat->total_isi)) . ' ' . $sat->nama_satuan;
        $x %= $sat->total_isi;
    }

    return implode(', ', $temp);
}
