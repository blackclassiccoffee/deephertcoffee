<style>
  html, body {
    height: 90%;
  }
  #tableContainer-1 {
    height: 100%;
    width: 100%;
    display: table;
  }
  #tableContainer-2 {
    vertical-align: middle;
    display: table-cell;
    height: 90%;
  }
  #myTable {
    margin: 0 auto;
  }
</style>

<html>
  <head>
     <link rel="stylesheet" href="<?php echo base_url();?>assets/fa/css/font-awesome.css">
  </head>
<title>Login</title>
    <body>
    <div id="tableContainer-1">
      <div id="tableContainer-2">
        <form name="form1" method="post" action="<?php echo site_url('login_auth'); ?>" id="myTable">
          <table width="35%" border="0" align="center" cellpadding="2" cellspacing="2">
            <tr>
              <td width="20%" rowspan="3" align="center" valign="top">
              	<img src="<?php echo base_url('assets/images/login.gif'); ?>" width="50">
              </td>
              <td width="22%">Username</td>
              <td width="2%">:</td>
              <td width="56%">
              	<input type="hidden" name='session_data' value="<?php echo $this->session->userdata('session_id'); ?>">
              	<input type="text" name="username" id="username" autocomplete="off" autofocus required>
              </td>
            </tr>
            <tr>
              <td>Password</td>
              <td>:</td>
              <td><input type="password" name="password" id="password" required></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td><input type="submit" name="button" id="button" value="Login"></td>
              <td>&nbsp;</td>
              <td>
              	<div style="color: red;">
					<?php 
						if($this->session->flashdata('Gagal')){
							echo "<span class='fa fa-exclamation-circle'></span> ";
							echo $this->session->flashdata('Gagal'); 
						}
					?>
                </div>
              </td>
            </tr>
          </table>
        </form>
      </div>
    </div>
    <div align="center">
    	<small>ASSEMBLED BY INTEGRAVITY <span style="color:blue;">BIRU</span> MAKMUR PT. @2016</small>
    </div>
    </body>
</html>