<style>
	.nav .nav-treeview {
		padding-left : 20px;
	}
	.nav-treeview .nav-treeview {
		padding-left : 20px;
	}
</style>

<script>
	$(document).ready(function(){
		
		$('[data-toggle="tooltip"]').tooltip();
		$('[data-toggle="popover"]').popover({
			trigger : 'hover'
		});
	});
</script>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
	<!-- Navbar -->
	<nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
		<!-- Left navbar links -->
		<ul class="navbar-nav">
			<li class="nav-item">
				<a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
			</li>
			<li class="nav-item d-none d-sm-inline-block">
				<a href="<?php echo base_url('dashboard'); ?>" class="nav-link">Dashboard</a>
			</li>
		</ul>

		<!-- Right navbar links -->
		<ul class="navbar-nav ml-auto">
			<!-- Messages Dropdown Menu -->
			<li class="nav-item dropdown">
				<a class="nav-link" data-toggle="dropdown" href="#">
					<i class="fa fa-comments"></i>
					<span class="badge badge-danger navbar-badge">3</span>
				</a>
				<div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
					<a href="#" class="dropdown-item">
						<!-- Message Start -->
						<div class="media">
							<img src="<?php echo base_url();?>assets/dist/img/user1-128x128.jpg" alt="User Avatar" class="img-size-50 mr-3 img-circle">
							<div class="media-body">
								<h3 class="dropdown-item-title">
									Brad Diesel
									<span class="float-right text-sm text-danger">
										<i class="fa fa-star"></i>
									</span>
								</h3>
								<p class="text-sm">Call me whenever you can...</p>
								<p class="text-sm text-muted">
									<i class="fa fa-clock mr-1"></i> 4 Hours Ago
								</p>
							</div>
						</div>
						<!-- Message End -->
					</a>
					<div class="dropdown-divider"></div>
					<div class="dropdown-divider"></div>
					<a href="<?php echo base_url('app/chat/v_data') ?>" class="dropdown-item dropdown-footer">Lihat Semua Pesan</a>
				</div>
			</li>
		<!-- Notifications Dropdown Menu -->
			<li class="nav-item dropdown notif_panel">
				<a class="nav-link" data-toggle="dropdown" href="#" id="notif_badge">
					<i class="fa fa-bell"></i>
					<span class="badge badge-warning navbar-badge total_notif"></span>
				</a>
				<div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
					<div class="dropdown-divider"></div>
					<a href="<?php echo base_url('laporan/tombol_darurat/v_data') ?>" class="dropdown-item panic_click">
						<i class="fa fa-exclamation-triangle mr-2"></i> 
						Pesan Darurat
						<span class="float-right badge badge-danger text-sm notif_panic"></span>
					</a>
					<div class="dropdown-divider"></div>
					<a href="<?php echo base_url('laporan/bongkar_muat/v_data') ?>" class="dropdown-item bongkar_click">
						<i class="fa fa-truck mr-2"></i>Laporan Bongkar Muat
						<span class="float-right badge badge-success text-sm notif_bongkar"></span>
					</a>
					<div class="dropdown-divider"></div>
					<a href="<?php echo base_url('laporan/lapor_kendala/v_data') ?>" class="dropdown-item kendala_click">
						<i class="fa fa-file mr-2"></i>Laporan Kendala
						<span class="float-right badge badge-warning text-sm notif_kendala"></span>
					</a>
					<div class="dropdown-divider"></div>
					<a href="#" class="dropdown-item dropdown-footer">
						<span class="total_notif"></span> Notifikasi
					</a>
				</div>
			</li>
			<li class="nav-item">
				<a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#">
					<i class="fa fa-th-large"></i>
				</a>
			</li>
		</ul>
	</nav>
	<!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="?" class="brand-link" style="background-color: #FFFFFF;">
      <img src="<?php echo base_url();?>assets/images/sucofindologo.png" alt="AdminLTE Logo" class="brand-image"
           style="opacity: .8">
      <span class="brand-text font-weight-light" style="color: #000000;">SIMLab</span>
    </a>

	<!-- Sidebar -->
	<div class="sidebar">
	<!-- Sidebar user panel (optional) -->
		<div class="user-panel mt-3 pb-3 mb-3 d-flex">
			<div class="image">
				<?php
					if ($avatar != "") {
						if (file_exists('./assets/uploads/user_pic/'.$avatar)) {
				?>
							<img src="<?php echo base_url();?>assets/uploads/user_pic/<?php echo $avatar; ?>" class="img-circle elevation-2" style="width: 2.1rem;" alt="User Image">
				<?php
						} else {
				?>
							<img src="<?php echo base_url();?>assets/images/placeholder_pic.png" class="img-circle elevation-2" style="width: 2.1rem;" alt="User Image">
				<?php
						}
					} else {
				?>
						<img src="<?php echo base_url();?>assets/images/placeholder_pic.png" class="img-circle elevation-2" style="width: 2.1rem;" alt="User Image">
				<?php
					}
				?>
				
			</div>
			<div class="info">
				<a href="#" class="d-block">
					<?php echo $namaLengkap; ?>
				</a>
			</div>
		</div>

		<!-- Sidebar Menu -->
		<nav class="mt-2">
			<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
			<?php echo $menu; ?>
			<!-- Add icons to the links using the .nav-icon class
			with font-awesome or any other icon font library -->


			<li class="nav-header">ACCOUNT</li>
			<li class="nav-item">
				<a href="<?php echo base_url('login_auth/logout'); ?>" class="nav-link">
					<i class="nav-icon fa fa-key"></i>
					<p>Logout</p>
				</a>
			</li>

			</ul>
		</nav>
		<!-- /.sidebar-menu -->
	</div>
	<!-- /.sidebar -->
  </aside>
  