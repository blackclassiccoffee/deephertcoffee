<!DOCTYPE html>
<html lang="en">
<head>
  	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
  	<meta http-equiv="x-ua-compatible" content="ie=edge">

  	<title>SIMLab | <?php echo $nama_menu; ?></title>

  	<link rel="shortcut icon" type="image/png" href="<?php echo base_url();?>assets/images/favicon.png"/>
  	<!-- Font Awesome Icons -->
  	<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/font-awesome2/css/all.css">
  	<!-- IonIcons -->
  	<link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  	<!-- Theme style -->
  	<link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/adminlte.min.css">
  	<!-- Google Font: Source Sans Pro -->
  	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  	<link rel="stylesheet" href="<?php echo base_url();?>assets/gritter/css/jquery.gritter.css">
  	<link rel="stylesheet" href="<?php echo base_url();?>assets/select2/dist/css/select2.css">
  	<link rel="stylesheet" href="<?php echo base_url() ?>assets/datetimepicker/jquery.datetimepicker.css">
	
  	<!-- Include SmartWizard CSS -->
  	<link href="<?php echo base_url() ?>assets/formstep/dist/css/smart_wizard.css" rel="stylesheet" type="text/css" />

  	<link href="<?php echo base_url() ?>assets/formstep/dist/css/smart_wizard_theme_dots.css" rel="stylesheet" type="text/css" />
	
  	<script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
	
  	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@8.13.0/dist/sweetalert2.css">
	
  	
  
</head>


	<style type="text/css">
  		body {
  			font-family: Rubik,sans-serif;
  			font-size: .875rem;
  		}
		.content-header h1 {
   			font-size: 21px;
    		margin: 0;
    		font-weight: bolder;
    		opacity: .70;
    		/* margin-top: 3px; */
    		line-height: 1.5;
		}
		input {
			padding-left: 5px;
		}
		.select2-container .select2-selection--single .select2-selection__rendered {
			padding-left: 0px;
			line-height: 25px;
		}
	</style>