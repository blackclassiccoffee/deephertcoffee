<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>SIMLab - Login</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="shortcut icon" type="image/png" href="<?php echo base_url();?>assets/images/favicon.png"/>
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/adminlte.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/iCheck/square/blue.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <img src="<?php echo base_url();?>assets/images/sucofindologo.png" width="150"/>
  </div>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">

      <form name="form1" action="<?php echo site_url('login_auth'); ?>" method="post">
        <div class="form-group has-feedback">
          <span class="form-control-feedback">Email :</span>
          <input type="hidden" name='session_data' value="<?php echo $this->session->userdata('session_id'); ?>">
          <input type="text" name="username" class="form-control" placeholder="Email">
        </div>
        <div class="form-group has-feedback">
          <span class="form-control-feedback">Password :</span>
          <input type="password" name="password" class="form-control" placeholder="Password">
        </div>
        <div class="row">
          <!-- /.col -->
			<div class="col-12">
				<?php 
					if($this->session->flashdata('Gagal')){
				?>
				<div class="alert alert-danger">
					<?php 
						echo "<span class='fa fa-exclamation-circle'></span> ";
						echo $this->session->flashdata('Gagal'); 
					?>
				</div>
				<?php
					}
				?>
				<div class="text-right mb-4">
					<a href="<?php echo base_url('settings/password/resetp'); ?>">Lupa Password?</a>
				</div>
			</div>
          	<div class="col-12">
            	<button type="submit" class="btn btn-primary btn-block btn-flat">Login</button>
          	</div>
          <!-- /.col -->
        </div>
        
        <div class=" text-center mt-3">
        	Assembled @2019
        </div>
      </form>
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->
</body>
</html>
