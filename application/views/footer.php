   <audio id="myAudio">
	  <source src="<?php echo base_url();?>assets/sirene.mp3" type="audio/mpeg">
	  Your browser does not support the audio element.
	</audio>
	
	<audio id="BongkarAudio">
	  <source src="<?php echo base_url();?>assets/notification.mp3" type="audio/mpeg">
	  Your browser does not support the audio element.
	</audio>
  
   <footer class="main-footer">
    <div class="float-right d-none d-sm-block">
      <b>Version</b> 2.0
    </div>
    Assambled by Team Socofindo @2019
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="<?php echo base_url() ?>assets/fixed-table/tableHeadFixer.js"></script>

<script src="<?php echo base_url();?>assets/gritter/js/jquery.gritter.min.js"></script>
<script src="<?php echo base_url();?>assets/select2/dist/js/select2.js"></script>

<script src="<?php echo base_url() ?>assets/datetimepicker/jquery.datetimepicker.js"></script>
<script src="<?php echo base_url() ?>assets/js/php.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/input-mask/jquery.inputmask.numeric.extensions.js"></script>
<!-- Bootstrap -->
<script src="<?php echo base_url();?>assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE -->
<script src="<?php echo base_url();?>assets/dist/js/adminlte.js"></script>
<script type="text/javascript" src="http://techlaboratory.net/demos/SmartWizard43/dist/js/jquery.smartWizard.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8.13.0/dist/sweetalert2.all.js"></script>
<script src="https://cdn.jsdelivr.net/npm/simplestorage.js@0.2.1/simpleStorage.min.js"></script>
<!-- OPTIONAL SCRIPTS -->
<script src="<?php echo base_url();?>assets/dist/js/demo.js"></script>
<script src="<?php echo base_url();?>assets/js/WLoad.js"></script>
<link rel="stylesheet" href="<?php echo base_url();?>assets/js/WLoad.css">

<script language="javascript">
	
	var audio;
	var audioBongkar;
	
	$(document).ready(function(){
		
		$('[data-toggle="tooltip"]').tooltip();
		$('[data-toggle="popover"]').popover({
			trigger : 'hover'
		});
		
		<?php 
			if ($this->session->flashdata('oke')) {
				$pesan = $this->session->flashdata('oke');
		?>
					suksesPesan('<?php echo $pesan;?>');
		<?php
			} 
		?>
		$('.datetimepicker').datetimepicker({
			format:'Y-m-d H:i:s',
			timepickerScrollbar:true
		});
		$('.datepicker').datetimepicker({
			format:'Y-m-d',
			timepicker:false,
			timepickerScrollbar:true
		});
		
		$(".select2").select2();
		
		//getNotif();
		setInterval(function(){ 
			//getNotif();
			//getPanic();
			//getBongkarMuat();
		}, 10000);
		
		$(".inputmask-number,.number").inputmask("decimal", {
			radixPoint: ",",
			autoGroup: true,
			groupSeparator: ".",
			groupSize: 3,
			autoUnmask: true
		});
		
		$(".number-string").inputmask("decimal");
		$(".number-string").css('text-align', 'left');
		
		audio = document.getElementById("myAudio");
		audioBongkar = document.getElementById("BongkarAudio");
		
		$('.panic_click').click(function(){
			updatePanic();
			audio.stop();
		});
		
		$('.bongkar_click').click(function(){
			updateBongkarReaded();
			audioBongkar.stop();
		});
		
		$('.kendala_click').click(function(){
			updateKendalaReaded();
		});
		
	});
	
	function clearLocalStorage() {
		localStorage.clear();
	}
	
	function suksesPesan($msg){
		var unique_id = $.gritter.add({
			title: 'Berhasil!',
			text: '<i class="icon-ok-sign"></i> '+$msg+'!',
			sticky: true,
			class_name: 'my-sticky-class'
		});
	
		setTimeout(function(){
	
			$.gritter.remove(unique_id, {
				fade: true,
				speed: 'slow'
			});
	
		}, 4000)
	
	
		return false;
	}
	
	function getPanic() {
		$.ajax({
			url   : '<?php echo base_url('laporan/lapor_n/getPanic'); ?>',
			type  : 'POST',
			async : false,
			dataType: 'json'
		}).done(function(data){
			if(data.length > 0) {
				
				audio.play();
				/*for (var x = 0; x<data.length; x++) {
					if (data[x].status_notif == 0) {
						
					}
				}*/
				
			}
		});
	}
	
	function getBongkarMuat() {
		$.ajax({
			url   : '<?php echo base_url('laporan/lapor_n/getBongkarMuat'); ?>',
			type  : 'POST',
			async : false,
			dataType: 'json'
		}).done(function(data){
			if(data.length > 0) {
				
				audioBongkar.play();
				/*for (var x = 0; x<data.length; x++) {
					if (data[x].status_notif == 0) {
						
					}
				}*/
				
			}
		});
	}
	
	function updatePanic() {
		$.ajax({
			url   : '<?php echo base_url('laporan/lapor_n/updatePanicReaded'); ?>',
			type  : 'POST',
			async : false,
			dataType: 'json'
		}).done(function(data){
			if(data) {
				
			}
		});
	}
	
	function updateBongkarReaded() {
		$.ajax({
			url   : '<?php echo base_url('laporan/lapor_n/updateBongkarReaded'); ?>',
			type  : 'POST',
			async : false,
			dataType: 'json'
		}).done(function(data){
			if(data) {
				
			}
		});
	}
	
	function updateKendalaReaded() {
		$.ajax({
			url   : '<?php echo base_url('laporan/lapor_n/updateKendalaReaded'); ?>',
			type  : 'POST',
			async : false,
			dataType: 'json'
		}).done(function(data){
			if(data) {
				
			}
		});
	}
	
	function getNotif() {
		$.ajax({
			url   : '<?php echo base_url('laporan/lapor_n/getNotif'); ?>',
			type  : 'POST',
			dataType: 'json'
		}).done(function(data){
			if(data) {
				
				$total_panic = 0;
				if (data.hasOwnProperty('dataPanic')) {
					$('.notif_panic').html(data.dataPanic);
					$total_panic = eval(data.dataPanic);
				}
				
				$total_kendala = 0;
				if (data.hasOwnProperty('dataKendala')) {
					$('.notif_kendala').html(data.dataKendala);
					$total_kendala = eval(data.dataKendala);
				}
				
				$total_bongkar = 0;
				if (data.hasOwnProperty('dataBongkar')) {
					$('.notif_bongkar').html(data.dataBongkar);
					$total_bongkar = eval(data.dataBongkar);
				}
				
				$total_notif = $total_panic + $total_kendala + $total_bongkar;
				$('.total_notif').html($total_notif);
			}
		});
	}
	
</script>

</body>
</html>