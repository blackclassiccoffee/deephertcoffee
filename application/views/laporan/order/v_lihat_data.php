
<style>
	table tr td {
		padding: 5px;
		font-family:Tahoma, Verdana, Arial;
		font-size:12px;
	}
	#WLoad {
		height: 100%;
	}
	.order {
		margin-left: 15px;
		font-weight: bold;
	}
	.orderNumber {
		font-weight: bold;
		color: #4E8AFF;
	}
	.orderDate {
		margin-left: 15px;
	}
	
	.orderHeader {
		font-weight: bold;
		font-size: 15px;
		margin-bottom: 10px;
	}
	
	table.table tr th {
		font-family:Tahoma, Verdana, Arial;
		font-size:12px;
		padding: 5px;
		vertical-align: middle;
	}
	
	table.table tr td {
		font-family:Tahoma, Verdana, Arial;
		font-size:12px;
		padding: 5px;
	}
</style>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
<script src="<?php echo base_url();?>assets/js/WLoad.js"></script>
<link rel="stylesheet" href="<?php echo base_url();?>assets/js/WLoad.css">
<div id="WLoad">
  	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	  	<tbody>
			<tr>
				<td width="26%" valign="top">
					<i class="fa fa-list"></i> Order
					<span class="orderNumber">
						#<?php echo $onLap->no_order; ?>
					</span>
					<div class="order">
						<?php echo $onLap->nama_customer; ?>
					</div>
				</td>
				<td width="29%" valign="top">
					<i class="fa fa-map-marker-alt"></i> Alamat Pengiriman
					<div class="order">
						<?php echo $onLap->alamat; ?>
				  	</div>
					<div class="orderDate" style="margin-top: 5px;">
						<i class="fa fa-calendar"></i>
						<span>
							<?php echo date('d M Y', strtotime($onLap->created_at)); ?>
						</span>
					</div>
				</td>
				<td width="14%" valign="top">
					<i class="fa fa-user"></i> Driver/Sopir
					<div class="order">
						<?php echo $onLap->nama_driver; ?>
			  	  </div>
				</td>
				<td width="15%" valign="top"></td>
				<td width="16%" valign="top"></td>
			</tr>
			<tr>
			  	<td colspan="5">
		  	    </td>
		  	</tr>
    	</tbody>
  	</table>
	<hr>
	<div class="orderHeader">
		<i class="fa fa-list"></i>  Detail Pesanan
	</div>
  	<table width="100%" border="0" class="table" cellspacing="0" cellpadding="0">
		<thead>
			<tr>
				<th width="6%" height="48" style="text-align: center;">No.</th>
				<th width="56%">Nama Barang</th>
				<th width="15%" style="text-align: right;">Jumlah</th>
				<th width="23%">Satuan</th>
			</tr>
		</thead>
		<tbody>
			<?php
				$i = 0;
				foreach ($onLapDetail as $data) {
					$i++;
			?>
				<tr>
					<td align="center"><?php echo $i; ?></td>
					<td><?php echo $data->nama_barang; ?></td>
					<td align="right">
						<?php echo $data->jumlah; ?>
					</td>
					<td><?php echo $data->nama_satuan; ?></td>
				</tr>
			<?php
				}
			?>
		</tbody>
  	</table>
</div>

<script>

	$(function(){
		
		$('#WLoad').Wload({
			text : "Loading"
		});
		
		$('#WLoad').Wload('hide',{time:3000})
		
	})

</script>