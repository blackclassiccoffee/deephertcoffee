<style type="text/css">

	table#data_tabel tr td {
		padding: 5px;
		font-family:Tahoma, Verdana, Arial;
		font-size:11px;
	}

	table#data_tabel tr td.verifikasi {
		color: #FFFFFF;
	}
	
	table#data_tabel tr td.verifikasi a {
		color: #FFFFFF;
	}
	
	table#data_tabel tr td.pending {
		color: #000000;
	}
	
	table#data_tabel tr th {
		font-family:Tahoma, Verdana, Arial;
		font-size:12px;
		padding: 5px;
		vertical-align: middle;
	}
	#parent {
		height: 300px;
	}

	#data_tabel {
		width: 1300px !important;
	}
	
</style>

<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
		  		<div class="col-sm-6">
					<h1>Laporan Pengiriman Barang</h1>
		  		</div>
		  		<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="#">Laporan</a></li>
						<li class="breadcrumb-item active">Laporan Pengiriman Barang</li>
					</ol>
         		</div>
			</div>
			<hr>
	  	</div><!-- /.container-fluid -->
	</section>
	
	<section class="content">
    	<div class="container-fluid">
        <!-- SELECT2 EXAMPLE -->
			<div class="card card-default">
				<form name="form1" action="" method="get">
				<div class="card-header">
					<h3 class="card-title">
						Filter/Pencarian
					</h3>

					<div class="card-tools">
					  <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
					</div>
			  	</div>
			  	<div class="card-body">
            		<div class="row">
              			<div class="col-md-12">
              				<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
								  <td></td>
								  <td></td>
								  <td></td>
								</tr>
								<tr>
								  <td width="16%"></td>
								  <td width="2%"></td>
								  <td width="82%"></td>
								</tr>
								<tr>
								  <td height="35">Customer/Outlet</td>
								  <td>:</td>
								  <td>
									  	<input type="hidden" id="kantor" name="kantor" value="<?php echo $KANTOR; ?>">
								  		<input class="col-4 input-sm" type="text" name="customer" placeholder="Customer/Outlet" value="<?php echo isset($_GET['customer']) && $_GET['customer'] != "" ? $_GET['customer'] : ""; ?>">
								  </td>
							  	</tr>
								<tr>
								  <td height="35">Status</td>
								  <td>:</td>
								  <td>
								  	<select class="form-control select2 col-3 input-sm" name="status">
										<option value="">Semua</option>
										<option value="Pending" <?php echo isset($_GET['status']) && $_GET['status'] == "Pending" ? "selected" : ""; ?>>Menunggu Pengiriman</option>
										<option value="Proses" <?php echo isset($_GET['status']) && $_GET['status'] == "Proses" ? "selected" : ""; ?>>Proses Kirim</option>
										<option value="Terkirim" <?php echo isset($_GET['status']) && $_GET['status'] == "Terkirim" ? "selected" : ""; ?>>Terkirim</option>
									</select>
								  </td>
							  	</tr>
								<tr>
								  <td height="35">Tanggal</td>
								  <td>:</td>
								  <td>
								  	<div class="row" style="padding-left: 8px;">
								  		<input class="col-2 datepicker" type="text" name="tanggal_awal" placeholder="Tanggal" value="<?php echo isset($_GET['tanggal_awal']) && $_GET['tanggal_awal'] != "" ? $_GET['tanggal_awal'] : ""; ?>">
								  		s/d
								  		<input class="col-2 datepicker" type="text" name="tanggal_akhir" placeholder="Tanggal" value="<?php echo isset($_GET['tanggal_akhir']) && $_GET['tanggal_akhir'] != "" ? $_GET['tanggal_akhir'] : ""; ?>">
								  	</div>
								  	
								  </td>
							  	</tr>
								
								<tr>
								  <td></td>
								  <td></td>
								  <td></td>
								</tr>
								<tr>
								  <td></td>
								  <td></td>
								  <td></td>
								</tr>
							</table>
						</div>
					</div>
				</div>
				<div class="card-footer clearfix">
					<input type="submit" name="cari" id="cari" class="btn btn-primary" value="Submit" />
				</div>
				</form>
			</div>
			<!-- Filter/Pencarian -->
			
			<div class="card card-default">
				<div class="card-header">
					<h3 class="card-title">
						Data Laporan
					</h3>

					<div class="card-tools">
                    	<?php if($hak_akses->role_download == 1): ?>
                          <a href="<?php echo base_url('laporan/lapor_kendala/excel')."?".$_SERVER['QUERY_STRING']; ?>" class="btn">
                            <i class="fa fa-download"></i> Export Excel
                          </a>
                      	<?php endif; ?>
					</div>
			  	</div>
			  	
			  	<div class="card-body p-0">
					<blockquote>&nbsp;</blockquote>
					<table width="100%" cellspacing="0" id="data_tabel" class="table" cellpadding="0">
					  	<thead>
							<tr>
						  		<th width="3%" height="45" style="text-align: center;">No</th>
						  		<th width="8%" style="text-align: center;">Tanggal</th>
								<th width="9%" style="text-align: center;">No. Order</th>
						  		<th width="11%" align="left">Nama Customer</th>
						  		<th width="22%" align="left">Alamat</th>
						  		<th width="10%">Driver</th>
						  		<th width="10%">Admin</th>
						  		<th width="13%" style="text-align: left;">Status</th>
						  		<th width="4%" style="text-align: center;">#</th>
						  		<th width="5%" style="text-align: center;">#</th>
						  		<th width="5%" style="text-align: center;">#</th>
							</tr>
					  	</thead>
					  	<tbody>
						<?php

							$per_page = 10;

							if($this->uri->segment(4) != ""){
								$page = ($this->uri->segment(4));
								$poss = (($page-1) * $per_page);
							} else {
								$page = 1;
								$poss = 0;
							}

							$no = $poss;
							foreach($onLaporan as $data) {  
								$no++;
								if ($data->status == "Terkirim") {
									$proved    = "#00a65a";
									$proved_tx = "verifikasi";
								} else if ($data->status == "Proses") {
									$proved    = "#f39c12";
									$proved_tx = "pending";
								} else {
									$proved    = "";
									$proved_tx = "";
								}
						?>
							<tr bgcolor="<?php echo $proved; ?>">
						  		<td align="center" class="<?php echo $proved_tx; ?>">
									<?php echo $no; ?>
						  		</td>
						  		<td align="center" class="<?php echo $proved_tx; ?>">
									<?php echo date('d-m-Y H:i', strtotime($data->created_at)); ?>
						  		</td>
						  		<td align="center" class="<?php echo $proved_tx; ?>">
									<?php echo $data->no_order; ?>
								</td>
							  	<td class="<?php echo $proved_tx; ?>">
									<?php echo $data->nama_customer; ?>
							  	</td>
						  		<td class="<?php echo $proved_tx; ?>">
									<?php 
										if (strlen($data->alamat) <= 50) {
											echo $data->alamat;
										} else {
											echo "<a data-toggle='popover' data-placement='right' data-content='".$data->alamat."'>".substr($data->alamat, 0, 50)."...</a>";
										}
								  	?>
						  		</td>
						  		<td align="left" class="<?php echo $proved_tx; ?>">
									<?php echo $data->nama_driver; ?>
					      		</td>
								<td align="left" class="<?php echo $proved_tx; ?>">
									<?php echo $data->Name; ?>
					      		</td>
						  		<td class="<?php echo $proved_tx; ?>">
									<?php
										if ($data->status == "Proses") {
											echo "Proses Kirim";
										} else if ($data->status == "Pending") {
											echo "Menunggu Pengiriman";
										} else {
											echo $data->status;
										}
									?>
								</td>
						  		<td align="center" class="<?php echo $proved_tx; ?>">
						    		<?php if($hak_akses->role_read == 1): ?>
										<a href="javascript:void(0)" onClick="lihatData(<?php echo $data->id; ?>)" title="Hapus">
											[ <i class="fa fa-search"></i> ]
										</a>
									<?php endif; ?>
									<?php if($hak_akses->role_read != 1): ?>
										<a href="javascript:void(0)" title="Akses terbatas!" onClick="alert('Akses Terbatas!')"> 
											[ <i class="fa fa-exclamation-circle"></i> ]
										</a>
									<?php endif; ?>
					      		</td>
						  		<td align="center" class="<?php echo $proved_tx; ?>">
                            		<?php if($hak_akses->role_read == 1): ?>
										<a href="javascript:void(0)" onClick="lihat_track(<?php echo $data->id_driver; ?>, <?php echo $data->id; ?>, <?php echo $data->lat_start; ?>, <?php echo $data->lng_start; ?>)" title="Lihat Peta">
											[ <i class="fa fa-map-marker-alt"></i> ]
										</a>
									<?php endif; ?>
									<?php if($hak_akses->role_read != 1): ?>
										<a href="javascript:void(0)" title="Akses terbatas!" onClick="alert('Akses Terbatas!')"> 
											[ <i class="fa fa-exclamation-circle"></i> ]
										</a>
									<?php endif; ?>
						  		</td>
						  		<td align="center" class="<?php echo $proved_tx; ?>">
									<?php if($hak_akses->role_delete == 1): ?>
										<a href="javascript:void(0)" onClick="hapus(<?php echo $data->id; ?>)" title="Hapus">
											[ <i class="fa fa-trash"></i> ]
										</a>
									<?php endif; ?>
									<?php if($hak_akses->role_delete != 1): ?>
										<a href="javascript:void(0)" title="Akses terbatas!" onClick="alert('Akses Terbatas!')"> 
											[ <i class="fa fa-exclamation-circle"></i> ]
										</a>
									<?php endif; ?>
                          		</td>
							</tr>
						<?php
							}
						?>
					  	</tbody>
					</table>
				</div>
				<div class="card-footer clearfix">
               		<div class="m-0 float-left">
                  		<span class="badge bg-success">Terkirim</span>
                  		<span class="badge bg-warning">Proses Kirim</span>
                	</div>
                	
                	<?php echo $links; ?>
                	
                	<div class="m-0 float-right" style="padding-top: 2px; padding-right: 10px;">
                		<?php echo $pagination_info; ?>
					</div>
              	</div>
			</div>
			
	  	</div>
	</section>
	
</div>

<div id="dialog-track_record" title="Detail Track Record"></div>
<div id="dialog-lihat_data" title="Lihat Laporan"></div>

<?php

if(isset($_GET['cari'])) {
	//echo "<script>$(function(){ get_supplier('".$_GET['kantor']."', '".$_GET['pelapor']."'); });</script>";
}

?>

<script language="javascript">
	
	$(document).ready(function(){
		$('#reset').click(function(){
			$('input[type=text]').val('');
			$('select[name=jenis_laporan]').val('');
		});
		
		$("#data_tabel").tableHeadFixer({"right" : 3}); 
	});
	
	function hapus(id) {
		if (confirm('Apakah data akan dihapus?')) {
			$.ajax({
				url   	  : '<?php echo base_url('laporan/lapor_kendala/deleteLaporan'); ?>',
				type  	 : 'POST',
				dataType : 'json',
				data : { 
					"id" : id
				}
			}).done(function(data){
				if(data) {
				  if (data.status == true) {
				  	 location.reload();
				  }
				}
			});
		}
	}

	function lihat_track(id_user, id_bongkar, lat, lng) {

		$('#dialog-track_record').html('Memuat Halaman ...');
		$('#dialog-track_record').load("<?php echo base_url('laporan/order/lihat_track'); ?>?id_user="+id_user+"&id_bongkar="+id_bongkar+"&lat="+lat+"&lng="+lng+"");
		$('#dialog-track_record').dialog({
			height: 650,
			width: 1020,
			modal: true,
			open: function(event, ui) {
			  	$(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
				$(".ui-dialog", ui.dialog | ui).css('z-index', 9999);
				$(event.target).parent().css('position', 'fixed');
				$(event.target).parent().css('top', '50%');
				$(event.target).parent().css('left', '50%');
				$(event.target).parent().css('transform', 'translate(-50%, -50%)');
			},
			buttons: [{
				id    : "close",
				text  : "Tutup",
				click : function(){
					location.reload();
				}
			}]
		});

	}
	
	function lihatData(id) {

		$('#dialog-lihat_data').html('Memuat Halaman ...');
		$('#dialog-lihat_data').load("<?php echo base_url('laporan/order/lihat_data'); ?>?id="+id+"");
		$('#dialog-lihat_data').dialog({
			height: 460,
			width: 800,
			modal: true,
			open: function(event, ui) {
			  	$(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
				$(".ui-dialog", ui.dialog | ui).css('z-index', 9999);
				$(event.target).parent().css('position', 'fixed');
				$(event.target).parent().css('top', '50%');
				$(event.target).parent().css('left', '50%');
				$(event.target).parent().css('transform', 'translate(-50%, -50%)');
			},
			buttons: [{
				id    : "close",
				text  : "Tutup",
				click : function(){
					location.reload();
				}
			}]
		});
	}

	
</script>