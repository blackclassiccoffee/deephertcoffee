
<style>
	table tr td {
		padding: 5px;
		font-family:Tahoma, Verdana, Arial;
		font-size:12px;
	}
</style>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody>
    <tr>
      <td colspan="3" valign="top"><table width="100%" border="0">
        <tr>
          <td width="31%">Pelapor</td>
          <td width="3%">:</td>
          <td width="66%"><?php echo $onLap->Name; ?></td>
        </tr>
        <tr>
          <td>Nomor Polisi</td>
          <td>:</td>
          <td><?php echo $onLap->plat_nomor; ?></td>
        </tr>
        <tr>
          <td>Jenis Laporan</td>
          <td>:</td>
          <td><?php echo $onLap->jenis_laporan; ?></td>
        </tr>
        <tr>
          <td>Tanggal</td>
          <td>:</td>
          <td><?php echo date('d M Y - H:i', strtotime($onLap->created_at)); ?></td>
        </tr>
        <tr>
          <td>Keterangan Kendala</td>
          <td>:</td>
          <td><?php echo $onLap->keterangan; ?></td>
        </tr>
        <tr>
          <td valign="top">Konfirmasi</td>
          <td valign="top">:</td>
          <td valign="top"><?php echo $onLap->keterangan_proved; ?></td>
        </tr>
        <tr>
          <td valign="top">Tanggal Konfirmasi</td>
          <td valign="top">:</td>
          <td valign="top"><?php echo date('d M Y - H:i', strtotime($onLap->date_proved)); ?></td>
        </tr>
        <tr>
          <td valign="top">Admin</td>
          <td valign="top">:</td>
          <td valign="top"><?php echo $onLap->NameProved; ?></td>
        </tr>
      </table></td>
      <td width="43%" align="center">
      	<img src="http://jasalia.com/trackbus_serv/public/uploads/Lapor/<?php echo $onLap->foto; ?>" width="200">
      </td>
    </tr>
  </tbody>
</table>
