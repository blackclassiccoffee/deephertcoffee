<style type="text/css">

	table#data_tabel tr td {
		padding: 5px;
		font-family:Tahoma, Verdana, Arial;
		font-size:11px;
	}

	table#data_tabel tr td.verifikasi {
		color: #FFFFFF;
	}
	
	table#data_tabel tr td.verifikasi a {
		color: #FFFFFF;
	}
	
	table#data_tabel tr td.pending {
		color: #000000;
	}
	
	table#data_tabel tr th {
		font-family:Tahoma, Verdana, Arial;
		font-size:12px;
		padding: 5px;
		vertical-align: middle;
	}
	#parent {
		height: 300px;
	}

	#data_tabel {
		width: 1600px !important;
	}
	
</style>

<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
		  		<div class="col-sm-6">
					<h1>Laporan Bongkar/Muat</h1>
		  		</div>
		  		<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="#">Laporan</a></li>
						<li class="breadcrumb-item active">Laporan Bongkat/Muat</li>
					</ol>
         		</div>
			</div>
			<hr>
	  	</div><!-- /.container-fluid -->
	</section>
	
	<section class="content">
    	<div class="container-fluid">
        <!-- SELECT2 EXAMPLE -->
			<div class="card card-default">
				<form name="form1" action="" method="get">
				<div class="card-header">
					<h3 class="card-title">
						Filter/Pencarian
					</h3>

					<div class="card-tools">
					  <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
					</div>
			  	</div>
			  	<div class="card-body">
            		<div class="row">
              			<div class="col-md-12">
              				<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
								  <td></td>
								  <td></td>
								  <td></td>
								</tr>
								<tr>
								  <td width="16%"></td>
								  <td width="2%"></td>
								  <td width="82%"></td>
								</tr>
								<tr>
								  <td height="35">Pelapor</td>
								  <td>:</td>
								  <td>
								  	<input type="hidden" id="kantor" name="kantor" value="<?php echo $KANTOR; ?>">
								  	<input type="text" class="col-3" name="pelapor" placeholder="Nama Pelapor" value="<?php echo isset($_GET['pelapor']) && $_GET['pelapor'] != "" ? $_GET['pelapor'] : ""; ?>">
								  </td>
							  	</tr>
								<tr>
								  <td height="45">Nomor Polisi</td>
								  <td>:</td>
								  <td>
								  	<input type="text" class="col-3" name="nopol" placeholder="H 0000 DA" value="<?php echo isset($_GET['nopol']) && $_GET['nopol'] != "" ? $_GET['nopol'] : ""; ?>">
								  </td>
							  </tr>
							  	<tr>
								  <td height="35">Distributor</td>
								  <td>:</td>
								  <td>
								  	<select class="form-control select2 col-3 input-sm" name="id_distributor">
								  		<option value="">Semua</option>
								  		<?php
											foreach($distributor as $data) {
												$sel = $data->id == $_GET['id_distributor'] ? "selected" : "";
												echo "<option value='".$data->id."' ".$sel.">".$data->nama_distributor."</option>";
											}
										?>
								  	</select>
								  </td>
							  	</tr>
								<tr>
								  <td height="35">Nomor SPJ</td>
								  <td>:</td>
								  <td>
								  	<input type="text" class="col-3" name="nospj" placeholder="XXX" value="<?php echo isset($_GET['nopol']) && $_GET['nospj'] != "" ? $_GET['nospj'] : ""; ?>">
								  </td>
							  	</tr>
								<tr>
								  <td height="43">Asal</td>
								  <td>:</td>
								  <td><div class="row" style="padding-left: 8px;">
								    <input type="text" class="col-3" name="asal" placeholder="Asal" value="<?php echo isset($_GET['asal']) && $_GET['asal'] != "" ? $_GET['asal'] : ""; ?>">
								    Tujuan :
								    <input type="text" class="col-3" name="tujuan" placeholder="Tujuan" value="<?php echo isset($_GET['tujuan']) && $_GET['tujuan'] != "" ? $_GET['tujuan'] : ""; ?>">
								    </div></td>
							  	</tr>
								<tr>
								  <td height="35">Status Tindakan</td>
								  <td>:</td>
								  <td>
								  	<select class="form-control select2 col-3 input-sm" name="status_proved">
										<option value="">Semua</option>
										<option value="1" <?php echo isset($_GET['status_proved']) && $_GET['status_proved'] == "1" ? "selected" : ""; ?>>Terverifikasi</option>
										<option value="0" <?php echo isset($_GET['status_proved']) && $_GET['status_proved'] == "0" ? "selected" : ""; ?>>Pending</option>
									</select>
								  </td>
							  	</tr>
								<tr>
								  <td height="35">Tanggal</td>
								  <td>:</td>
								  <td>
								  	<div class="row" style="padding-left: 8px;">
								  		<input class="col-3 datepicker" type="text" name="tanggal_awal" placeholder="Tanggal" value="<?php echo isset($_GET['tanggal_awal']) && $_GET['tanggal_awal'] != "" ? $_GET['tanggal_awal'] : ""; ?>">
								  		s/d
								  		<input class="col-3 datepicker" type="text" name="tanggal_akhir" placeholder="Tanggal" value="<?php echo isset($_GET['tanggal_akhir']) && $_GET['tanggal_akhir'] != "" ? $_GET['tanggal_akhir'] : ""; ?>">
								  	</div>
								  	
								  </td>
							  	</tr>
								<tr>
								  <td></td>
								  <td></td>
								  <td></td>
								</tr>
								<tr>
								  <td>
									
								  </td>
								  <td></td>
								  <td></td>
								</tr>
								<tr>
								  <td></td>
								  <td></td>
								  <td></td>
								</tr>
							</table>
						</div>
					</div>
				</div>
				<div class="card-footer clearfix">
					<button type="submit" name="cari" id="cari" class="btn btn-danger" value="1">
						<i class="fa fa-search"></i> Submit
					</button>
				</div>
				</form>
			</div>
			<!-- Filter/Pencarian -->
			
			<div class="card card-default">
				<div class="card-header">
					<h3 class="card-title">
						Data Laporan
					</h3>

					<div class="card-tools">
				  	  <a href="javascript:void(0)" onClick="lihat_grafik('<?php echo $_SERVER['QUERY_STRING']; ?>')" class="btn">
						<i class="fa fa-chart-bar"></i> Lihat Grafik
					  </a>
					  <?php if($hak_akses->role_download == 1): ?>
                          <a href="<?php echo base_url('laporan/bongkar_muat/excel')."?".$_SERVER['QUERY_STRING']; ?>" class="btn">
                            <i class="fa fa-download"></i> Export Excel
                          </a>
                      	<?php endif; ?>
					</div>
			  	</div>
			  	
			  	<div class="card-body p-0">
					<table width="100%" cellspacing="0" id="data_tabel" class="table" cellpadding="0">
					  <thead>
						<tr>
						  <th width="2%" height="45" style="text-align: center;">No</th>
						  <th width="5%" style="text-align: center;">Tanggal</th>
						  <th width="8%" align="left">Nama</th>
						  <th width="6%" align="left">No Polisi</th>
						  <th width="9%" align="left">Distributor</th>
						  <th width="7%" align="left">No. SPJ</th>
						  <th width="6%" align="left">No. Manifest</th>
						  <th width="6%" style="text-align: right;">Tonase Muat</th>
						  <th width="7%" style="text-align: right;">Tonase Bongkar</th>
						  <th width="7%">Asal</th>
						  <th width="6%">Tujuan</th>
						  <th width="6%" style="text-align: right;">Uang Jalan</th>
						  <th width="7%" style="text-align: right;">Ongkos Angkut</th>
						  <th width="5%" style="text-align: right;">Total</th>
						  <th width="5%" style="text-align: center;">#</th>
						  <th width="4%" style="text-align: center;">#</th>
						  <th width="4%" style="text-align: center;">#</th>
					    </tr>
					  </thead>
					  <tbody>
						<?php

							$per_page = 10;

							if($this->uri->segment(4) != ""){
								$page = ($this->uri->segment(4));
								$poss = (($page-1) * $per_page);
							} else {
								$page = 1;
								$poss = 0;
							}

							$no = $poss;
							foreach($onLaporan as $data) {  
								$no++;
								$proved_tx   = $data->ongkos_angkut != 0 ? "verifikasi" : "pending";
								if ($data->tonase_bongkar != 0 && $data->ongkos_angkut != 0) {
									$proved = "#00a65a";
								} else if ($data->tonase_bongkar == 0 && $data->ongkos_angkut == 0) {
									$proved = "#e26a4f";
								} else {
									$proved = "#f39c12";
								}
								
								$total = (( $data->tonase_bongkar * $data->ongkos_angkut ) - $data->uang_jalan);
						?>
						<tr bgcolor="<?php echo $proved; ?>">
						  <td align="center" class="<?php echo $proved_tx; ?>">
							<?php echo $no; ?>
						  </td>
						  <td align="center" class="<?php echo $proved_tx; ?>"><?php echo date('d-m-Y', strtotime($data->created_at)); ?></td>
						  <td class="<?php echo $proved_tx; ?>">
							<?php echo $data->Name; ?>
						  </td>
						  <td class="<?php echo $proved_tx; ?>">
						  	<?php echo $data->plat_nomor; ?>
						  </td>
						  <td class="<?php echo $proved_tx; ?>">
						  	<?php echo $data->nama_distributor; ?>
						  </td>
						  <td class="<?php echo $proved_tx; ?>">
							<?php echo $data->no_spj; ?>
						  </td>
						  <td class="<?php echo $proved_tx; ?>"><?php echo $data->no_manifest; ?></td>
						  <td align="right" class="<?php echo $proved_tx; ?>">
							<?php echo $data->tonase_muat; ?>
						  </td>
						  <td align="right" class="<?php echo $proved_tx; ?>">
						  	<?php echo $data->tonase_bongkar; ?>
						  </td>
						  <td align="left" class="<?php echo $proved_tx; ?>">
						  	<?php
								if (strlen($data->asal) <= 12) {
									echo $data->asal;
								} else {
									echo "<a data-toggle='tooltip' data-placement='top' title='".$data->asal."'>".substr($data->asal, 0, 12)."...</a>";
								}
							?>
						  </td>
						  <td align="left" class="<?php echo $proved_tx; ?>">
						  	<?php
								if (strlen($data->tujuan) <= 12) {
									echo $data->tujuan;
								} else {
									echo "<a data-toggle='tooltip' data-placement='top' title='".$data->tujuan."'>".substr($data->tujuan, 0, 12)."...</a>";
								}
							?>
						  </td>
						  <td align="right" class="<?php echo $proved_tx; ?>">
						  	<?php echo number_format($data->uang_jalan); ?>
						  </td>
						  <td align="right" class="<?php echo $proved_tx; ?>"><?php echo number_format($data->ongkos_angkut); ?></td>
						  <td align="right" class="<?php echo $proved_tx; ?>"><?php echo number_format($data->total); ?></td>
						  <td align="center" class="<?php echo $proved_tx; ?>">
							<?php
                                if ($data->ongkos_angkut == 0 && $data->tonase_bongkar != 0) {
                            ?>
                            <?php if($hak_akses->role_update == 1): ?>
                                <a href="javascript:void(0)" onClick="konfirmasi(<?php echo $data->id; ?>,<?php echo $data->id_user; ?>)" data-toggle="tooltip" data-placement="top" title="Konfirmasi">
                                    [ <i class="fa fa-check"></i> ]
                                </a>
                            <?php endif; ?>
                            <?php if($hak_akses->role_update != 1 && $hak_akses->role_update != 1): ?>
                                <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="Akses terbatas!" onClick="alert('Akses Terbatas!')"> 
                                    [ <i class="fa fa-exclamation-circle"></i> ]
                                </a>
                            <?php endif; ?>
                            <?php
                                } else if($data->ongkos_angkut == 0 && $data->tonase_bongkar == 0) {
                            ?>
                                    <a href="javascript:void(0)" onClick="konfirmasi_awal(<?php echo $data->id; ?>, <?php echo $data->id_user; ?>, '<?php echo $data->no_spj; ?>')" data-toggle="tooltip" data-placement="top" title="Konfirmasi Awal">
                                        [ <i class="fa fa-clipboard-check fa-lg"></i> ]
                                    </a>
                            <?php
                                } else {
                            ?>
                            		<a href="javascript:void(0)" onClick="lihat_data(<?php echo $data->id; ?>)" data-toggle="tooltip" data-placement="top" title="Lihat Data">
                                        [ <i class="fa fa-search"></i> ]
                                    </a>
                                    <a href="javascript:void(0)" onClick="edit_data(<?php echo $data->id; ?>)" data-toggle="tooltip" data-placement="top" title="Edit Data">
                                        [ <i class="fa fa-edit fa-lg"></i> ]
                                    </a>
                            <?php
                                }
                            ?>
                          </td>
                          <td align="center" class="<?php echo $proved_tx; ?>">
                          	<?php
                                if ($data->ongkos_angkut == 0) {
                            ?>
                            <?php if($hak_akses->role_read == 1): ?>
                            <a href="javascript:void(0)" onClick="lihatPeta(<?php echo $data->id_user; ?>,<?php echo $data->lat; ?>,<?php echo $data->lng; ?>,<?php echo $data->id; ?>)" data-toggle="tooltip" data-placement="top" title="Lihat Lokasi di Peta">
                                [ <i class="fa fa-map-marked-alt"></i> ]
                            </a>
                            <?php endif; ?>
                            <?php if($hak_akses->role_read != 1 && $hak_akses->role_read != 1): ?>
                                [ <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="Akses terbatas!" onClick="alert('Akses Terbatas!')"> <i class="fa fa-exclamation-circle"></i> </a> ]
                            <?php endif; ?>
                            <?php
                                } else {
                            ?>
                            <a href="javascript:void(0)" onClick="lihatPeta(<?php echo $data->id_user; ?>,<?php echo $data->lat; ?>,<?php echo $data->lng; ?>,<?php echo $data->id; ?>)" data-toggle="tooltip" data-placement="top" title="Lihat Lokasi di Peta">
                                [ <i class="fa fa-map-marked-alt"></i> ]
                            </a>
                            <?php
                                }
                            ?>
                          </td>
                          <td align="center" class="<?php echo $proved_tx; ?>">
                            <?php if($hak_akses->role_delete == 1): ?>
                                <a href="javascript:void(0)" onClick="hapus(<?php echo $data->id; ?>,<?php echo $data->id_user; ?>, '', 'hapus')" data-toggle="tooltip" data-placement="top" title="Hapus">
                                    [ <i class="fa fa-trash"></i> ]
                                </a>
                            <?php endif; ?>
                            <?php if($hak_akses->role_delete != 1): ?>
                                <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="Akses terbatas!" onClick="alert('Akses Terbatas!')"> 
                                    [ <i class="fa fa-exclamation-circle"></i> ]
                                </a>
                            <?php endif; ?>
                          </td>
					    </tr>
						<?php
							}
						?>
					  </tbody>
					</table>
				</div>
				<div class="card-footer clearfix">
               		<div class="m-0 float-left">
                  		<span class="badge bg-success">Proses Selesai</span>
                  		<span class="badge bg-warning">Ongkos Bongkar Belum Update</span>
                        <span class="badge bg-danger">Tonase Bongkar Belum Update</span>
                	</div>
                	
                	<?php echo $links; ?>
                	
                	<div class="m-0 float-right" style="padding-top: 2px; padding-right: 10px;">
                		<?php echo $pagination_info; ?>
					</div>
              	</div>
			</div>
			
			<!--<div class="card card-default">
				<div class="card-header">
					<h3 class="card-title">
						Grafik
					</h3>
				</div>
				<div class="card-body p-0">
					<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
				</div>
			</div>-->
	  	</div>
	</section>
	
</div>

<div id="dialog-lihatPeta" title="Lihat Peta">
	
</div>

<div id="dialog-konfirmasi" title="Konfirmasi Laporan">
	
</div>

<div id="dialog-edit_data" title="Edit Konfirmasi Laporan">
	
</div>

<div id="dialog-lihat_data" title="Lihat Data">
	
</div>

<div id="dialog-lihat_grafik" title="Lihat Grafik">
	
</div>

<div id="dialog-hapus" title="Konfirmasi Hapus">
	
</div>

<div id="dialog-konfirmasi_awal" title="Konfirmasi Awal">
	
</div>


<script src="http://code.highcharts.com/stock/highstock.js"></script>
<script src="<?php echo base_url('assets/highchart/modules/exporting.js') ?>"></script>
<script src="<?php echo base_url('assets/highchart/modules/export-data.js') ?>"></script>

<script language="javascript">
	
	$(document).ready(function() {
		
		$("#data_tabel").tableHeadFixer({"right" : 3}); 
		$('#reset').click(function(){
			$('input[type=text]').val('');
			$('select[name=jenis_laporan]').val('');
		});
	});
	
	function konfirmasi_awal(id, id_user, no_spj) {
		$('#dialog-konfirmasi_awal').html('Memuat Halaman ...');
		$('#dialog-konfirmasi_awal').load("<?php echo base_url('laporan/bongkar_muat/lihat_data'); ?>?id="+id+"");
		$('#dialog-konfirmasi_awal').dialog({
			height: 460,
			width: 800,
			modal: true,
			open: function(event, ui) {
			  $(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
			},
			buttons: [{
				id    : "simpan",
				text  : "Setujui",
				click : function(){
					$.ajax({
						url   	 : '<?php echo base_url('laporan/bongkar_muat/approve_laporan'); ?>',
						type  	 : 'POST',
						dataType : 'json',
						data : { 
							"id" 	  : id,
							"id_user" : id_user,
							"no_spj"  : no_spj
						}
					}).done(function(data){
						if(data) {
						  if (data.status == true) {
							  suksesPesan('Laporan telah berhasil disetujui');
						  }
						}
					});
				}
			},{
				id    : "tolak",
				text  : "Tolak",
				click : function(){
					hapus(id, id_user, no_spj, 'reject');
				}
			},{
				id    : "close",
				text  : "Tutup",
				click : function(){
					$("#dialog-konfirmasi_awal").dialog('close');
					location.reload();
				}
			}]
		});
	}
	
	function hapus(id, id_user, no_spj, act) {
		
		if (act == 'hapus') {
			if (confirm('Apakah anda yakin akan menghapus data ini?')) {
				$.ajax({
					url   	 : '<?php echo base_url('laporan/bongkar_muat/deleteLaporan'); ?>',
					type  	 : 'POST',
					dataType : 'json',
					data : { 
						"id" 		 : id,
						"id_user" 	 : id_user,
						"keterangan" : $('#keterangan_hapus').val(),
						"no_spj"	 : no_spj != null ? no_spj : "",
						"act"		 : act
					}
				}).done(function(data){
					if(data) {
					  if (data.status == true) {
						  //$("#dialog-hapus").dialog('close');
						  if (act == 'hapus') {
								// Reload page hanya kalau aksi hapus!
								location.reload();
						  }
					  }
					}
				});
			}
		} else {
			
			$('#dialog-hapus').html('Memuat Halaman ...');
			$('#dialog-hapus').load("<?php echo base_url('laporan/bongkar_muat/konfirmasi_hapus'); ?>?id="+id+"");
			$('#dialog-hapus').dialog({
				height: 210,
				width: 500,
				modal: true,
				open: function(event, ui) {
				  $(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
				},
				buttons: [{
					id    : "simpan",
					text  : "Simpan",
					click : function(){
						
						if ($('#keterangan_hapus').val() == "") {
							alert('Keterangan tidak boleh kosong!');
						} else {
						
							$.ajax({
								url   	 : '<?php echo base_url('laporan/bongkar_muat/deleteLaporan'); ?>',
								type  	 : 'POST',
								dataType : 'json',
								data : { 
									"id" 		 : id,
									"id_user" 	 : id_user,
									"keterangan" : $('#keterangan_hapus').val(),
									"no_spj"	 : no_spj != null ? no_spj : "",
									"act"		 : act
								}
							}).done(function(data){
								if(data) {
								  if (data.status == true) {
									  suksesPesan('Laporan telah ditolak dan berhasil dihapus!');
									  $("#dialog-hapus").dialog('close');
									  if (act != 'reject') {
											// Reload page hanya kalau aksi hapus!
											location.reload();
									  }
								  }
								}
							});
						}
					}
				},{
					id    : "close",
					text  : "Tutup",
					click : function(){

						$("#dialog-hapus").dialog('close');
						if (act != 'reject') {
							// Reload page hanya kalau aksi hapus!
							location.reload();
						}
					}
				}]
			});
			
		}
	}
	
	function lihat_grafik(url) {

		$('#dialog-lihat_grafik').html('Memuat Halaman ...');
		$('#dialog-lihat_grafik').load("<?php echo base_url('laporan/bongkar_muat/lihat_grafik'); ?>?"+url+"");
		$('#dialog-lihat_grafik').dialog({
			height: 560,
			width: 800,
			modal: true,
			open: function(event, ui) {
			  $(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
			},
			buttons: [{
				id    : "close",
				text  : "Tutup",
				click : function(){
					location.reload();
				}
			}]
		});

	}

	function lihatPeta(id_user, Lat, Lng, id_bongkar) {

		$('#dialog-lihatPeta').html('Memuat Halaman ...');
		$('#dialog-lihatPeta').load("<?php echo base_url('laporan/bongkar_muat/lihat_peta'); ?>?lat="+Lat+"&lng="+Lng+"&id_user="+id_user+"&id_bongkar="+id_bongkar+"");
		$('#dialog-lihatPeta').dialog({
			height: 500,
			width: 800,
			modal: true,
			open: function(event, ui) {
			  $(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
			},
			buttons: [{
				id    : "close",
				text  : "Tutup",
				click : function(){
					location.reload();
				}
			}]
		});

	}
	
	function lihat_data(id) {

		$('#dialog-lihat_data').html('Memuat Halaman ...');
		$('#dialog-lihat_data').load("<?php echo base_url('laporan/bongkar_muat/lihat_data'); ?>?id="+id+"");
		$('#dialog-lihat_data').dialog({
			height: 460,
			width: 800,
			modal: true,
			open: function(event, ui) {
			  $(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
			},
			buttons: [{
				id    : "close",
				text  : "Tutup",
				click : function(){
					$("#dialog-lihat_data").dialog('close');
					location.reload();
				}
			}]
		});
	}
	
	function edit_data(id) {

		$('#dialog-edit_data').html('Memuat Halaman ...');
		$('#dialog-edit_data').load("<?php echo base_url('laporan/bongkar_muat/edit_data'); ?>?id="+id+"");
		$('#dialog-edit_data').dialog({
			height: 460,
			width: 800,
			modal: true,
			open: function(event, ui) {
			  $(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
			},
			buttons: [{
				id    : "simpan",
				text  : "Simpan",
				click : function(){
					$.ajax({
						url   : '<?php echo base_url('laporan/bongkar_muat/simpan_konfirmasi'); ?>',
						type  : 'POST',
						data  : { 
							"id" : id,
							"uang_jalan" : $('#uang_jalan_edit').val()
						}
					}).done(function(data){
						if(data) {
							$("#simpan").text("Ubah data . . .");
							$("#dialog-edit_data").dialog('close');
							location.reload();
						}
					});
				}
			},{
				id    : "close",
				text  : "Tutup",
				click : function(){
					$("#dialog-edit_data").dialog('close');
					location.reload();
				}
			}]
		});

	}

	function konfirmasi(id, id_user) {

		$('#dialog-konfirmasi').html('Memuat Halaman ...');
		$('#dialog-konfirmasi').load("<?php echo base_url('laporan/bongkar_muat/konfirmasi'); ?>?id="+id+"");
		$('#dialog-konfirmasi').dialog({
			height: 460,
			width: 800,
			modal: true,
			open: function(event, ui) {
			  $(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
			},
			buttons: [{
				id    : "simpan",
				text  : "Simpan",
				click : function(){
					$.ajax({
						url   : '<?php echo base_url('laporan/bongkar_muat/simpan_konfirmasi'); ?>',
						type  : 'POST',
						data  : { 
							"id" : id,
							"tonase_bongkar" : $('#tonase_bongkar').val(),
							"uang_jalan" 	 : $('#uang_jalan').val(),
							"ongkos_angkut"  : $('#ongkos_angkut').val()
						}
					}).done(function(data){
						if(data) {
							$("#simpan").text("Ubah data . . .");
							$("#dialog-konfirmasi").dialog('close');
							location.reload();
						}
					});
				}
			},{
				id    : "close",
				text  : "Tutup",
				click : function(){
					$("#dialog-konfirmasi").dialog('close');
					location.reload();
				}
			}]
		});

	}
	
</script>