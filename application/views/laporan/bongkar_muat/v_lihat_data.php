
<style>
	table tr td {
		padding: 5px;
		font-family:Tahoma, Verdana, Arial;
		font-size:12px;
	}
</style>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody>
    <tr>
      <td colspan="3" valign="top"><table width="100%" border="0">
        <tr>
          <td>Tanggal Kirim</td>
          <td>:</td>
          <td><?php echo date('d M Y - H:i', strtotime($onLap->created_at)); ?></td>
        </tr>
        <tr>
          <td width="35%">Pelapor</td>
          <td width="3%">:</td>
          <td width="62%"><?php echo $onLap->Name; ?></td>
        </tr>
        <tr>
          <td>Nomor Polisi</td>
          <td>:</td>
          <td><?php echo $onLap->plat_nomor; ?></td>
        </tr>
        <tr>
          <td>No. SPJ</td>
          <td>:</td>
          <td><?php echo $onLap->no_spj; ?></td>
        </tr>
        <tr>
          <td>Tonase Muat</td>
          <td>:</td>
          <td><?php echo $onLap->tonase_muat; ?></td>
        </tr>
        <tr>
          <td>Tonase Bongkar</td>
          <td>:</td>
          <td><?php echo $onLap->tonase_bongkar; ?></td>
        </tr>
        <tr>
          <td>Asal</td>
          <td>:</td>
          <td><?php echo $onLap->asal; ?></td>
        </tr>
        <tr>
          <td>Tujuan</td>
          <td>:</td>
          <td><?php echo $onLap->tujuan; ?></td>
        </tr>
        <tr>
          <td>Uang Jalan</td>
          <td>:</td>
          <td><?php echo number_format($onLap->uang_jalan); ?></td>
        </tr>
        <tr>
          <td>Ongkos Angkut</td>
          <td>:</td>
          <td><?php echo number_format($onLap->ongkos_angkut); ?></td>
        </tr>
        <tr>
          <td>Total</td>
          <td>:</td>
          <td>
          	<?php
			  $total = (( $onLap->tonase_bongkar * $onLap->ongkos_angkut ) - $onLap->uang_jalan);
			  if ($onLap->tonase_bongkar != 0) {
			  	  echo number_format($total);
			  } else {
				  echo 0;
			  }
			?>
          </td>
        </tr>
      </table></td>
      <td width="50%" align="center" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tbody>
          <tr>
            <td align="center">Foto Muatan</td>
            <td align="center">Foto Bongkar Muatan</td>
          </tr>
          <tr>
            <td align="center" valign="top"><img src="http://jasalia.com/trackmobile/assets/uploads/lapor/<?php echo $onLap->foto; ?>" width="200"></td>
            <td align="center" valign="top"><img src="http://jasalia.com/trackmobile/assets/uploads/lapor/<?php echo $onLap->foto_bongkar; ?>" width="200"></td>
          </tr>
        </tbody>
      </table></td>
    </tr>
  </tbody>
</table>
