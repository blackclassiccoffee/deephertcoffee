<html>
<head>
<style>
       #map {
        height: 300px;
        width: 100%;
       }
	   
	table tr td {
		padding: 10px;
		font-size: 12px;
		font-family: Tahoma, Verdana, Arial;
	}
	
	table tr th {
		padding: 10px;
		font-size: 12px;
		font-family: Tahoma, Verdana, Arial;
	}
    </style>
<title>Track Bus</title>
  </head>
<body>
<pre>
<?php //print_r($track); ?>
</pre>
<div id="map"></div>
<br>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
  	<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDOwOfwjtgZrAI-uF9Ug_cqaFhlC5mKaEQ&callback=initMap"></script>
<script>
	  
	function createMap() {
		return new google.maps.Map(document.getElementById('map'), {
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			center: new google.maps.LatLng(<?php echo $lat; ?>, <?php echo $lng; ?>),
			zoom: 18
		});
	}
	
	var locations = [];
	function getTrackLoc() {
	    $.ajax({
			url   : '<?php echo base_url('laporan/bongkar_muat/getTrackLocPath'); ?>',
			type  : 'POST',
			async : false,
			dataType: 'JSON',
			data  : { 
				"id_user" : <?php echo $id_user; ?>,
				"id_bongkar" : <?php echo $id_bongkar; ?>
			}
		}).done(function(data){
			if(data) {
				var newLoc = [];
				for (var x = 0; x < data.length; x++) {
					locations.push({
							lat : parseFloat(data[x].lat), 
							lng : parseFloat(data[x].lng)
						}
			  		);
				}
			    
			}
		});
	}
	
	$(function(){
		
		getTrackLoc();
		console.log(locations);
		
	});

	var marker = null;
	
	function initMap() {
		
		map = createMap();
		
		var tripPath = new google.maps.Polyline({
        	path: locations,
          	geodesic: true,
          	strokeColor: '#FF0000',
          	strokeOpacity: 1.0,
          	strokeWeight: 4
        });
		
		tripPath.setMap(map);
		
		var geocoder    = new google.maps.Geocoder;
		var infowindow  = new google.maps.InfoWindow;
		var LatLng 	    = new google.maps.LatLng(<?php echo $lat; ?>, <?php echo $lng; ?>);
		var konten_info = '';
		
		var marker = new google.maps.Marker({
			position: LatLng,
		  	title: 'new marker',
		  	draggable: false,
		  	icon: "http://jasalia.com/trackmobile/assets/marker_truck.png",
		  	map: map
	  	});
		
		geocoder.geocode({'location': LatLng}, function(results, status) {
          if (status === 'OK') {
            if (results[0]) {
              
			  konten_info = '<div style="width: 200px;">'+results[0].formatted_address+'</div>';
              infowindow.setContent(konten_info);
              infowindow.open(map, marker);
			  
			  marker.addListener('click', function() {
				 infowindow.open(map, marker);
			  });
				
            } else {
              window.alert('No results found');
            }
          } else {
            window.alert('Geocoder failed due to: ' + status);
          }
        });

		map.panTo( new google.maps.LatLng(<?php echo $lat; ?>, <?php echo $lng; ?>) );
	}
</script>
</body>
</html>