<style>
	table tr td {
		padding: 5px;
		font-family:Tahoma, Verdana, Arial;
		font-size:12px;
	}
</style>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody>
    <tr>
      <td width="48%" valign="top"><table width="100%" border="0">
        <tr>
          <td>Tanggal Kirim</td>
          <td>:</td>
          <td><?php echo date('d M Y - H:i', strtotime($onLap->created_at)); ?></td>
        </tr>
        <tr>
          <td width="36%">Pelapor</td>
          <td width="4%">:</td>
          <td width="60%"><?php echo $onLap->Name; ?></td>
        </tr>
        <tr>
          <td>Nomor Polisi</td>
          <td>:</td>
          <td><?php echo $onLap->plat_nomor; ?></td>
        </tr>
        <tr>
          <td>No. SPJ</td>
          <td>:</td>
          <td><?php echo $onLap->no_spj; ?></td>
        </tr>
        <tr>
          <td>Tonase Muat</td>
          <td>:</td>
          <td><?php echo $onLap->tonase_muat; ?></td>
        </tr>
        <tr>
          <td>Tonase Bongkar</td>
          <td>:</td>
          <td>
          	<?php echo $onLap->tonase_bongkar; ?>
          	<input type="hidden" name="tonase_bongkar" id="tonase_bongkar" value="<?php echo $onLap->tonase_bongkar; ?>" />
          </td>
        </tr>
        <tr>
          <td>Asal</td>
          <td>:</td>
          <td><?php echo $onLap->asal; ?></td>
        </tr>
        <tr>
          <td>Tujuan</td>
          <td>:</td>
          <td><?php echo $onLap->tujuan; ?></td>
        </tr>
        <tr>
          <td>Uang Jalan</td>
          <td>:</td>
          <td>
          	<?php echo number_format($onLap->uang_jalan); ?>
          	<input type="hidden" name="uang_jalan" id="uang_jalan" value="<?php echo $onLap->uang_jalan; ?>" />
          </td>
        </tr>
        <tr>
          <td>Ongkos Angkut</td>
          <td>:</td>
          <td><input type="text" name="ongkos_angkut" id="ongkos_angkut" class="inputmask-number" style="text-align: right;" /></td>
        </tr>
      </table></td>
      <td width="52%" align="center" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tbody>
          <tr>
            <td align="center">Foto Muatan</td>
            <td align="center">Foto Bongkar Muatan</td>
          </tr>
          <tr>
            <td align="center" valign="top">
            	<img src="http://jasalia.com/trackmobile/assets/uploads/lapor/<?php echo $onLap->foto; ?>" width="200">
            </td>
            <td align="center" valign="top">
            	<img src="http://jasalia.com/trackmobile/assets/uploads/lapor/<?php echo $onLap->foto_bongkar; ?>" width="200">
            </td>
          </tr>
        </tbody>
      </table>
      	
      </td>
    </tr>
  </tbody>
</table>


<script language="javascript">

	$(function(){
		$(".inputmask-number,.number").inputmask("decimal", {
			radixPoint: ",",
			autoGroup: true,
			groupSeparator: ".",
			groupSize: 3,
			autoUnmask: true
		});
	});
</script>