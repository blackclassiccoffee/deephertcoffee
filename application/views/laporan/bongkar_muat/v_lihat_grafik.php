

<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

<script type="text/javascript">
	
	var dataOmset = [];
	var armadaName = [];
	
	getOmset();
	function getOmset() {
	    $.ajax({
			url   : '<?php echo base_url('laporan/bongkar_muat/getOmset'); ?>',
			type  : 'POST',
			async : false,
			dataType: 'JSON'
		}).done(function(data){
			if(data) {
				for (var x = 0; x < data.length; x++) {
					dataOmset.push({
						y : parseInt(data[x].total),
						color : getRandomColor()
					});
					
					armadaName.push([data[x].Name]);
				}
			    console.log(data);
			}
		});
	}
	
	function getRandomColor() {
		var letters = '0123456789ABCDEF';
	  	var color = '#';
	  	for (var i = 0; i < 6; i++) {
			color += letters[Math.floor(Math.random() * 16)];
	  	}
	  	return color;
	}

	var dataSeries = [{
		name: 'Omset',
		data: dataOmset

	}]

	Highcharts.chart('container', {
		chart: {
			type: 'column'
		},
		title: {
			text: 'Armada Productivity'
		},
		subtitle: {
			text: 'TrakTruk'
		},
		credits: {
            enabled: true,
            position: {
                align: 'right',
                x: -10,
                verticalAlign: 'bottom',
                y: -5
            },
            href: "#",
            text: "TrakTruk IG"
        },
		xAxis: {
			categories: armadaName,
			min: 0,
            max: 3
		},
		scrollbar: {
			enabled: true
		},
		yAxis: {
			min: 0,
			title: {
				text: 'Omset'
			}
		},
		tooltip: {
			headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
			pointFormat: '<tr><td style="color:{series.color};padding:0">Omset : </td>' +
				'<td style="padding:0"><b>Rp. {point.y:,.0f}</b></td></tr>',
			footerFormat: '</table>',
			shared: true,
			useHTML: true
		},
		plotOptions: {
			column: {
				pointPadding: 0.2,
				borderWidth: 0
			}
		},
		series: dataSeries
	});

</script>