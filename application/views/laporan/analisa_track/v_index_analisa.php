<style type="text/css">

table#data_tabel tr td {
	padding: 5px;
	font-family:Tahoma, Verdana, Arial;
	font-size:11px;
	border-top: thin solid #F0F0F0; 
    border-bottom: thin solid #F0F0F0;
}
table#data_tabel tr th {
	font-family:Tahoma, Verdana, Arial;
	font-size:12px;
	padding: 5px;
}
#parent {
	height: 300px;
}

#data_tabel {
	width: 100% !important;
}
</style>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="55%">
    	<h2>
          <i class="fa fa-file"></i> Laporan Analisa Track
      </h2>
    </td>
    <td width="2%"></td>
    <td width="43%" align="right">
    </td>
  </tr>
</table>
<hr>
<div style="margin-top: 25px; margin-bottom: 25px;">
  <form name="form1" method="get" action="">
    <fieldset style="border: 1px solid black;">
      <legend style='padding: 10px; border: 1px solid black; width:190px; margin-bottom:0px; font-size:14px;'>
          <i class="fa fa-search"></i> 
          Filter / Pencarian
      </legend>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        
        <tr>
          <td></td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td width="16%"></td>
          <td width="2%"></td>
          <td width="82%"></td>
        </tr>
        <?php if ($KANTOR == 1 && $LEVEL == 1 ){ ?>
        <tr>
          <td height="35">Kantor</td>
          <td>:</td>
          <td>
              <select class="form-control select2" id="kantor" name="kantor" onChange="get_supplier(this.value)">
                <option value="all">Semua</option>
                <?php foreach ($toko as $tk): ?>
                  <option value="<?=$tk->id?>"  <?=$tk->id == $this->input->get('kantor') ? 'selected' : ''?>><?php echo $tk->nama?></option>
                <?php endforeach ?>
              </select>
          </td>
        </tr>
        <?php }else{ ?>
            <input type="hidden" id="kantor" name="kantor" value="<?php echo $KANTOR; ?>">
        <?php } ?>
        <tr>
          <td></td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td>
          	<input type="submit" name="cari" id="cari" value="Submit" />
          	<input type="reset" name="reset" id="reset" value="Clear" />
          </td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td></td>
          <td></td>
          <td></td>
        </tr>
      </table>
    </fieldset>
  </form>
</div>
<?php
$start = strtotime('17:50:00');
$end   = strtotime('18:04:00');
$diff  = $end - $start;

$hours = floor($diff / (60 * 60));
$minutes = $diff - $hours * (60 * 60);
//echo 'Remaining time: '. floor( $minutes / 60 ) . ' minutes';

?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="5%">
    	<h2>
          <i class="fa fa-file"></i> Data
        </h2>
    </td>
    <td width="49%">
    	<span id="loader_act"></span>
    </td>
    <td width="1%"></td>
    <td width="45%" align="right">
    	<?php if($hak_akses->role_create == 1): ?>
   	  	<a href="javascript:void(0)" id="tambah_barang">
        	<!--<i class="fa fa-plus"></i> Tambah Barang-->
        </a> | 
        <?php endif; ?>
        <?php if($hak_akses->role_download == 1): ?>
   	  	<a href="<?php echo site_url('master/barang/excel?'.$_SERVER['QUERY_STRING']);?>" title="">
<!--       		<i class="fa fa-download"></i> Export Excel-->
        </a>
        <?php endif; ?>
    </td>
  </tr>
</table>
<hr>
<div style="margin-top: 25px; margin-bottom: 25px;" id="parent">
<?php

$jumlahTrayek = count($onTrayek) / count($dataDes['dataDest']);
$bo = (floor(count($dataDes['dataTime']) / 2));
//echo $bo;
//print_r($dataDes['dataTime'][18]);
for ($f = 0; $f<ceil($jumlahTrayek); $f++) {
?>
  <table width="100%" border="0" id="data_tabel" style="border-collapse: collapse;" cellspacing="0" cellpadding="0">
    <thead>
      <tr>
      	<th width="332">Ket <?php echo $f; ?></th>
        <?php
		$i = 1;
		foreach($dataDes['dataDest'] as $dataPoint) {
		?>
        	<th width="480" height="26" bgcolor="#F0F0F0" align="center">
            	Point <?php echo $f % 2 == 0 ? $i : --$bo; ?>
            </th>
        <?php
			$i++;
		}
		?>
        <th width="248">Total</th>
      </tr>
    </thead>
    <tbody>
      <tr>
      	<td height="26">Jarak</td>
        <?php
		$a = 0;
		$totalDest = 0;
		$dataJarak = array();
		foreach($dataDes['dataDest'] as $dataDesta) {
			$hit = 0;
			if ($dataDesta > 1000) {
				$hit = number_format(($dataDesta/1000), 1)."km";
			} else {
				$hit = number_format($dataDesta, 1)."m";
			}
			echo "<td height='26' align='center'>".$hit."</td>";
			$totalDest += $dataDesta/1000;
			$dataJarak[] = $dataDesta/1000;
			$a++;
		}
		?>
        <td height="26" align="center"><?php echo number_format(floor($totalDest), 1)."Km"; ?></td>
      </tr>
      <tr>
      	<td height="26">Waktu Tempuh</td>
        <?php
		if ($f % 2 == 0) {
			$sisa_trayek = floor(count($dataDes['dataTime']) / 2)-1;
			$from = $f * (count($dataDes['dataDest'])+1);
			if ($f == 0) {
				$end = $sisa_trayek;
			} else {
				$end = count($dataDes['dataTime']);
			}
			for($s = $from; $s<=$end; $s++) {
				if (count($dataDes['dataTime']) > $s) {
					if ($dataDes['dataTime'][$s]['id_point'] > 0) {
						echo "<td height='26' align='center'>".$dataDes['dataTime'][$s]['timeArr']."</td>";
					}
				} else {
					echo "<td height='26' align='center'></td>";
				}
			}
		} else {
			$sisa_trayek = floor(count($dataDes['dataTime']) / 2);
			$onWew = count($dataDes['dataTime']) / (count($dataDes['dataDest']));
			if (round($onWew) > 0) {
				$onNumb = 0;
			} else {
				$onNumb = round($onWew);
			}
			for($s = $sisa_trayek-1; $s<count($dataDes['dataTime'])-3; $s++) {
				if ($s) {
					if ($dataDes['dataTime'][$s]['id_point'] > 0) {
						//echo $s;
						echo "<td height='26' align='center'>".$dataDes['dataTime'][$s]['timeArr']."</td>";
					}
				} else {
					echo "<td height='26' align='center'></td>";
				}
			}
		}
		?>
        <td height="26" align="center"></td>
      </tr>
      <tr>
      	<td height="26">Est. Waktu Tempuh (Menit)</td>
        <?php
		$onTimeData = array();
		$totalInMinute = 0;
		if ($f % 2 == 0) {
			for($z = 0; $z<count($dataDes['dataDest']); $z++) {
				if (count($dataDes['dataTime']) > $z) {
					if ($dataDes['dataTime'][$z]['id_point'] == 0) {
						$timeStart = $dataDes['dataTime'][$z]['timeArr'];
					}
					if ($z + 1 < count($dataDes['dataTime'])) {
						
						if ($z == 0) {
							$start = strtotime($timeStart);
							$end   = strtotime($dataDes['dataTime'][$z+1]['timeArr']);
							$diff  = $end - $start;
							
							$hours = floor($diff / (60 * 60));
							$minutes = $diff - $hours * (60 * 60);
							
							$onTimeData[] = floor( $minutes / 60 );
							$totalInMinute += floor( $minutes / 60 );
							echo "<td height='26' align='center'>".floor( $minutes / 60 )."</td>";
						} else {
							
							$start = strtotime($dataDes['dataTime'][$z]['timeArr']);
							$end   = strtotime($dataDes['dataTime'][$z+1]['timeArr']);
							$diff  = $end - $start;
							
							$hours = floor($diff / (60 * 60));
							$minutes = $diff - $hours * (60 * 60);
							
							$onTimeData[] = floor( $minutes / 60 );
							$totalInMinute += floor( $minutes / 60 );
							echo "<td height='26' align='center'>".floor( $minutes / 60 )."</td>";
							
						}
					}
				} else {
					
					echo "<td height='26' align='center'>0</td>";
					
				}
			}
		} else {
			$sisa_trayek = floor(count($dataDes['dataTime']) / 2);
			$onWew = count($dataDes['dataTime']) / (count($dataDes['dataDest']));
			if (round($onWew) > 0) {
				$onNumb = round($onWew);
			} else {
				$onNumb = round($onWew);
			}
			//echo $sisa_trayek;
			for($z = $sisa_trayek-2; $z<count($dataDes['dataTime'])-4; $z++) {
				
				if (count($dataDes['dataTime']) > $z) {
					if ($dataDes['dataTime'][$z]['id_point'] == 0) {
						$timeStart = $dataDes['dataTime'][$z]['timeArr'];
					}
					if ($z + 1 < count($dataDes['dataTime'])-4) {
						
						if ($z == 0) {
							$start = strtotime($timeStart);
							$end   = strtotime($dataDes['dataTime'][$z+1]['timeArr']);
							$diff  = $end - $start;
							
							$hours = floor($diff / (60 * 60));
							$minutes = $diff - $hours * (60 * 60);
							
							$onTimeData[] = floor( $minutes / 60 );
							$totalInMinute += floor( $minutes / 60 );
							echo "<td height='26' align='center'>".floor( $minutes / 60 )."</td>";
						} else {
							
							$start = strtotime($dataDes['dataTime'][$z]['timeArr']);
							$end   = strtotime($dataDes['dataTime'][$z+1]['timeArr']);
							$diff  = $end - $start;
							
							$hours = floor($diff / (60 * 60));
							$minutes = $diff - $hours * (60 * 60);
							
							$onTimeData[] = floor( $minutes / 60 );
							$totalInMinute += floor( $minutes / 60 );
							echo "<td height='26' align='center'>".floor( $minutes / 60 )."</td>";
							
						}
					}
				} else {
					
					echo "<td height='26' align='center'>0</td>";
					
				}
			}
		
		}
		?>
        <td height="26" align="center"><?php echo $totalInMinute; ?> Menit</td>
      </tr>
      <tr>
      	<td height="26">Est. Waktu Tempuh (Jam)</td>
        <?php
		$totalInHour = 0;
		if ($f % 2 == 0) {
			for($p = 0; $p<count($onTimeData); $p++) {
				if (count($onTimeData) > $p) {
					$totalInHour += ($onTimeData[$p]/60);
					echo "<td height='26' align='center'>".number_format(($onTimeData[$p]/60), 2)."</td>";
				} else {
					echo "<td height='26' align='center'></td>";
				}
			}
		} else {
			for($p = 0; $p<count($onTimeData); $p++) {
				if (count($onTimeData) > $p) {
					$totalInHour += ($onTimeData[$p]/60);
					echo "<td height='26' align='center'>".number_format(($onTimeData[$p]/60), 2)."</td>";
				} else {
					echo "<td height='26' align='center'></td>";
				}
			}
		}
		?>
        <td height="26" align="center"><?php echo number_format($totalInHour, 2) ?> Jam</td>
      </tr>
      <tr>
      	<td height="26">Kecepatan</td>
        <?php
		$totalKecepatan = 0;
		if ($f % 2 == 0) {
			for($q = 0; $q<count($onTimeData); $q++) {
				if (count($onTimeData) > $q) {
					$hitungKec = $dataJarak[$q]/($onTimeData[$q]/60);
					$totalKecepatan += $hitungKec;
					echo "<td height='26' align='center'>".number_format($hitungKec, 1)." km/Jam</td>";
				} else {
					echo "<td height='26' align='center'></td>";
				}
			}
		} else {
		
			for($q = 0; $q<count($onTimeData); $q++) {
				if (count($onTimeData) > $q) {
					$hitungKec = $dataJarak[$q]/($onTimeData[$q]/60);
					$totalKecepatan += $hitungKec;
					echo "<td height='26' align='center'>".number_format($hitungKec, 1)." km/Jam</td>";
				} else {
					echo "<td height='26' align='center'></td>";
				}
			}
		
		}
		?>
        <td height="26" align="center"><?php echo number_format(($totalKecepatan/count($onTimeData)), 1); ?> km/jam</td>
      </tr>
    </tbody>
    
  </table>
  <br />
<?php
}
?>
</div>

<div style="margin-bottom: 55px;">
	<div style="float: left;">
		<?php //echo $links; ?>
	</div>
    <div style="float: left; position: relative; top: 11px; left: 25px;">
		<?php //echo $pagination_info; ?>
	</div>
</div>

<div id="dialog-tambah_barang" title="Tambah Barang">
	
</div>

<div id="dialog-ubah_barang" title="Ubah Barang">
	
</div>
<?php

if(isset($_GET['cari'])) {
	echo "<script>$(function(){ get_supplier('".$_GET['kantor']."', '".$_GET['nama_supplier']."'); });</script>";
}

?>