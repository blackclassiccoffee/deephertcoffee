<style type="text/css">

	table#data_tabel tr td {
		padding: 5px;
		font-family:Tahoma, Verdana, Arial;
		font-size:11px;
	}

	table#data_tabel tr td.verifikasi {
		color: #FFFFFF;
	}
	
	table#data_tabel tr td.verifikasi a {
		color: #FFFFFF;
	}
	
	table#data_tabel tr td.pending {
		color: #000000;
	}
	
	table#data_tabel tr th {
		font-family:Tahoma, Verdana, Arial;
		font-size:12px;
		padding: 5px;
		vertical-align: middle;
	}
	#parent {
		height: 300px;
	}

	#data_tabel {
		width: 1200px !important;
	}
	
</style>

<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
		  		<div class="col-sm-6">
					<h1>Pesan Darurat</h1>
		  		</div>
		  		<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="#">Laporan</a></li>
						<li class="breadcrumb-item active">Pesan Darurat</li>
					</ol>
         		</div>
			</div>
			<hr>
	  	</div><!-- /.container-fluid -->
	</section>
	
	<section class="content">
    	<div class="container-fluid">
        <!-- SELECT2 EXAMPLE -->
			<div class="card card-default">
				<form name="form1" action="" method="get">
				<div class="card-header">
					<h3 class="card-title">
						Filter/Pencarian
					</h3>

					<div class="card-tools">
					  <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
					</div>
			  	</div>
			  	<div class="card-body">
            		<div class="row">
              			<div class="col-md-12">
              				<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
								  <td></td>
								  <td></td>
								  <td></td>
								</tr>
								<tr>
								  <td width="16%"></td>
								  <td width="2%"></td>
								  <td width="82%"></td>
								</tr>
								<tr>
								  <td height="35">Pelapor</td>
								  <td>:</td>
								  <td>
								  	<input class="form-control col-5 input-sm" type="text" name="pelapor" placeholder="Nama Pelapor" value="<?php echo isset($_GET['pelapor']) && $_GET['pelapor'] != "" ? $_GET['pelapor'] : ""; ?>">
								  </td>
							  	</tr>
								<tr>
								  <td height="45">Nomor Polisi</td>
								  <td>:</td>
								  <td>
								  	<input class="form-control col-5 input-sm" type="text" name="nopol" placeholder="H 0000 DA" value="<?php echo isset($_GET['nopol']) && $_GET['nopol'] != "" ? $_GET['nopol'] : ""; ?>">
								  </td>
							  	</tr>
								<tr>
								  <td height="35">Status Tindakan</td>
								  <td>:</td>
								  <td>
								  	<select class="form-control select2 col-3 input-sm" name="status_proved">
										<option value="">Semua</option>
										<option value="1" <?php echo isset($_GET['status_proved']) && $_GET['status_proved'] == "1" ? "selected" : ""; ?>>Terverifikasi</option>
										<option value="0" <?php echo isset($_GET['status_proved']) && $_GET['status_proved'] == "0" ? "selected" : ""; ?>>Pending</option>
									</select>
								  </td>
							  	</tr>
								<tr>
								  <td height="35">Tanggal</td>
								  <td>:</td>
								  <td>
								  	<div class="row" style="padding-left: 8px;">
								  		<input class="form-control col-2 datepicker" type="text" name="tanggal_awal" placeholder="Tanggal" value="<?php echo isset($_GET['tanggal_awal']) && $_GET['tanggal_awal'] != "" ? $_GET['tanggal_awal'] : ""; ?>">
								  		s/d
								  		<input class="form-control col-2 datepicker" type="text" name="tanggal_akhir" placeholder="Tanggal" value="<?php echo isset($_GET['tanggal_akhir']) && $_GET['tanggal_akhir'] != "" ? $_GET['tanggal_akhir'] : ""; ?>">
								  	</div>
								  	
								  </td>
							  	</tr>
								<tr>
								  <td></td>
								  <td></td>
								  <td></td>
								</tr>
								<tr>
								  <td>
									
								  </td>
								  <td></td>
								  <td></td>
								</tr>
								<tr>
								  <td></td>
								  <td></td>
								  <td></td>
								</tr>
							</table>
						</div>
					</div>
				</div>
				<div class="card-footer clearfix">
					<input type="submit" name="cari" id="cari" class="btn btn-primary" value="Submit" />
				</div>
				</form>
			</div>
			<!-- Filter/Pencarian -->
			
			<div class="card card-default">
				<div class="card-header">
					<h3 class="card-title">
						Data Laporan
					</h3>

					<div class="card-tools">
                    	<?php if($hak_akses->role_download == 1): ?>
                          <a href="<?php echo base_url('laporan/tombol_darurat/excel') ?>" class="btn">
                            <i class="fa fa-download"></i> Export Excel
                          </a>
                      	<?php endif; ?>
					</div>
			  	</div>
			  	
			  	<div class="card-body p-0">
					<table width="100%" cellspacing="0" id="data_tabel" class="table" cellpadding="0">
						<thead>
							<tr>
							  <th width="5%" height="45">No</th>
							  <th width="15%" align="left">Pelapor</th>
							  <th width="9%">Nomor Polisi</th>
							  <th width="13%" style="text-align: center;">Tanggal Kirim</th>
							  <th width="18%">Konfirmasi Admin</th>
							  <th width="11%">Admin</th>
							  <th width="11%" style="text-align: center;">Tanggal Konfirmasi</th>
							  <th width="6%" style="text-align: center;">#</th>
							  <th width="6%" style="text-align: center;">#</th>
                              <th width="6%" style="text-align: center;">#</th>
							</tr>
						</thead>
						<tbody>
							<?php
								$no = 0;
								foreach($onLaporan as $data) {  
									$no++;
									$proved    = $data->status_proved == 1 ? "#00a65a" : "#f39c12";
									$proved_tx = $data->status_proved == 1 ? "verifikasi" : "pending";
							?>
							<tr bgcolor="<?php echo $proved; ?>">
							  <td align="center" class="<?php echo $proved_tx; ?>">
							  	<?php echo $no; ?>
							  </td>
							  <td class="<?php echo $proved_tx; ?>">
							  	<?php echo $data->Name; ?>
							  </td>
							  <td align="left" class="<?php echo $proved_tx; ?>">
							  	<?php echo $data->plat_nomor; ?>
							  </td>
							  <td align="center" class="<?php echo $proved_tx; ?>"><?php echo date('d M Y - H:i', strtotime($data->created_at)); ?></td>
							  <td align="left" class="<?php echo $proved_tx; ?>">
							  	<?php 
									if ($data->status_proved == 1) {
										echo $data->keterangan_proved; 	
									} else {
										echo "Pending";
									}
								?>
							  </td>
							  <td align="left" class="<?php echo $proved_tx; ?>">
							  	<?php
									if ($data->status_proved == 1) {
										echo $data->NameProved;	
									} else {
										echo "Pending";
									}	
								?>
							  </td>
							  <td align="center" class="<?php echo $proved_tx; ?>">
							  	<?php
									if ($data->status_proved == 1) {
										echo date('d M Y - H:i', strtotime($data->date_proved));
									} else {
										echo "Pending";
									}	
								?>
							  </td>
							  <td align="center" class="<?php echo $proved_tx; ?>">
								<?php
                                    if ($data->status_proved != 1) {
                                ?>
                                <?php if($hak_akses->role_update == 1): ?>
                                    <a href="javascript:void(0)" onClick="konfirmasi(<?php echo $data->id; ?>, <?php echo $data->id_user; ?>)" title="Konfirmasi">
                                        [ <i class="fa fa-check"></i> ]
                                    </a>
                                <?php endif; ?>
                                <?php if($hak_akses->role_update != 1 && $hak_akses->role_update != 1): ?>
                                    <a href="javascript:void(0)" title="Akses terbatas!" onClick="alert('Akses Terbatas!')"> 
                                        [ <i class="fa fa-exclamation-circle"></i> ]
                                    </a>
                                <?php endif; ?>
                                <?php
                                    } else {
                                ?>
                                        <a href="javascript:void(0)" onClick="lihat_data(<?php echo $data->id; ?>)" title="Lihat Data">
                                            [ <i class="fa fa-check-circle fa-lg"></i> ]
                                        </a>
                                <?php
                                    }
                                ?>
                              </td>
							  <td align="center" class="<?php echo $proved_tx; ?>">
								<?php if($hak_akses->role_read == 1): ?>
                                <a href="javascript:void(0)" onClick="lihatPeta(<?php echo $data->lat; ?>,<?php echo $data->lng; ?>)" title="Lihat Lokasi di Peta">
                                    [ <i class="fa fa-map"></i> ]
                                </a>
                                <?php endif; ?>
                                <?php if($hak_akses->role_read != 1 && $hak_akses->role_read != 1): ?>
                                    [ <a href="javascript:void(0)" title="Akses terbatas!" onClick="alert('Akses Terbatas!')"> <i class="fa fa-exclamation-circle"></i> </a> ]
                                <?php endif; ?>
                              </td>
                              <td align="center" class="<?php echo $proved_tx; ?>">
								<?php if($hak_akses->role_delete == 1): ?>
                                    <a href="javascript:void(0)" onClick="hapus(<?php echo $data->id; ?>)" title="Hapus">
                                        [ <i class="fa fa-trash"></i> ]
                                    </a>
                                <?php endif; ?>
                                <?php if($hak_akses->role_delete != 1): ?>
                                    <a href="javascript:void(0)" title="Akses terbatas!" onClick="alert('Akses Terbatas!')"> 
                                        [ <i class="fa fa-exclamation-circle"></i> ]
                                    </a>
                                <?php endif; ?>
                              </td>
							</tr>
							<?php
								}
							?>
						</tbody>
					</table>
				</div>
				<div class="card-footer clearfix">
               		<div class="m-0 float-left">
                  		<span class="badge bg-success">Terverifikasi</span>
                  		<span class="badge bg-warning">Pending</span>
                	</div>
                	
                	<?php echo $links; ?>
                	
                	<div class="m-0 float-right" style="padding-top: 2px; padding-right: 10px;">
                		<?php echo $pagination_info; ?>
					</div>
              	</div>
			</div>
		</div>
	</section>
	
</div>

<div id="dialog-lihatPeta" title="Lihat Peta">
	
</div>

<div id="dialog-konfirmasi" title="Konfirmasi Laporan">
	
</div>

<div id="dialog-lihat_data" title="Lihat Laporan">
	
</div>

<?php

if(isset($_GET['cari'])) {
	//echo "<script>$(function(){ get_supplier('".$_GET['kantor']."', '".$_GET['nama_supplier']."'); });</script>";
}

?>


<script language="javascript">

	$(document).ready(function(){
		$("#data_tabel").tableHeadFixer({"right" : 3}); 
	});
	
	function hapus(id) {
		if (confirm('Apakah data akan dihapus?')) {
			$.ajax({
				url   	  : '<?php echo base_url('laporan/tombol_darurat/deleteLaporan'); ?>',
				type  	 : 'POST',
				dataType : 'json',
				data : { 
					"id" : id
				}
			}).done(function(data){
				if(data) {
				  if (data.status == true) {
				  	 location.reload();
				  }
				}
			});
		}
	}

	function lihatPeta(Lat, Lng) {
	
		$('#dialog-lihatPeta').html('Memuat Halaman ...');
		$('#dialog-lihatPeta').load("<?php echo base_url('laporan/tombol_darurat/lihat_peta'); ?>?lat="+Lat+"&lng="+Lng+"");
		$('#dialog-lihatPeta').dialog({
			height: 460,
			width: 600,
			modal: true,
			open: function(event, ui) {
			  $(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
			},
			buttons: [{
				id    : "close",
				text  : "Tutup",
				click : function(){
					location.reload();
				}
			}]
		});
	
	}
	
	function lihat_data(id) {

		$('#dialog-lihat_data').html('Memuat Halaman ...');
		$('#dialog-lihat_data').load("<?php echo base_url('laporan/tombol_darurat/lihat_data'); ?>?id="+id+"");
		$('#dialog-lihat_data').dialog({
			height: 340,
			width: 600,
			modal: true,
			open: function(event, ui) {
			  $(".ui-dialog-titlebar-close", ui.dialog | ui).show();
			},
			buttons: [{
				id    : "close",
				text  : "Tutup",
				click : function(){
					$("#dialog-lihat_data").dialog('close');
							location.reload();
				}
			}]
		});
	}
		
	function konfirmasi(id, id_user) {
	
		$('#dialog-konfirmasi').html('Memuat Halaman ...');
		$('#dialog-konfirmasi').load("<?php echo base_url('laporan/tombol_darurat/konfirmasi'); ?>?id="+id+"");
		$('#dialog-konfirmasi').dialog({
			height: 360,
			width: 600,
			modal: true,
			open: function(event, ui) {
			  $(".ui-dialog-titlebar-close", ui.dialog | ui).show();
			},
			buttons: [{
				id    : "close",
				text  : "Tutup",
				click : function(){
					$("#dialog-konfirmasi").dialog('close');
							location.reload();
				}
			},{
				id    : "simpan",
				text  : "Simpan",
				click : function(){
					$.ajax({
						url   : '<?php echo base_url('laporan/tombol_darurat/simpan_konfirmasi'); ?>',
						type  : 'POST',
						data  : { 
							"id" 		 : id,
							"id_user" 	 : id_user,
							"keterangan" : $('#konfirmasi_ket').val()
						}
					}).done(function(data){
						if(data) {
							$("#simpan").text("Ubah data . . .");
							$("#dialog-konfirmasi").dialog('close');
							location.reload();
						}
					});
				}
			}]
		});
	
	}
	
</script>