<html>
<head>
<style>
       #map {
        height: 100%;
        width: 100%;
       }
    </style>
<title>Track Bus</title>
  </head>
<body>
<div id="map"></div>
<script>
	  
	  function createMap() {
			return new google.maps.Map(document.getElementById('map'), {
				mapTypeId: google.maps.MapTypeId.ROADMAP,
				center: new google.maps.LatLng(-7.451355, 112.734604),
				zoom: 8,
				tilt: 30,
				mapTypeControl: true,
				  mapTypeControlOptions: {
					  style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
					  position: google.maps.ControlPosition.TOP_CENTER
				  },
				  zoomControl: true,
				  zoomControlOptions: {
					  position: google.maps.ControlPosition.LEFT_CENTER
				  },
				  scaleControl: true,
				  streetViewControl: true,
				  streetViewControlOptions: {
					  position: google.maps.ControlPosition.LEFT_TOP
				  },
				  fullscreenControl: true
			});
	  }
	  
	  var marker = null;
      function initMap() {
			map = createMap();
		    var LatLng = new google.maps.LatLng(<?php echo $lat; ?>, <?php echo $lng; ?>)
			var marker = new google.maps.Marker({
				position: LatLng,
				title: 'new marker',
				draggable: false,
				map: map
			});
		  
		  	map.panTo( new google.maps.LatLng(<?php echo $lat; ?>, <?php echo $lng; ?>) );
      }
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
  <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDOwOfwjtgZrAI-uF9Ug_cqaFhlC5mKaEQ&callback=initMap">
    </script>
</body>
</html>