<style>
   #map {
	   height: 100%;
	   width: 100%;
   }
	#search_marker {
		position: absolute; 
		top: 10px; 
		left: 260px; 
		z-index: 99;
	}
	
	#search_name {
		width: 250px;
		height: 130px;
	}
</style>

<div class="content-wrapper" style="height: 100px;">
	<div class="row" style="height: 100%;">
		<div class="col-lg-12">
			<div id="search_marker">
				<select class="form-control select2" id="search_name" onChange="loadMarkers()">
					<option value="">Semua Armada</option>
					<?php
						foreach($Users as $data) {
							echo "<option value='".$data->id."'>".$data->Name."</option>";
						}
					?>
				</select>
			</div>
			<div id="map"></div>
		</div>
	</div>
</div>

<div id="dialog-track_record" title="Detail Track Record"></div>

<script>
	
	function lihat_track(id_user, lat, lng) {

		$('#dialog-track_record').html('Memuat Halaman ...');
		$('#dialog-track_record').load("<?php echo base_url('app/locator_armada/lihat_track'); ?>?id_user="+id_user+"&lat="+lat+"&lng="+lng+"");
		$('#dialog-track_record').dialog({
			height: 650,
			width: 1020,
			modal: true,
			open: function(event, ui) {
			  	$(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
				$(".ui-dialog", ui.dialog | ui).css('z-index', 9999);
				$(event.target).parent().css('position', 'fixed');
				$(event.target).parent().css('top', '50%');
				$(event.target).parent().css('left', '50%');
				$(event.target).parent().css('transform', 'translate(-50%, -50%)');
			},
			buttons: [{
				id    : "close",
				text  : "Tutup",
				click : function(){
					location.reload();
				}
			}]
		});

	}
	
  	function createMap() {
		return new google.maps.Map(document.getElementById('map'), {
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			center: new google.maps.LatLng(-7.451355, 112.734604),
			zoom: 3
		});
  	}

  	var marker = null;
  	var markers = [];
	
	function loadMarkers() {
		
		
		for (i = 0;  i <  markers.length;  i++) {
			markers[i].setMap(null);
		}
		markers = [];

		var datax = [];
		$.ajax({
			'url'      : '<?php echo base_url('app/locator_armada/getArmadaPos'); ?>',
			'dataType' : 'json',
			'type'	   : 'POST',
			'async'	   : false,
			'data'	 : {
				'category' : 'x',
				'id_user'  : $('#search_name').val() != "" ? $('#search_name').val() : ""
			},
			'success' : function(data){

				for(var i = 0; i<data.length; i++) {

					datax.push({
						position	: new google.maps.LatLng(data[i].lat, data[i].lng),
						type		: 'car',
						data_armada : data[i],
						icon		: 'http://jasalia.com/trackmobile/assets/marker_truck.png',
						panToName	: $('#search_name').val() != "" ? "1" : "0"
					});
				}

			}
		});

		// Create markers.
		datax.forEach(function(feature) {

			var geocoder   = new google.maps.Geocoder;
		  	var infowindow = new google.maps.InfoWindow();
			
		  	//console.log(feature);
		  	

		  	marker = new google.maps.Marker({
		  		map: map,
				position: feature.position,
				draggable: false,
				icon: feature.icon,
				animation: feature.anim
		  	});
			
			if (feature.panToName == 1) {
				map.panTo( feature.position );
				map.setZoom(17);
			} else {
				map.setZoom(8);
			}
			
		  	markers.push(marker);
			

		  	google.maps.event.addListener(marker,'click', (function(marker,infowindow){ 
		  		return function() {
					geocoder.geocode({'location': feature.position}, function(results, status) {
						if (status === 'OK') {
							if (results[0]) {
								
								var content = '\
									<table width="337" border="0">\
									  <tbody>\
										<tr>\
										  <td width="32%">Nama</td>\
										  <td width="2%">:</td>\
										  <td width="66%">'+feature.data_armada.Name+'</td>\
										</tr>\
										<tr>\
										  <td>Nomor Polisi</td>\
										  <td>:</td>\
										  <td>'+feature.data_armada.plat_nomor+'</td>\
										</tr>\
										<tr>\
										  <td>Posisi</td>\
										  <td>:</td>\
										  <td>'+results[0].formatted_address+'</td>\
										</tr>\
                                        <tr>\
										  <td>Terakhir Update</td>\
										  <td>:</td>\
										  <td>'+feature.data_armada.updated_at+'</td>\
										</tr>\
										<tr>\
										  <td>&nbsp;</td>\
										  <td>&nbsp;</td>\
										  <td>&nbsp;</td>\
										</tr>\
										<tr>\
										  <td colspan="3">\
											<a href="javascript:void(0)" onClick="lihat_track('+feature.data_armada.id_user+', '+feature.data_armada.lat+', '+feature.data_armada.lng+')">\
												[ Track Record ]\
											</a>\
										  </td>\
										</tr>\
									  </tbody>\
									</table>\
								';
								
						  		infowindow.setContent(content);
						  		infowindow.open(map, marker);

								marker.addListener('click', function() {
									infowindow.open(map, marker);
								});

							} else {
						  		window.alert('No results found');
							}
					  	} else {
							window.alert('Geocoder failed due to: ' + status);
					  	}
					});
		  		};
		  	})(marker,infowindow)); 

		});

		setTimeout(loadMarkers, 20000);
	}
	
  	function initMap() {
		map = createMap();
		loadMarkers();
  	}
</script>
<script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js">
    </script>
<script async defer
src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDOwOfwjtgZrAI-uF9Ug_cqaFhlC5mKaEQ&callback=initMap">
</script>