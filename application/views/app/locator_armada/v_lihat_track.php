<html>
<head>
<style>
       #mapTrack {
        height: 300px;
        width: 100%;
       }
	   
	table tr td {
		padding: 10px;
		font-size: 12px;
		font-family: Tahoma, Verdana, Arial;
	}
	
	table tr th {
		padding: 10px;
		font-size: 12px;
		font-family: Tahoma, Verdana, Arial;
	}
    </style>
<title>Track Bus</title>
  </head>
<body>

<table width="100%" border="0">
	<tbody>
		<tr>
			<td width="14%">Tanggal</td>
			<td width="3%">:</td>
			<td width="50%">
				<input type="text" id="dateTrack1" class="datepicker" value="<?php echo date('Y-m-d', strtotime('-1 days')) ?>"> s/d <input type="text" id="dateTrack2" class="datepicker" value="<?php echo date('Y-m-d') ?>">
				<input type="button" id="cariTrack" value="Cari">
			</td>
			<td width="33%">
				<span id="loading">Harap Tunggu...</span>
				<!--<input type="button" id="cariTrack" value="Cari">-->
			</td>
		</tr>
	</tbody>
</table>
<div id="mapTrack"></div>
<hr>
	<table width="100%" border="1">
	  	<thead>
	    	<tr>
	      		<th width="6%" style="text-align: center;">No</th>
			  	<th width="30%">Asal</th>
			  	<th width="26%">Tujuan</th>
			  	<th width="14%" style="text-align: center;">Waktu Tempuh</th>
			  	<th width="12%" style="text-align: right;">Jarak (km)</th>
			  	<th width="12%" style="text-align: right;">Kecepatan (km/h)</th>
        	</tr>
		</thead>
		<tbody id="trackData">
			
  		</tbody>
		<tfoot>
			<tr>
				<td colspan="4" align="right">Total Jarak (Km) : </td>
				<td align="right">
					<span id="totalJarak"></span>
				</td>
				<td align="right">
					<span id="totalKecepatan"></span>
				</td>
			</tr>
		</tfoot>
	</table>
	
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCIi-B_JgCVjCn_YEU0ImFA__e6-PBElsM&callback=initMapTrack"></script>
<script src="<?php echo base_url();?>assets/js/moment.min.js"></script>
<script>
	  
	function createMapTrack() {
		return new google.maps.Map(document.getElementById('mapTrack'), {
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			center: new google.maps.LatLng(<?php echo $lat; ?>, <?php echo $lng; ?>),
			zoom: 13
		});
	}
	
	function getAddress(LatLng) {
		var address = '';
		$.ajax({
			url   : 'https://maps.googleapis.com/maps/api/geocode/json?address='+LatLng+'&key=AIzaSyCIi-B_JgCVjCn_YEU0ImFA__e6-PBElsM',
			type  : 'POST',
			async : false,
			dataType: 'JSON'
		}).done(function(data){
			if (data.status == 'OK') {
				/*var jalan = data.results[0].address_components[0].short_name;
				var kelurahan = data.results[0].address_components[2].short_name;
				var kecamatan = data.results[0].address_components[3].short_name;
				var kabupaten = data.results[0].address_components[4].short_name;
				var prov = data.results[0].address_components[5].short_name;*/
				address = data.results[0].formatted_address;
			}
		});
		
		return address;
	}
	
	function getTrackDestince(dateTrack1, dateTrack2){
		$.ajax({
			url   : '<?php echo base_url('app/locator_armada/getTrack'); ?>',
			type  : 'POST',
			dataType: 'JSON',
			data  : { 
				"id_user"   : <?php echo $id_user; ?>,
				"tanggal_awal"  : dateTrack1,
				"tanggal_akhir" : dateTrack2
			},
			beforeSend: function(){
			 	$("#loading").show();
		   	},
		   	complete: function(){
			 	$("#loading").hide();
		   	},
		}).done(function(data){
			if(data) {
				
				var rad = function(x) {
					return x * Math.PI / 180;
				};
				
				var tD = '';
				var totalJarak = 0;
				var totalKecepatan = 0;
				var n = 0;
				for (var x = 0; x < data.length; x++) {
					
					if (data[x].in_hour != 0) {
						var R = 6378137; // Earth’s mean radius in meter
						var dLat = rad(parseFloat(data[x].dest_lat) - parseFloat(data[x].origin_lat));
						var dLong = rad(parseFloat(data[x].dest_lng) - parseFloat(data[x].origin_lng));

						var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
								Math.cos(rad(parseFloat(data[x].origin_lat))) * Math.cos(rad(parseFloat(data[x].dest_lat))) *
								Math.sin(dLong / 2) * Math.sin(dLong / 2);

						var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
						var d = R * c; // returns the distance in meter 


						var distanceKM = (Math.round(d) / 1000);


						var LatLngOri = parseFloat(data[x].origin_lat)+","+parseFloat(data[x].origin_lng);
						var LatLngDes = parseFloat(data[x].dest_lat)+","+parseFloat(data[x].dest_lng);

						var dateOri = moment(data[x].origin_date, 'YYYY-MM-DD kk:mm:ss');
						var dateDes = moment(data[x].dest_date);

						var speed = distanceKM / data[x].in_hour;

						tD += "<tr>\
									<td style='text-align: center;'>"+(x + 1)+"</td>\
									<td>\
										<div style='margin-bottom: 5px;'>\
											<i class='fa fa-clock'></i> "+dateOri.format('DD MMM YYYY - HH:mm')+"\
										</div>\
										"+getAddress(LatLngOri)+"\
									</td>\
									<td>\
										<div style='margin-bottom: 5px;'>\
											<i class='fa fa-clock'></i> "+dateDes.format('DD MMM YYYY - HH:mm')+"\
										</div>\
										"+getAddress(LatLngDes)+"\
									</td>\
									<td style='text-align: right;'>\
										"+data[x].run_time+"\
									</td>\
									<td style='text-align: right;'>\
										"+Number(distanceKM).toFixed(2)+"\
									</td>\
									<td style='text-align: right;'>\
										"+Number(speed).toFixed(2)+"\
									</td>\
								</tr>";
						n++;
						totalJarak += distanceKM;
						totalKecepatan += speed;
					}
					
				}
				
				$('#trackData').html(tD);
				$('#totalJarak').text(Number(totalJarak).toFixed(2));
				
				var speed_avg = totalKecepatan / n;
				$('#totalKecepatan').text(Number(speed_avg).toFixed(2));
			}
		});
	}
	
	
	var locations = [];
	function getTrackLoc(dateTrack1, dateTrack2) {
		locations = [];
	    $.ajax({
			url   : '<?php echo base_url('app/locator_armada/getTrackLocPath'); ?>',
			type  : 'POST',
			async : false,
			dataType: 'JSON',
			data  : { 
				"id_user"   : <?php echo $id_user; ?>,
				"dateTrack1" : dateTrack1,
				"dateTrack2" : dateTrack2
			}
		}).done(function(data){
			if(data) {
				for (var x = 0; x < data.length; x++) {
					locations.push({
							'lat' : parseFloat(data[x].lat), 
							'lng' : parseFloat(data[x].lng)
						}
			  		);
				}
			    
			}
		});
	}
	
	$(function(){
		
		getTrackLoc($('#dateTrack1').val(), $('#dateTrack2').val());
		getTrackDestince($('#dateTrack1').val(), $('#dateTrack2').val());
		
		$('#cariTrack').on('click', function(){
			getTrackLoc($('#dateTrack1').val(), $('#dateTrack2').val());
			getTrackDestince($('#dateTrack1').val(), $('#dateTrack2').val());
			initMapTrack();
		});
		
		$('.datepicker').datetimepicker({
			format:'Y-m-d',
			timepicker:false,
			timepickerScrollbar:true
		});
		
	});

	var marker = null;
	
	function initMapTrack() {
		
		mapT = createMapTrack();
		var geocoder    = new google.maps.Geocoder;
		var infowindow  = new google.maps.InfoWindow;
		var LatLng 	    = new google.maps.LatLng(<?php echo $lat; ?>, <?php echo $lng; ?>);
		var konten_info = '';

		var marker = new google.maps.Marker({
			position: LatLng,
			title: 'new marker',
			draggable: false,
			icon: "http://jasalia.com/trackmobile/assets/marker_truck.png",
			map: mapT
		});

		geocoder.geocode({'location': LatLng}, function(results, status) {
		  if (status === 'OK') {
			if (results[0]) {

			  konten_info = '<div style="width: 200px;">'+results[0].formatted_address+'</div>';
			  infowindow.setContent(konten_info);
			  infowindow.open(mapT, marker);

			  marker.addListener('click', function() {
				 infowindow.open(mapT, marker);
			  });

			} else {
			  window.alert('No results found');
			}
		  } else {
			window.alert('Geocoder failed due to: ' + status);
		  }
		});

		mapT.panTo( new google.maps.LatLng(<?php echo $lat; ?>, <?php echo $lng; ?>) );
		
		if (locations.length > 0) {
			var service = new google.maps.DirectionsService;

			/*var tripPath = new google.maps.Polyline({
				path: locations,
				geodesic: true,
				strokeColor: '#FF0000',
				strokeOpacity: 1.0,
				strokeWeight: 4
			});

			tripPath.setMap(mapT);*/

			// Zoom and center map automatically by stations (each station will be in visible map area)
			var lngs = locations.map(function(station) { return station.lng; });
			var lats = locations.map(function(station) { return station.lat; });
			mapT.fitBounds({
				west: Math.min.apply(null, lngs),
				east: Math.max.apply(null, lngs),
				north: Math.min.apply(null, lats),
				south: Math.max.apply(null, lats),
			});

			// Divide route to several parts because max stations limit is 25 (23 waypoints + 1 origin + 1 destination)
			for (var i = 0, parts = [], max = 25 - 1; i < locations.length; i = i + max)
				parts.push(locations.slice(i, i + max + 1));

			// Service callback to process service results
			var service_callback = function(response, status) {
				if (status != 'OK') {
					console.log('Directions request failed due to ' + status);
					return;
				}
				var renderer = new google.maps.DirectionsRenderer;
				renderer.setMap(mapT);
				renderer.setOptions({ suppressMarkers: true, preserveViewport: true });
				renderer.setDirections(response);
			};

			// Send requests to service to get route (for stations count <= 25 only one request will be sent)
			for (var i = 0; i < parts.length; i++) {
				// Waypoints does not include first station (origin) and last station (destination)
				var waypoints = [];
				for (var j = 1; j < parts[i].length - 1; j++)
					waypoints.push({location: parts[i][j], stopover: false});
					// Service options
					var service_options = {
						origin: parts[i][0],
						destination: parts[i][parts[i].length - 1],
						waypoints: waypoints,
						travelMode: 'WALKING'
				};
				// Send request
				service.route(service_options, service_callback);
			}
		}
	}
</script>
</body>
</html>