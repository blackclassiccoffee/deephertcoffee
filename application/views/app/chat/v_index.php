<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1>Chat</h1>
				</div>
			</div>
		</div><!-- /.container-fluid -->
	</section>
	
	<section class="content">
      	<div class="container-fluid">
      		<div class="row">
				<div class="col-md-4">
					<div class="card">
						<div class="card-header">
							<h5 class="card-title">Kontak</h5>
							<div class="card-tools">
								x
							</div>
						</div>
							
						<div class="card-body p-0">
							<ul class="nav nav-pills flex-column" style="height: 500px;">
								<li class="nav-item active">
									<a href="#" class="nav-link">
										<div class="media">
											<img src="<?php echo base_url();?>assets/dist/img/user1-128x128.jpg" alt="User Avatar" class="img-size-50 mr-3 img-circle">
											<div class="media-body">
												<h6 class="dropdown-item-title  text-muted">
													Brad Diesel
												</h6>
												<p class="text-sm text-muted" style="margin: 0;">
													Call me whenever you can...
													<span class="badge bg-primary float-right">12</span>
												</p>
												<p class="text-sm text-muted" style="margin-bottom: 0;">
													<i class="fa fa-clock mr-1"></i> 4 Hours Ago
												</p>
											</div>
										</div>
									</a>
								</li>
								<li class="nav-item active">
									<a href="#" class="nav-link">
										<div class="media">
											<img src="<?php echo base_url();?>assets/dist/img/user1-128x128.jpg" alt="User Avatar" class="img-size-50 mr-3 img-circle">
											<div class="media-body">
												<h6 class="dropdown-item-title  text-muted">
													Brad Diesel
												</h6>
												<p class="text-sm text-muted" style="margin: 0;">
													Call me whenever you can...
													<span class="badge bg-primary float-right">12</span>
												</p>
												<p class="text-sm text-muted" style="margin-bottom: 0;">
													<i class="fa fa-clock mr-1"></i> 4 Hours Ago
												</p>
											</div>
										</div>
									</a>
								</li>
							</ul>
            			</div>
					</div>
				</div>
				
				<div class="col-md-8">
					<!-- DIRECT CHAT WARNING -->
					<div class="card card-warning direct-chat direct-chat-warning">
						<div class="card-header">
							<h3 class="card-title">
								<div style="display: flex;">
									<div>
										<img src="<?php echo base_url();?>assets/dist/img/user1-128x128.jpg" alt="User Avatar" class="img-size-32 mr-3 img-circle">
									</div>
									<div style="display: grid;">
										<small><b>Alexander Pierce</b></small>
										<span style="font-size: 11px;">Online</span>
									</div>
								</div>
							</h3>

							<div class="card-tools">
								<button type="button" class="btn btn-tool" data-widget="collapse">
									<i class="fa fa-minus"></i>
								</button>
							</div>
						</div>
						<!-- /.card-header -->
						<div class="card-body">
							<!-- Conversations are loaded here -->
							<div class="direct-chat-messages" style="height: 430px;">
								<!-- Message. Default to the left -->
								<div class="direct-chat-msg col-md-6">
									<div class="direct-chat-info clearfix">
										<span class="direct-chat-name float-left">Alexander Pierce</span>
										<span class="direct-chat-timestamp float-right">23 Jan 2:00 pm</span>
									</div>
									<!-- /.direct-chat-info -->
									<img class="direct-chat-img" src="<?php echo base_url(); ?>assets/dist/img/user1-128x128.jpg" alt="Message User Image">
									<!-- /.direct-chat-img -->
									<div class="direct-chat-text">
										Is this template really for free? That's unbelievable!
									</div>
								<!-- /.direct-chat-text -->
								</div>
								<!-- /.direct-chat-msg -->

								<!-- Message to the right -->
								<div class="direct-chat-msg right" style="max-width: 50%; float: right; margin-right: 10px;">
									<div class="direct-chat-info clearfix">
										<span class="direct-chat-timestamp float-left">23 Jan 2:05 pm</span>
									</div>
									<!-- /.direct-chat-info -->
									<!-- /.direct-chat-img -->
									<div class="direct-chat-text" style="margin-right: 0px;">
										You better believe it!
									</div>
									<!-- /.direct-chat-text -->
								</div>
								<!-- /.direct-chat-msg -->
							</div>
							<!--/.direct-chat-messages-->

							<!-- Contacts are loaded here -->
							<div class="direct-chat-contacts">
								<ul class="contacts-list">
									<li>
										<a href="#">
											<img class="contacts-list-img" src="../dist/img/user1-128x128.jpg">

											<div class="contacts-list-info">
												<span class="contacts-list-name">
													Count Dracula
													<small class="contacts-list-date float-right">2/28/2015</small>
												</span>
												<span class="contacts-list-msg">
													How have you been? I was...
												</span>
											</div>
										<!-- /.contacts-list-info -->
										</a>
									</li>
									<!-- End Contact Item -->
								</ul>
								<!-- /.contatcts-list -->
							</div>
						<!-- /.direct-chat-pane -->
						</div>
						<!-- /.card-body -->
						<div class="card-footer">
							<form action="#" method="post">
								<div class="input-group">
									<input type="text" name="message" placeholder="Type Message ..." class="form-control">
									<span class="input-group-append">
										<button type="submit" class="btn btn-warning">Send</button>
									</span>
								</div>
							</form>
						</div>
						<!-- /.card-footer-->
					</div>
					<!--/.direct-chat -->
				</div>
			</div>
		</div>
	</section>
	
</div>