<style type="text/css">

	table#data_tabel tr td {
		padding: 5px;
		font-family:Tahoma, Verdana, Arial;
		font-size:11px;
	}

	table#data_tabel tr td.verifikasi {
		color: #FFFFFF;
	}
	
	table#data_tabel tr td.verifikasi a {
		color: #FFFFFF;
	}
	
	table#data_tabel tr td.pending {
		color: #000000;
	}
	
	table#data_tabel tr th {
		font-family:Tahoma, Verdana, Arial;
		font-size:12px;
		padding: 5px;
		vertical-align: middle;
	}
	#parent {
		height: 300px;
	}

	#data_tabel {
		width: 100% !important;
	}
	
</style>

<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
		  		<div class="col-sm-6">
					<h1>Info</h1>
		  		</div>
		  		<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="#">Dashboard</a></li>
						<li class="breadcrumb-item active">Info</li>
					</ol>
         		</div>
			</div>
			<hr>
	  	</div><!-- /.container-fluid -->
	</section>
	
	<section class="content">
    	<div class="container-fluid">
        <!-- SELECT2 EXAMPLE -->
			<div class="card card-default">
				<form name="form1" action="" method="get">
				<div class="card-header">
					<h3 class="card-title">
						Filter/Pencarian
					</h3>

					<div class="card-tools">
					  <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
					</div>
			  	</div>
			  	<div class="card-body">
            		<div class="row">
              			<div class="col-md-12">
              				<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
								  <td></td>
								  <td></td>
								  <td></td>
								</tr>
								<tr>
								  <td width="16%"></td>
								  <td width="2%"></td>
								  <td width="82%"></td>
								</tr>
								<tr>
								  <td height="43">Judul Info</td>
								  <td>:</td>
								  <td>
								  	<input class="form-control col-5 input-sm" type="text" name="judul" placeholder="Judul Info" value="<?php echo isset($_GET['judul']) && $_GET['judul'] != "" ? $_GET['judul'] : ""; ?>">
								  </td>
							  	</tr>
								<?php if ($KANTOR == 1 && $LEVEL == 1 ){ ?>
								<tr>
								  <td height="35">Tanggal</td>
								  <td>:</td>
								  <td>
								  	<div class="row" style="padding-left: 8px;">
								  		<input class="form-control col-2 datepicker" type="text" name="tanggal_awal" placeholder="Tanggal" value="<?php echo isset($_GET['tanggal_awal']) && $_GET['tanggal_awal'] != "" ? $_GET['tanggal_awal'] : ""; ?>">
								  		s/d
								  		<input class="form-control col-2 datepicker" type="text" name="tanggal_akhir" placeholder="Tanggal" value="<?php echo isset($_GET['tanggal_akhir']) && $_GET['tanggal_akhir'] != "" ? $_GET['tanggal_akhir'] : ""; ?>">
								  	</div>
								  	
								  </td>
							  	</tr>
								<?php }else{ ?>
									<input type="hidden" id="kantor" name="kantor" value="<?php echo $KANTOR; ?>">
								<?php } ?>
								<tr>
								  <td></td>
								  <td></td>
								  <td></td>
								</tr>
								<tr>
								  <td>
									
								  </td>
								  <td></td>
								  <td></td>
								</tr>
								<tr>
								  <td></td>
								  <td></td>
								  <td></td>
								</tr>
							</table>
						</div>
					</div>
				</div>
				<div class="card-footer clearfix">
					<input type="submit" name="cari" id="cari" class="btn btn-primary" value="Submit" />
				</div>
				</form>
			</div>
			<!-- Filter/Pencarian -->
			
			<div class="card card-default">
				<div class="card-header">
					<h3 class="card-title">
						Data Laporan
					</h3>

					<div class="card-tools">
				  	  <?php if($hak_akses->role_create == 1): ?>
						  <a href="javascript:void(0)" onClick="tambah_data()" class="btn">
							<i class="fa fa-plus-circle"></i> Tambah Info
						  </a>
						  |
                      <?php endif; ?>
					  <?php if($hak_akses->role_download == 1): ?>
                          <a href="<?php echo base_url('app/info/excel')."?".$_SERVER['QUERY_STRING']; ?>" class="btn">
                            <i class="fa fa-download"></i> Export Excel
                          </a>
                      	<?php endif; ?>
					</div>
			  	</div>
			  	
			  	<div class="card-body p-0">
					<table width="100%" cellspacing="0" id="data_tabel" class="table" cellpadding="0">
					  <thead>
						<tr>
						  <th width="6%" height="45" style="text-align: center;">No</th>
						  <th width="17%" align="left" style="text-align: left;">Judul</th>
						  <th width="37%" align="left">Isi Konten</th>
						  <th width="19%" align="center" style="text-align: center;">Tanggal</th>
						  <th width="10%" style="text-align: center;">#</th>
					    </tr>
					  </thead>
					  <tbody>
						<?php

							$per_page = 10;

							if($this->uri->segment(4) != ""){
								$page = ($this->uri->segment(4));
								$poss = (($page-1) * $per_page);
							} else {
								$page = 1;
								$poss = 0;
							}

							$no = $poss;
							foreach($onLaporan as $data) {
								$no++;
								
						?>
						<tr>
						  <td height="28" style="text-align: center;"><?php echo $no; ?></td>
						  <td align="left" style="text-align: left;"><?php echo $data->title; ?></td>
						  <td align="left"><?php echo $data->content; ?></td>
						  <td align="center"><?php echo date('d-m-Y', strtotime($data->created_at)); ?></td>
						  <td style="text-align: center;">
						  	 <?php if($hak_akses->role_delete == 1): ?>
								 <a href="javascript:void(0)" onClick="hapus(<?php echo $data->id; ?>)" title="Hapus">
									[ <i class="fa fa-trash fa-lg"></i> ]
								 </a>
							 <?php endif; ?>
						  	 <?php if($hak_akses->role_delete != 1): ?>
                                <a href="javascript:void(0)" title="Akses terbatas!" onClick="alert('Akses Terbatas!')"> 
                                    [ <i class="fa fa-exclamation-circle"></i> ]
                                </a>
                            <?php endif; ?>
						  </td>
					    </tr>
					    <?php
							}
						?>
					  </tbody>
					</table>
				</div>
				<div class="card-footer clearfix">
                	
                	<?php echo $links; ?>
                	
                	<div class="m-0 float-right" style="padding-top: 2px; padding-right: 10px;">
                		<?php echo $pagination_info; ?>
					</div>
              	</div>
			</div>
	  	</div>
	</section>
</div>

<div id="dialog-konfirmasi" title="Tambah Info">
	
</div>

<div id="dialog-edit_data" title="Edit">
	
</div>

<script language="javascript">
	
	function hapus(id) {
		if (confirm('Apakah data akan dihapus?')) {
			$.ajax({
				url   	  : '<?php echo base_url('app/info/deleteLaporan'); ?>',
				type  	 : 'POST',
				dataType : 'json',
				data : { 
					"id" : id
				}
			}).done(function(data){
				if(data) {
				  if (data.status == true) {
				  	 location.reload();
				  }
				}
			});
		}
	}
	
	function edit_data(id) {

		$('#dialog-edit_data').html('Memuat Halaman ...');
		$('#dialog-edit_data').load("<?php echo base_url('app/info/edit_data'); ?>?id="+id+"");
		$('#dialog-edit_data').dialog({
			height: 310,
			width: 600,
			modal: true,
			open: function(event, ui) {
			  $(".ui-dialog-titlebar-close", ui.dialog | ui).show();
			},
			buttons: [{
				id    : "close",
				text  : "Tutup",
				click : function(){
					$("#dialog-edit_data").dialog('close');
					location.reload();
				}
			},{
				id    : "simpan",
				text  : "Simpan",
				click : function(){
					$.ajax({
						url   : '<?php echo base_url('app/info/ubah_konfirmasi'); ?>',
						type  : 'POST',
						data  : { 
							"id" : id,
							"title"   : $('#judul_edit').val(),
							"content" : $('#konten_edit').val()
						}
					}).done(function(data){
						if(data) {
							$("#simpan").text("Ubah data . . .");
							$("#dialog-edit_data").dialog('close');
							location.reload();
						}
					});
				}
			}]
		});

	}

	function tambah_data() {

		$('#dialog-konfirmasi').html('Memuat Halaman ...');
		$('#dialog-konfirmasi').load("<?php echo base_url('app/info/tambah_data'); ?>");
		$('#dialog-konfirmasi').dialog({
			height: 310,
			width: 600,
			modal: true,
			open: function(event, ui) {
			  $(".ui-dialog-titlebar-close", ui.dialog | ui).show();
			},
			buttons: [{
				id    : "close",
				text  : "Tutup",
				click : function(){
					$("#dialog-konfirmasi").dialog('close');
					location.reload();
				}
			},{
				id    : "simpan",
				text  : "Simpan",
				click : function(){
					$.ajax({
						url   : '<?php echo base_url('app/info/simpan_konfirmasi'); ?>',
						type  : 'POST',
						data  : { 
							"title"   : $('#judul').val(),
							"content" : $('#konten').val(),
							"user"	  : $('#user').val()
						}
					}).done(function(data){
						if(data) {
							$("#simpan").text("Ubah data . . .");
							$("#dialog-konfirmasi").dialog('close');
							location.reload();
						}
					});
				}
			}]
		});

	}
	
</script>