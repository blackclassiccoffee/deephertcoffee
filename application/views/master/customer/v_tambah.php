<style>
	#mapAddress {
        height: 300px;
        width: 100%;
    }
	
	#infowindow-content .title {
        font-weight: bold;
      }

	#infowindow-content {
		display: none;
	}

	#mapAddress #infowindow-content {
		display: inline;
	}

	.pac-card {
		margin: 10px 10px 0 0;
		border-radius: 2px 0 0 2px;
		box-sizing: border-box;
		-moz-box-sizing: border-box;
		outline: none;
		box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
		background-color: #fff;
		font-family: Roboto;
	}

	#pac-container {
		margin: 12px;
	}
	
	#cari_alamat_customer {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 400px;
		height: 30px;
      }

	
	#title {
        color: #fff;
        background-color: #4d90fe;
        font-size: 15px;
        font-weight: 500;
        padding: 6px 12px;
    }
	
	.pac-container {
        z-index: 10000 !important;
    }
	table#add_data tr td {
		padding: 5px;
		font-family:Tahoma, Verdana, Arial;
		font-size:13px;
	}
</style>
<table id="add_data" width="100%" border="0">
	<input type="hidden" id="onIdp" value="<?php echo $id_perusahaan ?>">
	<tr>
		<td>Kantor</td>
		<td>:</td>
		<td>
			<select id="kantor" class="select2" style="width: 100%;">
				<option value="0">Pilih Kantor</option>
				<?php
					foreach($kantor as $dataKantor) {
						$isUtama = $dataKantor->status_kantor == "Utama" ? "(Utama)" : "";
						echo "<option value='".$dataKantor->id."'>".$dataKantor->nama_kantor." ".$isUtama."</option>";
					}
				?>
			</select>
		</td>
	</tr>
	<tr>
		<td colspan="3">
			<hr>
		</td>
	</tr>
	<tr>
		<td width="29%">Kode</td>
		<td width="2%">:</td>
		<td width="69%">
			<input type="text" placeholder="Kode Customer" id="kode_customer" style="width: 200px;">
		</td>
	</tr>
	<tr>
		<td>Customer/Outlet</td>
		<td>:</td>
		<td>
			<input type="text" placeholder="Nama Customer/Outlet" id="nama_customer" style="width: 100%;">
		</td>
	</tr>
	<tr>
		<td>Pimpinan</td>
		<td>:</td>
		<td>
			<input type="text" placeholder="Pimpinan" id="head_office" style="width: 100%;">
		</td>
	</tr>
	<tr>
		<td>Nomor Telpon</td>
		<td>:</td>
		<td>
			<input type="text" placeholder="Nomor Telpon" id="no_telp" style="width: 100%;">
		</td>
	</tr>
	<tr>
		<td colspan="3">
			<hr>
		</td>
	</tr>
	<tr>
		<td height="79" colspan="3">
			<div class="pac-card" id="pac-card">
			  	<div>
				  <div id="title">
				  		Cari Alamat :
					</div>
			  	</div>
		  	  <div id="pac-container">
					<input type="text" placeholder="Cari Alamat Customer" id="cari_alamat_customer" onFocus="geolocate()" >
			  	</div>
			</div>
			<div id="mapAddress"></div>
		  <div id="infowindow-content">
		  	<img src="" width="16" height="16" id="place-icon">
		  	<span id="place-name"  class="title"></span><br>
		  	<span id="place-address"></span>
			</div>
		</td>
	</tr>
	<tr>
		<td valign="top">Alamat Customer</td>
		<td valign="top">:</td>
		<td>
			<textarea rows="5" id="alamat_customer" style="width: 100%;" placeholder="Alamat Customer"></textarea>
			<div style="line-height: 1; text-align: justify;">
				<small><i>* Apabila pencarian alamat dipeta tidak lengkap anda dapat menambahkan keterangan tambahan pada kolom Alamat Customer diatas. Ex. Nama Ruko, No. Ruko, dll.</i></small>
			</div>
			<input type="hidden" id="lat">
			<input type="hidden" id="lng">
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
</table>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDOwOfwjtgZrAI-uF9Ug_cqaFhlC5mKaEQ&libraries=places&callback=initAutocomplete&language=id"
        async defer></script>
<script>
	
	var placeSearch, autocomplete;
	function initAutocomplete() {
		var map = new google.maps.Map(document.getElementById('mapAddress'), {
			mapTypeId: google.maps.MapTypeId.ROADMAP,
          	center: {lat: -7.008185, lng: 110.036713},
			disableDefaultUI: true,
			zoomControl : true,
			gestureHandling : 'greedy',
          	zoom: 7
        });
		
		map.controls[google.maps.ControlPosition.TOP_RIGHT].push(document.getElementById('pac-card'));
		
		autocomplete = new google.maps.places.Autocomplete(
			(document.getElementById('cari_alamat_customer')),
			{
				types: []
			}
		);
		
		autocomplete.bindTo('bounds', map);

        // Set the data fields to return when the user selects a place.
        autocomplete.setFields(
            ['address_components', 'geometry', 'icon', 'name']);

        var infowindow = new google.maps.InfoWindow();
        var infowindowContent = document.getElementById('infowindow-content');
        infowindow.setContent(infowindowContent);
        var marker = new google.maps.Marker({
          	map: map,
          	anchorPoint: new google.maps.Point(0, -29)
        });

		// When the user selects an address from the dropdown, populate the address
		// fields in the form.
		autocomplete.addListener('place_changed', function() {
          	infowindow.close();
          	marker.setVisible(false);
          	var place = autocomplete.getPlace();
			
          	if (!place.geometry) {
				// User entered the name of a Place that was not suggested and
            	// pressed the Enter key, or the Place Details request failed.
            	window.alert("No details available for input: '" + place.name + "'");
            	return;
          	}
			
			var lat = place.geometry.location.lat();
			var lng = place.geometry.location.lng();

			$('#alamat_customer').val($('#cari_alamat_customer').val());
			$('#lat').val(lat);
			$('#lng').val(lng);

          	// If the place has a geometry, then present it on a map.
          	if (place.geometry.viewport) {
            	map.fitBounds(place.geometry.viewport);
          	} else {
            	map.setCenter(place.geometry.location);
				map.setZoom(17);  // Why 17? Because it looks good.
          	}
			
          	marker.setPosition(place.geometry.location);
          	marker.setVisible(true);

          	var address = '';
          	if (place.address_components) {
            	address = [
              		(place.address_components[0] && place.address_components[0].short_name || ''),
              		(place.address_components[1] && place.address_components[1].short_name || ''),
              		(place.address_components[2] && place.address_components[2].short_name || '')
            	].join(' ');
			}

          	infowindowContent.children['place-icon'].src = place.icon;
          	infowindowContent.children['place-name'].textContent = place.name;
          	infowindowContent.children['place-address'].textContent = address;
          	infowindow.open(map, marker);
        });
	}
	
	function geolocate() {
		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(function(position) {
				var geolocation = {
					lat: position.coords.latitude,
					lng: position.coords.longitude
				};

				var circle = new google.maps.Circle({
					center: geolocation,
					radius: position.coords.accuracy
				});
				autocomplete.setBounds(circle.getBounds());
			});
		}
	}
	
	$(document).ready(function() {
		$(".select2").select2().on('select2:opening', function(e) {
			var dataDrop = $(this).data('select2').$dropdown.find('.select2-dropdown');
			dataDrop.css('z-index', 9999);
		});
		
		$('.datepicker').datetimepicker({
			format:'Y-m-d',
			timepicker:false,
			timepickerScrollbar:true,
			yearStart:1945,
			yearEnd:(new Date().getFullYear()),
			defaultDate:'1945-01-01'
		});
		
		$(".number").inputmask("decimal", {
			radixPoint: ",",
			autoGroup: true,
			groupSeparator: ",",
			groupSize: 3,
			autoUnmask: true
		});
	});
	
</script>
