<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1>Dashboard</h1>
				</div>
			</div>
		</div><!-- /.container-fluid -->
	</section>
	
	<section class="content">
      	<div class="container-fluid">
      		
			<div class="row">
				<div class="col-lg-4 col-6">
				<!-- small box -->
					<div class="small-box bg-danger">
						<div class="inner">
							<h3 class="notif_panic">0</h3>
							<p>Pesan Darurat</p>
						</div>
						<div class="icon">
							<i class="fa fa-exclamation-triangle"></i>
						</div>
						<a href="<?php echo base_url('laporan/tombol_darurat/v_data?status_proved=0'); ?>" class="small-box-footer">Lihat Semua <i class="fa fa-arrow-circle-right"></i></a>
					</div>
				</div>
				<!-- ./col -->
				<div class="col-lg-4 col-6">
					<!-- small box -->
					<div class="small-box bg-success">
						<div class="inner">
							<h3 class="notif_bongkar">0</h3>
							<p>Bongkar/Muat</p>
						</div>
						<div class="icon">
							<i class="ion ion-stats-bars"></i>
						</div>
						<a href="<?php echo base_url('laporan/bongkar_muat/v_data?status_proved=0'); ?>" class="small-box-footer">Lihat Semua <i class="fa fa-arrow-circle-right"></i></a>
					</div>
				</div>
				<!-- ./col -->
				<div class="col-lg-4 col-6">
					<!-- small box -->
					<div class="small-box bg-warning">
						<div class="inner">
							<h3 class="notif_kendala">0</h3>
							<p>Laporan Kendala</p>
						</div>
						<div class="icon">
							<i class="fa fa-file"></i>
						</div>
						<a href="<?php echo base_url('laporan/tombol_darurat/v_data?status_proved=0'); ?>" class="small-box-footer">Lihat Semua <i class="fa fa-arrow-circle-right"></i></a>
					</div>
				</div>
				<!-- ./col -->
			</div>
      		
			<div class="row">
				<div class="col-md-8">
					<div class="card">
						<div class="card-header">
							<h5 class="card-title">Laporan Omset Bulanan</h5>
							<div class="card-tools">
								<div class="btn-group">
									<button type="button" class="btn btn-tool dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
										<i class="fa fa-date"></i> Tahun <span id="in_year_text"><?php echo date('Y') ?></span>
									</button>
									<div class="dropdown-menu dropdown-menu-right" role="menu" x-placement="top-end">
										<?php
											for ($i = date('Y', strtotime('-2 year')); $i <= date('Y'); $i++) {
										?>
												<a onClick="getOmset('<?php echo $i; ?>')" class="dropdown-item">
													<?php echo $i; ?>
												</a>
										<?php
											}
										?>
									</div>
								</div>
							</div>
						</div>
							
						<div class="card-body">
							<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
						</div>
					</div>
				</div>
				
				<div class="col-md-4">
					<div class="card">
						<div class="card-header">
							<h5 class="card-title">Laporan</h5>
							<div class="card-tools">
								<div class="btn-group">
									<button type="button" class="btn btn-tool dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
										<i class="fa fa-calendar-alt"></i>
										<span id="inLap_month_text"><?php echo date('M') ?></span>
										<input type="hidden" id="inLap_month" value="<?php echo date('m') ?>">
									</button>
									<div class="dropdown-menu dropdown-menu-right" role="menu" x-placement="top-end">
										<?php
											$bulan = ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agt', 'Sept', 'Okt', 'Nov', 'Des'];
											for ($i = 0; $i < count($bulan); $i++) {
										?>
												<a onClick="getGraphLaporan('<?php echo $i+1; ?>', document.getElementById('inLap_year').value)" class="dropdown-item">
													<?php echo $bulan[$i]; ?>
												</a>
										<?php
											}
										?>
									</div>
								</div>
								<div class="btn-group">
									<button type="button" class="btn btn-tool dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
										<span id="inLap_year_text"><?php echo date('Y') ?></span>
										<input type="hidden" id="inLap_year" value="<?php echo date('Y') ?>">
									</button>
									<div class="dropdown-menu dropdown-menu-right" role="menu" x-placement="top-end">
										<?php
											for ($i = date('Y', strtotime('-2 year')); $i <= date('Y'); $i++) {
										?>
												<a onClick="getGraphLaporan(document.getElementById('inLap_month').value,'<?php echo $i; ?>')" class="dropdown-item">
													<?php echo $i; ?>
												</a>
										<?php
											}
										?>
									</div>
								</div>
							</div>
						</div>
							
						<div class="card-body">
							<div id="cont_lap" style="min-width: 270px; width: 270px; height: 400px; margin: 0 auto"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
</div>

<script src="http://code.highcharts.com/stock/highstock.js"></script>
<script src="http://code.highcharts.com/modules/no-data-to-display.js"></script>
<script src="<?php echo base_url('assets/highchart/modules/exporting.js') ?>"></script>
<script src="<?php echo base_url('assets/highchart/modules/export-data.js') ?>"></script>
<script type="text/javascript">
	
	getOmset();
	getGraphLaporan();
	
	function getOmset(in_tahun) {
		
		$('#in_year_text').text(in_tahun);
		var tahun = in_tahun == null ? (new Date()).getFullYear() : in_tahun;
		
	    $.ajax({
			url   : '<?php echo base_url('dashboard/getOmsetBulanan'); ?>',
			type  : 'POST',
			async : false,
			data  : {
				tahun : tahun
			},
			dataType: 'JSON'
		}).done(function(data){
			if(data) {
				
				var dataOmset = [];
				var label = ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agt', 'Sept', 'Okt', 'Nov', 'Des'];
				for (var x = 0; x < data.length; x++) {
					var total = data[x].total;
					dataOmset.push({
						y : parseInt(data[x].total),
						//color : getRandomColor()
					});
				}
			    //console.log(dataOmset);
				
				var dataSeries = [{
					name: 'Perolehan Omset',
					data: dataOmset

				}]
				
				Highcharts.chart('container', {
					chart: {
						type: 'column'
					},
					title: {
						text: 'Armada Productivity '+tahun
					},
					subtitle: {
						text: 'TrakTruk'
					},
					credits: {
						enabled: true,
						position: {
							align: 'right',
							x: -10,
							verticalAlign: 'bottom',
							y: -5
						},
						href: "#",
						text: "TrakTruk IG"
					},
					xAxis: {
						categories: label,
						//min: 0,
						//max: 3
					},
					scrollbar: {
						enabled: false
					},
					yAxis: {
						min: 0,
						title: {
							text: 'Omset'
						}
					},
					tooltip: {
						headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
						pointFormat: '<tr><td style="color:{series.color};padding:0">Omset : </td>' +
							'<td style="padding:0"><b>Rp. {point.y:,.0f}</b></td></tr>',
						footerFormat: '</table>',
						shared: true,
						useHTML: true
					},
					plotOptions: {
						column: {
							pointPadding: 0,
							borderWidth: 1
						}
					},
					series: dataSeries
				});
			}
		});
	}
	
	function getGraphLaporan(in_bulan, in_tahun) {
		
		var label = ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agt', 'Sept', 'Okt', 'Nov', 'Des'];
		
		var bulan = in_bulan == null ? (new Date()).getMonth()+1 : in_bulan;
		var tahun = in_tahun == null ? (new Date()).getFullYear() : in_tahun;
		
		$('#inLap_month').val(bulan);
		$('#inLap_month_text').text(label[bulan-1]);
		$('#inLap_year').val(tahun);
		$('#inLap_year_text').text(tahun);
		
		$.ajax({
			url   : '<?php echo base_url('dashboard/getLaporanBulanan'); ?>',
			type  : 'POST',
			async : false,
			data  : {
				bulan : bulan,
				tahun : tahun
			},
			dataType: 'JSON'
		}).done(function(data){
			if(data) {
				
				var dataLaporan = [];
				//var label = ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agt', 'Sept', 'Okt', 'Nov', 'Des'];
				for (var x = 0; x < data.length; x++) {
					var total = data[x].dataTotal.total;
					if (total != 0) {
						dataLaporan.push({
							y : parseInt(total),
							color : Highcharts.getOptions().colors[x],
							name :  data[x].name
						});
					}
				}
				
				//console.log(dataLaporan);
				
				// Create the chart
				Highcharts.setOptions({
					lang: {
						noData: "Tidak ada data di bulan "+label[bulan-1]+'/'+tahun
					}
				});
				Highcharts.chart('cont_lap', {
					chart: {
						type: 'pie'
					},
					title: {
						text: 'Data Laporan'
					},
					subtitle: {
						text: label[bulan-1]+'/'+tahun
					},
					credits: {
						enabled: true,
						position: {
							align: 'right',
							x: -10,
							verticalAlign: 'bottom',
							y: -5
						},
						href: "#",
						text: "TrakTruk IG"
					},
					plotOptions: {
						pie: {
							shadow: false,
							center: ['50%', '50%']
						}
					},
					tooltip: {
						//valueSuffix: '%'
					},
					series: [{
						name: 'Laporan',
						data: dataLaporan,
						size: '60%',
						innerSize: '60%',
						dataLabels: {
							formatter: function () {
								return this.y >= 1 ? this.point.name + " : "+ this.y : null;
							},
							distance : 10
						},
						id: 'laporan'
					}],
					responsive: {
						rules: [{
							condition: {
								maxWidth: 400
							},
							chartOptions: {
								series: [{
									id: 'laporan',
									dataLabels: {
										enabled: true
									}
								}]
							}
						}]
					}
				})
			}
		});
		
	}
	
	function getRandomColor() {
		var letters = '0123456789ABCDEF';
	  	var color = '#';
	  	for (var i = 0; i < 6; i++) {
			color += letters[Math.floor(Math.random() * 16)];
	  	}
	  	return color;
	}

	

</script>