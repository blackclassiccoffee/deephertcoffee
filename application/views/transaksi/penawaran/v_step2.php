<br>
<div class="float-right" style="margin-bottom: 5px;">
	<button class="btn btn-primary btn-sm btn-flat" onClick="addKomoditi()">
		<i class="fa fa-plus-circle"></i> Tambah Komoditi
	</button>
</div>
<table width="100%" id="data_tabel-step" border="0" class="table" cellspacing="0" cellpadding="0">
	<thead>
		<tr>
			<th width="4%" style="text-align: center;">No</th>
			<th width="24%">Komoditi</th>
			<th width="26%">Jenis Jasa</th>
			<th width="8%" style="text-align: right;">Volume</th>
			<th width="10%" style="text-align: left;">Satuan</th>
			<th width="14%" style="text-align: right;">Total Biaya (Rp)</th>
			<th width="14%" style="text-align: center;">#</th>
		</tr>
  	</thead>
  	<tbody id="dTableKomoditi">
		
		<tr id="dTableKomoditiNoData">
			<td colspan="7" align="center">
				<div style="margin-top: 10px;">
					Belum ada data!<br>
					<a href="javascript:void(0)" onClick="addKomoditi()">
						[ <i class="fa fa-plus-circle"></i> Tambah Komoditi ]
					</a>
				</div>
			</td>
		</tr>
  	</tbody>
</table>
<hr>
