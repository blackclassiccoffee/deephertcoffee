<style type="text/css">

	table#t_form tr td {
		padding: 5px;
		font-family:Tahoma, Verdana, Arial;
		font-size:12.5px;
	}
	
	table#t_form input:read-only, table#t_form textarea:read-only {
		cursor: not-allowed;
		color: #4D4D4D;
		
	}
	
	table#data_tabel tr td, table#data_tabel-step tr td {
		padding: 5px;
		font-family:Tahoma, Verdana, Arial;
		font-size:11px;
	}
	
	table#data_tabel tr th, table#data_tabel-step tr th {
		font-family:Tahoma, Verdana, Arial;
		font-size:12px;
		padding: 5px;
		vertical-align: middle;
	}
	#parent {
		height: 300px;
	}

	#data_tabel {
		width: 1200px !important;
	}
	
	.title {
		font-size: 15px;
		padding: 10px 10px 10px 20px;
		margin: 3px;
		background: #D5DB87;
	}
	
</style>

<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
		  		<div class="col-sm-6">
					<h1>Buat Penawaran</h1>
		  		</div>
		  		<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="#">Transaksi</a></li>
						<li class="breadcrumb-item">
							<a href="<?php echo base_url(); ?>transaksi/penawaran/v_data">
								Panawaran Order Lab
							</a>
						</li>
						<li class="breadcrumb-item active">Buat Panawaran</li>
					</ol>
         		</div>
			</div>
			<hr>
	  	</div><!-- /.container-fluid -->
	</section>
	
	<section class="content">
    	<div class="container-fluid">
        <!-- SELECT2 EXAMPLE -->
			<div class="card card-default">
				<form name="form1" action="" method="get">
					<div class="card-body">
						<div class="row">
							<div class="col-md-12">
								<div class="container">
									<!-- SmartWizard html -->
									<div id="smartwizard">
										<ul>
											<li>
												<a href="#step-1">
													<i class="fa fa-user-tag"></i> Pelanggan<br />
													<small>Informasi data Pelanggan</small>
												</a>
											</li>
											<li>
												<a href="#step-2">
													<i class="fa fa-flask"></i> Jasa/Komoditi<br />
													<small>Informasi data Jasa/Komoditi</small>
												</a>
											</li><li>
												<a href="#step-3">
													<i class="fa fa-money-bill-wave-alt"></i> Biaya<br />
													<small>Informasi Biaya Penawaran</small>
												</a>
											</li><li>
												<a href="#step-4">
													<i class="fa fa-clipboard-check"></i> Konfirmasi<br />
													<small>Konfirmasi data Penawaran</small>
												</a>
											</li>
										</ul>

										<input type="hidden" id="step-pop" value="0">
										<div>
											<div id="step-1" class="">
												<?php $this->load->view('transaksi/penawaran/v_step1') ?>
											</div>
											<div id="step-2" class="">
												<?php $this->load->view('transaksi/penawaran/v_step2') ?>
											</div>
											<div id="step-3" class="">
												3
											</div>
											<div id="step-4" class="">
												x
											</div>
										</div>
									</div>


								</div>
								
							</div>
						</div>
					</div>
					<div class="card-footer clearfix">
						<button id="cancel-btn" class="btn btn-default">
							<i class="fa fa-arrow-left"></i> Batal
						</button>
						<button id="prev-btn" class="btn btn-default">
							<i class="fa fa-arrow-left"></i> Kembali
						</button>
						<button id="next-btn" class="btn btn-primary">
							<i class="fa fa-arrow-right"></i>
						</button>
						<button id="create-btn" class="btn btn-primary">
							<i class="fa fa-save"></i> Simpan
						</button>
						
						<button id="reset-btn" class="btn btn-warning float-right">
							<i class="fa fa-redo-alt"></i> Reset Data
						</button>
					</div>
				</form>
			</div>
			<!-- Filter/Pencarian -->
			
	  	</div>
	</section>
	
</div>
<div id="dialog-lookupPelanggan" title="Lookup Data Pelanggan"></div>
<div id="dialog-addKomoditi" title="Tambah Data Komoditi"></div>

<script language="javascript">
	
	$(document).ready(function(){
		loadDataAfterReload();
		console.log(simpleStorage.get('dataKomoditi'));
		
		/*window.onbeforeunload = function() {
			if ($('#step-pop').val() == "0") {
				return "You have some unsaved changes!!";
			} else {
				$('#step-pop').val(0);
				//return false;
			}
		};*/
		
		// Step show event
		$("#smartwizard").on("showStep", function(e, 
												   anchorObject, 
												   stepNumber, 
												   stepDirection, 
												   stepPosition) {
			//alert("You are on step "+stepNumber+" now");
		   	if(stepPosition === 'first'){
				$("#cancel-btn").show();
				$("#prev-btn").hide();
				$("#next-btn").show();
				$("#create-btn").hide();
		   	} else if (stepPosition === 'final') {
				$("#cancel-btn").hide();
			   	$("#next-btn").addClass('disabled');
				$("#next-btn").hide();
				$("#create-btn").show();
		   	} else {
				$("#cancel-btn").hide();
			   	$("#prev-btn").show();
			   	$("#next-btn").show();
			   	$("#next-btn").removeClass('disabled');
				$("#create-btn").hide();
		    }
		});
		
		// Smart Wizard
		$('#smartwizard').smartWizard({
			selected: 0,
			autoAdjustHeight:false,
			keyNavigation:false,
			theme: 'dots',
			transitionEffect:'fade',
			showStepURLhash: true,
			toolbarSettings: {
				showNextButton: false,
				showPreviousButton: false
			}
		});
		
		$("#cancel-btn").on("click", function() {
			window.location.href = "<?php echo base_url(); ?>transaksi/penawaran/v_data";
			return false;
		});
		
		$("#prev-btn").on("click", function() {
			// Navigate previous
			$('#smartwizard').smartWizard("prev");
			return false;
		});

		$("#next-btn").on("click", function() {
			// Navigate next
			if ($('#pelanggan_id').val() != '') {
				$('#smartwizard').smartWizard("next");
				return false;
			} else {
				Swal.fire({
				  type: 'error',
				  title: 'Oops...',
				  text: 'Data Pelanggan kosong!'
				});
				
				return false;
			}
			//return true;
		});
		
		$("#create-btn").on("click", function() {
			alert('Simpan!');
		});
		
		$("#reset-btn").on("click", function() {
			Swal.fire({
				title: 'Reset data Penawaran?',
			  	text: "Apakah anda yakin akan mereset data Penawaran?",
			  	type: 'warning',
			  	showCancelButton: true,
			  	confirmButtonColor: '#3085d6',
			  	cancelButtonColor: '#d33',
			  	confirmButtonText: 'Ya'
			}).then((result) => {
			  	if (result.value) {
					// Navigate Reset
					$('#smartwizard').smartWizard("reset");
					clearLocalStorage();
					loadDataAfterReload();
					return true;
			  	}
			});
			return false;
		});
		
	});
	
	function loadDataAfterReload() {
		$('#pelanggan_id').val(localStorage.pelanggan_id);
		$('#nama_pelanggan').val(localStorage.nama_pelanggan);
		$('#no_telp').val(localStorage.telpon);
		$('#no_ktp').val(localStorage.ktp);
		$('#email_perusahaan').val(localStorage.email);
		$('#provinsi').val(localStorage.prov_pelanggan);
		$('#kabupaten').val(localStorage.kab_pelanggan);
		$('#alamat').val(localStorage.alamat_pelanggan);
		$('#no_npwp').val(localStorage.npwp);
		$('#npwp_nama_perusahaan').val(localStorage.nama_npwp);
		$('#npwp_provinsi').val(localStorage.prov_npwp);
		$('#npwp_kabupaten').val(localStorage.kab_npwp);
		$('#npwp_alamat').val(localStorage.alamat_npwp);
		$('#nama_kontak').val(localStorage.nama_contact);
		$('#no_telp_kontak').val(localStorage.telp_contact);
		$('#email_kontak').val(localStorage.email_contact);
	}

	function lookupPelanggan() {
		$('#dialog-lookupPelanggan').dialog({
			height: 560,
			width: 840,
			modal: true,
			open: function(event, ui) {
				$(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
				$(".ui-dialog", ui.dialog | ui).css('z-index', 9999);

				$('#dialog-lookupPelanggan').html('Memuat Halaman ...');
				$('#dialog-lookupPelanggan').load('<?php echo base_url('transaksi/penawaran/lookupPelanggan'); ?>');
			},
			buttons: [{
				id    : "close",
				text  : "Tutup",
				click : function() {
					$('#dialog-lookupPelanggan').dialog("close");
				}
			}]
		});
	}
	
	function addKomoditi() {
		$('#dialog-addKomoditi').dialog({
			height: 560,
			width: 890,
			modal: true,
			open: function(event, ui) {
				$(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
				$(".ui-dialog", ui.dialog | ui).css('z-index', 9999);

				$('#dialog-addKomoditi').html('Memuat Halaman ...');
				$('#dialog-addKomoditi').load('<?php echo base_url('transaksi/penawaran/addKomoditi'); ?>');
			},
			buttons: [{
				id    : "save",
				text  : "Tambah Komoditi",
				click : function() {
					addKomoditiToTable();
					$('#dialog-addKomoditi').dialog("close");
				}
			}, {
				id    : "close",
				text  : "Tutup",
				click : function() {
					$('#dialog-addKomoditi').dialog("close");
				}
			}]
		});
	}
	
	var data_arr = [];
	var ii = 0;
	function addKomoditiToTable() {
		
		ii++;
		
		$('#dTableKomoditiNoData').remove();
		
		var tKomoditi = '\
			<tr class="t_row inrow_'+ii+'" row-number="'+ii+'">\
				<td align="center">\
					<span class="inc_no_'+ii+'">'+ii+'</span>\
				</td>\
				<td>AIR BERSIN '+ii+'</td>\
				<td>JASA ANALISA LAB LINGKUNGAN '+ii+'</td>\
				<td align="right">1</td>\
				<td align="left">Liter</td>\
				<td align="right">100.000</td>\
				<td align="center">\
					<a href="javascript:void(0)" data-toggle="tooltip" data-placement="left" title="Lihat Detail Komoditi">\
						[ Lihat Detail ]\
					</a>\
					<a href="javascript:void(0)" onClick="detele_tabel('+ii+')">\
						[ <i class="fa fa-trash"></i> ]\
					</a>\
				</td>\
			</tr>\
		';
		$('#dTableKomoditi').append(tKomoditi);
		
		updateIncreTable();
		$('[data-toggle="tooltip"]').tooltip();
		
		// Its gonna be store data into LocalStorage
		/*var data_x = {
			'ab' : ii,
			'cd' : '12'
		};
		
		data_arr.push(data_x);
		simpleStorage.set('dataKomoditi', data_arr);*/
	}
	
	function detele_tabel(i){
		
		Swal.fire({
			title: 'Hapus data Komoditi?',
			text: "Apakah anda yakin akan menghapus data ini?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Ya'
		}).then((result) => {
			if (result.value) {
				if($('.t_row').length > 0){
					$('.inrow_'+i).remove();
					updateIncreTable();
				}
				
				if($('.t_row').length == 0){
					$("#dTableKomoditi").append('\
						<tr id="dTableKomoditiNoData">\
							<td colspan="7" align="center">\
								<div style="margin-top: 10px;">\
									Belum ada data!<br>\
									<a href="javascript:void(0)" onClick="addKomoditi()">\
										[ <i class="fa fa-plus-circle"></i> Tambah Komoditi ]\
									</a>\
								</div>\
							</td>\
						</tr>\
					');
					$('#totalPembelian').html(0);

					ii = 0;
				}
			}
		});
		
	}
	
	function updateIncreTable() {
		if($('.t_row').length > 0){
			$.each($('[row-number]'), function(index, element) {
				$(this).attr("row-number",(index+1));
				$("[class^=inrow]", $(this)).attr("class", "inrow_"+(index+1));
				$("[class^=inc_no]", $(this)).attr("class", "inc_no_"+(index+1));
				//$("[onClick^=detele_tabel]", $(this)).attr("onClick", "detele_tabel("+(index+1)+")");
				$('span.inc_no_'+(index+1)).html(index+1);
				
				//ii = (index+1);
			});
		}
	}
	
</script>