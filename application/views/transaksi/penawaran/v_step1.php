<br>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tbody>
		<tr>
    	  	<td colspan="4">
				<div class="title">
					<i class="fa fa-list-ul"></i>
					Data Pelanggan/Perusahaan
			  </div>
			</td>
    	  	<td colspan="4">
				<div class="title">
					<i class="fa fa-list-ul"></i>
					Data NPWP
				</div>
			</td>
   	  	</tr>
		<tr>
		  	<td colspan="8">&nbsp;</td>
	  	</tr>
    	<tr>
   	  	  	<td height="365" colspan="4" valign="top">
				<table width="100%" id="t_form" border="0">
					<tr>
					  <td width="4%">&nbsp;</td>
						<td width="36%" height="34" valign="top">
							Nama Pelanggan<br>
							<small><i>Nama Pelanggan/Perusahaan</i></small>
						</td>
						<td width="4%" valign="top">:</td>
						<td width="56%" valign="top">
							<input class="col-9" type="text" readonly id="nama_pelanggan" placeholder="Nama Pelanggan/Perusahaan" value="">
							<input type="hidden" id="pelanggan_id">
					  	  <a href="javascript:void(0)" onClick="lookupPelanggan()">
								[ <i class="fa fa-search"></i> ]
							</a>
						</td>
					</tr>
				  	<tr>
				  	  <td>&nbsp;</td>
						<td height="34">No. Telepon</td>
						<td>:</td>
						<td>
							<input class="col-10" type="text" readonly id="no_telp" placeholder="+62 08xxx" value="">
						</td>
				  	</tr>
				  	<tr>
				  	  <td>&nbsp;</td>
						<td height="34">e-KTP</td>
						<td>:</td>
						<td>
							<input class="col-10 number-string" type="text" readonly id="no_ktp" maxlength="16" placeholder="3324151234560001" value="">
						</td>
				  	</tr>
				  	<tr>
				  	  <td>&nbsp;</td>
						<td height="34">Email</td>
						<td>:</td>
						<td>
							<input class="col-10" type="email" readonly id="email_perusahaan" placeholder="mail@domain.com" value="">
						</td>
				  	</tr>
				  	<tr>
				  	  <td>&nbsp;</td>
						<td height="30">&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
				  	</tr>
				  	<tr>
				  	  <td>&nbsp;</td>
						<td height="34">Provinsi</td>
						<td>:</td>
						<td>
							<input class="col-10" type="text" readonly id="provinsi" placeholder="Nama Provinsi" value="">
						</td>
				  	</tr>
				  	<tr>
				  	  <td>&nbsp;</td>
						<td height="34">Kabupaten/Kota</td>
						<td>:</td>
						<td>
							<input class="col-10" type="text" readonly id="kabupaten" placeholder="Nama Kabupaten/Kota" value="">
						</td>
				  	</tr>
				  	<tr>
				  	  <td valign="top">&nbsp;</td>
						<td height="89" valign="top">Alamat</td>
						<td valign="top">:</td>
						<td valign="top">
							<textarea readonly id="alamat" rows="5" class="col-10"></textarea>
						</td>
				  	</tr>
	      		</table>
			</td>
      		<td colspan="4" valign="top">
				<table width="100%" id="t_form" border="0">
      		  		<tr>
   		  		  	  <td width="4%">&nbsp;</td>
      		    		<td width="33%" height="34">NPWP</td>
      		    		<td width="3%">:</td>
   		    		  	<td width="60%">
							<input class="col-10" type="text" readonly id="no_npwp" placeholder="012345660019000" value="">
					  	</td>
   		      		</tr>
				  	<tr>
			  	  	  <td>&nbsp;</td>
						<td height="34">Nama Perusahaan</td>
						<td>:</td>
					  	<td>
							<input class="col-10" type="text" readonly id="npwp_nama_perusahaan" placeholder="Nama Perusahaan" value="">
					  	</td>
				  	</tr>
				  	<tr>
			  	  	  <td>&nbsp;</td>
						<td height="34">Provinsi</td>
						<td>:</td>
					  	<td>
							<input class="col-10" type="text" readonly id="npwp_provinsi" placeholder="Provinsi" value="">
					  	</td>
				  	</tr>
				  	<tr>
				  	  <td>&nbsp;</td>
						<td height="34">Kabupaten/Kota</td>
						<td>:</td>
					  	<td>
							<input class="col-10" type="text" readonly id="npwp_kabupaten" placeholder="Kabupaten/Kota" value="">
					  	</td>
				  	</tr>
				  	<tr>
			  	  	  <td>&nbsp;</td>
						<td height="34" valign="top">Alamat</td>
						<td valign="top">:</td>
					  	<td valign="top">
							<textarea readonly id="npwp_alamat" rows="5" class="col-10"></textarea>
					  	</td>
				  	</tr>
    	  	  </table>
			</td>
   		</tr>
    	<tr>
    	  	<td height="18" colspan="4">
				<div class="title">
					<i class="fa fa-list-ul"></i>
					Data Kontak
				Personal</div>
			</td>
    	  	<td width="20%">&nbsp;</td>
    	  	<td width="10%">&nbsp;</td>
    	  	<td width="10%">&nbsp;</td>
    	  	<td width="13%">&nbsp;</td>
  	  	</tr>
    	<tr>
    	  	<td height="141" colspan="4" valign="top">
				<table width="100%" id="t_form" border="0">
					<tr>
      		  		  	<td height="17" colspan="4"></td>
   		    		</tr>
      		  		<tr>
      		  		  	<td width="4%">&nbsp;</td>
      		    		<td width="36%" height="34">Nama </td>
      		    		<td width="4%">:</td>
   		    		  	<td width="56%">
							<input class="col-10" type="text" readonly id="nama_kontak" placeholder="Nama Kontak" value="">
					  	</td>
   		      		</tr>
				  	<tr>
				  	  	<td>&nbsp;</td>
						<td height="34">No. Telepon</td>
						<td>:</td>
					  	<td>
							<input class="col-10" type="text" readonly id="no_telp_kontak" placeholder="+62 08xx" value="">
					  	</td>
				  	</tr>
				  	<tr>
				  	  	<td>&nbsp;</td>
						<td height="34">Email</td>
						<td>:</td>
					  	<td>
							<input class="col-10" type="email" readonly id="email_kontak" placeholder="mail@domain.com" value="">
					  	</td>
				  	</tr>
	    	  	</table>
			</td>
    	  	<td>&nbsp;</td>
    	  	<td>&nbsp;</td>
    	  	<td>&nbsp;</td>
    	  	<td>&nbsp;</td>
  	  </tr>
    	<tr>
    	  <td width="11%">&nbsp;</td>
    	  <td width="20%" height="18">&nbsp;</td>
    	  <td width="6%">&nbsp;</td>
    	  <td width="10%">&nbsp;</td>
    	  <td>&nbsp;</td>
    	  <td>&nbsp;</td>
    	  <td>&nbsp;</td>
    	  <td>&nbsp;</td>
  	  </tr>
  	</tbody>
</table>
