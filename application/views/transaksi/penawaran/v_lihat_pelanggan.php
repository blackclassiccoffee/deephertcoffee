
<style>
	table#theader tr td {
		padding: 5px;
		font-family:Tahoma, Verdana, Arial;
		font-size:12px;
	}
	#WLoad {
		height: 100%;
	}
	.order {
		margin-left: 15px;
		font-weight: bold;
	}
	.orderNumber {
		font-weight: bold;
		color: #4E8AFF;
	}
	.orderDate {
		margin-left: 15px;
	}
	
	.orderHeader {
		font-weight: bold;
		font-size: 15px;
		margin-bottom: 10px;
	}
	
	table.table tr th {
		font-family:Tahoma, Verdana, Arial;
		font-size:12px;
		padding: 5px;
		vertical-align: middle;
	}
	
	table.table tr td {
		font-family:Tahoma, Verdana, Arial;
		font-size:12px;
		padding: 5px;
	}
</style>

<div id="WLoad">
  	<table width="100%" id="theader" border="0" cellspacing="0" cellpadding="0">
  	  	<tbody>
  	    	<tr>
  	      		<td width="51%">
					<div class="orderHeader">
						<i class="fa fa-list"></i>  Daftar Pelanggan
				  </div>
				</td>
  	      		<td width="15%">Pencarian</td>
  	      		<td width="34%" align="right">
					: 
					<input type="text" id="cari" style="width: 200px;" placeholder="Nama Pelanggan/NPWP">
					<button id="lButtonCari">Cari</button>
				</td>
			</tr>
      	</tbody>
  	</table>
	<table width="100%" border="0" class="table" cellspacing="0" cellpadding="0">
	  	<thead>
			<tr>
				<th width="6%" height="48" style="text-align: center;">No.</th>
				<th width="40%" style="text-align: left;">Nama Pelanggan</th>
				<th width="27%" style="text-align: left;">NPWP</th>
				<th width="27%" style="text-align: left;">No. Telepon</th>
			</tr>
		</thead>
		<tbody id="dData">
			
		</tbody>
  	</table>
  	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	  	<tbody>
	    	<tr>
	      		<td width="50%">
					<div id="pagination_info"></div>
				</td>
	      		<td width="50%" align="right">
					<div id="pagination_link"></div>
				</td>
			</tr>
    	</tbody>
  	</table>
</div>

<script>

	$(document).ready(function(){
		
		var onLink = "<?php echo base_url('transaksi/penawaran/getDataPelanggan'); ?>";
		getDataPelanggan(onLink, "");
		
		$('#lButtonCari').on('click', function(){
			getDataPelanggan(onLink, $('#cari').val());
		});
		
		$('#pagination_link').on('click', 'a', function(e){
			e.preventDefault(); 
			var addressValue = $(this).attr("href");
			getDataPelanggan(addressValue, "");
		});
		
	});
	
	function fillData(data1) {
		
		$.ajax({
		  	url   : "<?php echo base_url('transaksi/penawaran/getDataDetailPelanggan'); ?>",
		  	type  : 'GET',
		  	dataType : "json",
		  	data  : {
				"pelanggan_id" : data1
		  	}
		}).done(function(data){
			if(data) {
				
				$('#pelanggan_id').val(data.id);
				localStorage.pelanggan_id = data.id;
				
				$('#nama_pelanggan').val(data.nama_pelanggan);
				localStorage.nama_pelanggan = data.nama_pelanggan;
				
				$('#no_telp').val(data.telpon);
				localStorage.telpon = data.telpon;
				
				$('#no_ktp').val(data.ktp);
				localStorage.ktp = data.ktp;
				
				$('#email_perusahaan').val(data.email);
				localStorage.email = data.email;
				
				$('#provinsi').val(data.prov_pelanggan);
				localStorage.prov_pelanggan = data.prov_pelanggan;
				
				$('#kabupaten').val(data.kab_pelanggan);
				localStorage.kab_pelanggan = data.kab_pelanggan;
				
				$('#alamat').val(data.alamat_pelanggan);
				localStorage.alamat_pelanggan = data.alamat_pelanggan;
				
				$('#no_npwp').val(data.npwp);
				localStorage.npwp = data.npwp;
				
				$('#npwp_nama_perusahaan').val(data.nama_npwp);
				localStorage.nama_npwp = data.nama_npwp;
				
				$('#npwp_provinsi').val(data.prov_npwp);
				localStorage.prov_npwp = data.prov_npwp;
				
				$('#npwp_kabupaten').val(data.kab_npwp);
				localStorage.kab_npwp = data.kab_npwp;
				
				$('#npwp_alamat').val(data.alamat_npwp);
				localStorage.alamat_npwp = data.alamat_npwp;
				
				$('#nama_kontak').val(data.nama_contact);
				localStorage.nama_contact = data.nama_contact;
				
				$('#no_telp_kontak').val(data.telp_contact);
				localStorage.telp_contact = data.telp_contact;
				
				$('#email_kontak').val(data.email_contact);
				localStorage.email_contact = data.email_contact;
			}
		});
		
		$('#step-pop').val(1);
		$('#dialog-lookupPelanggan').dialog("close");
		
		//$('#nama_pelanggan').focus();
	}
	
	function getDataPelanggan(link, cariData) {
		
		$('#WLoad').Wload({
			text : "Loading"
		});
		
		$.ajax({
		  	url   : link,
		  	type  : 'GET',
		  	dataType : "json",
		  	data  : {
				"cari" : cariData
		  	}
		}).done(function(data){
			if(data) {
				
				var tData = '';
				var no = data.onRow;
				for (var i = 0; i<data.onLaporan.length; i++) {
					
					no++;
					
					tData += '\
					<tr>\
						<td align="center">'+no+'</td>\
						<td align="left">\
							<a href="javascipt:void(0)" style="color: #3959DE;" onClick="fillData(\''+data.onLaporan[i].id+'\')">\
								'+data.onLaporan[i].nama_pelanggan+'\
							</a>\
						</td>\
						<td>'+data.onLaporan[i].npwp+'</td>\
						<td align="left">\
							'+data.onLaporan[i].telpon+'\
						</td>\
					</tr>';
					
				}
				
				$('#dData').html(tData);
				$('#pagination_info').html(data.onPagination.pagination_info);
				$('#pagination_link').html(data.onPagination.links);
				$('#WLoad').Wload('hide',{time:1000});
				
			}
		});
	}

</script>