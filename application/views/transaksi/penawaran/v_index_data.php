<style type="text/css">

	table#data_tabel tr td {
		padding: 5px;
		font-family:Tahoma, Verdana, Arial;
		font-size:11px;
	}

	table#data_tabel tr td.verifikasi {
		color: #FFFFFF;
	}
	
	table#data_tabel tr td.verifikasi a {
		color: #FFFFFF;
	}
	
	table#data_tabel tr td.pending {
		color: #000000;
	}
	
	table#data_tabel tr th {
		font-family:Tahoma, Verdana, Arial;
		font-size:12px;
		padding: 5px;
		vertical-align: middle;
	}
	#parent {
		height: 300px;
	}

	#data_tabel {
		width: 1200px !important;
	}
	
</style>

<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
		  		<div class="col-sm-6">
					<h1>Penawaran Order Lab</h1>
		  		</div>
		  		<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="#">Transaksi</a></li>
						<li class="breadcrumb-item active">Panawaran Order Lab</li>
					</ol>
         		</div>
			</div>
			<hr>
	  	</div><!-- /.container-fluid -->
	</section>
	
	<section class="content">
    	<div class="container-fluid">
        <!-- SELECT2 EXAMPLE -->
			<div class="card card-default">
				<form name="form1" action="" method="get">
				<div class="card-header">
					<h3 class="card-title">
						Filter/Pencarian
					</h3>

					<div class="card-tools">
					  <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
					</div>
			  	</div>
			  	<div class="card-body">
            		<div class="row">
              			<div class="col-md-12">
              				<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
								  	<td width="17%"></td>
								  	<td width="1%"></td>
								  	<td width="82%"></td>
								</tr>
								<tr>
								  	<td height="35">Nomor Penawaran</td>
								  	<td>:</td>
								  	<td>
								  		<input class="col-3 input-sm" type="text" name="no_penawaran_src" placeholder="No. Penawaran" value="<?php echo isset($_GET['no_penawaran_src']) && $_GET['no_penawaran_src'] != "" ? $_GET['no_penawaran_src'] : ""; ?>">
								  	</td>
							  	</tr>
								<tr>
								  	<td height="35">Pelanggan</td>
								  	<td>:</td>
								  	<td>
								  		<input class="col-3 input-sm" type="text" name="pelanggan_src" placeholder="Nama Pelanggan" value="<?php echo isset($_GET['pelanggan_src']) && $_GET['pelanggan_src'] != "" ? $_GET['pelanggan_src'] : ""; ?>">
								  	</td>
							  	</tr>
								<tr>
								  	<td height="35">Tanggal Penawaran</td>
								  	<td>:</td>
								  	<td>
										<div class="row" style="padding-left: 8px;">
											<input class="col-2 datepicker" type="text" name="tanggal_awal" placeholder="Tanggal" value="<?php echo isset($_GET['tanggal_awal']) && $_GET['tanggal_awal'] != "" ? $_GET['tanggal_awal'] : ""; ?>">
											s/d
											<input class="col-2 datepicker" type="text" name="tanggal_akhir" placeholder="Tanggal" value="<?php echo isset($_GET['tanggal_akhir']) && $_GET['tanggal_akhir'] != "" ? $_GET['tanggal_akhir'] : ""; ?>">
										</div>
								  	
								  	</td>
							  	</tr>
								<tr>
								  	<td></td>
								  	<td></td>
								  	<td></td>
								</tr>
							</table>
						</div>
					</div>
				</div>
				<div class="card-footer clearfix">
					<input type="submit" name="cari" id="cari" class="btn btn-primary" value="Submit" />
				</div>
				</form>
			</div>
			<!-- Filter/Pencarian -->
			
			<div class="card card-default">
				<div class="card-header">
					<h3 class="card-title">
						Data Penawaran
					</h3>

					<div class="card-tools">
                    	<?php if($hak_akses->role_create == 1): ?>
                          <a href="<?php echo base_url(); ?>transaksi/penawaran/create" class="btn">
                            <i class="fa fa-plus"></i> Buat Penawaran
                          </a>
                      	<?php endif; ?>
					</div>
			  	</div>
			  	
			  	<div class="card-body p-0">
					<table width="100%" cellspacing="0" id="data_tabel" class="table" cellpadding="0">
					  <thead>
						<tr>
						  <th width="2%" height="45" style="text-align: center;">No</th>
						  <th width="13%" style="text-align: center;">Nomor Penawaran</th>
						  <th width="32%" align="left">Pelanggan</th>
						  <th width="13%" align="left">Sales</th>
						  <th width="9%" align="left">Lab</th>
						  <th width="12%" style="text-align: center;">Tanggal Penawaran</th>
						  <th width="10%" style="text-align: center;">Status</th>
						  <th width="4%" style="text-align: center;">#</th>
						  <th width="5%" style="text-align: center;">#</th>
						</tr>
					  </thead>
					  <tbody>
						<?php

							$per_page = 10;

							if($this->uri->segment(4) != ""){
								$page = ($this->uri->segment(4));
								$poss = (($page-1) * $per_page);
							} else {
								$page = 1;
								$poss = 0;
							}

							$no = $poss;
							foreach($onLaporan as $data) {  
								$no++;
						?>
						<tr bgcolor="">
						  	<td align="center" >
								<?php echo $no; ?>
						  	</td>
						  	<td align="center"><?php echo $data->no_penawaran; ?></td>
						  	<td ><?php echo $data->nama_pelanggan; ?></td>
						  	<td ><?php echo $data->nama_karyawan; ?></td>
						  	<td ><?php echo $data->nama_lab; ?></td>
						  	<td align="center" >
								<?php echo date('d/m/Y H:i', strtotime($data->tanggal_penawaran)); ?>
							</td>
						  	<td align="center" >
								<?php
									if ($data->status_order == 1) {
										echo "
											<span class='badge bg-success'>
												Berhasil
											</span>
										";
									} else {
										echo "
											<span class='badge bg-danger'>
												Tidak Berhasil
											</span>
										";
									}
								?>
							</td>
						  	<td align="center" >
								<?php if($hak_akses->role_read == 1): ?>
								<a href="javascript:void(0)" onClick="" title="Lihat Lokasi di Peta">
									[ <i class="fa fa-map"></i> ]
								</a>
								<?php endif; ?>
								<?php if($hak_akses->role_read != 1 && $hak_akses->role_read != 1): ?>
									[ <a href="javascript:void(0)" title="Akses terbatas!" onClick="alert('Akses Terbatas!')"> <i class="fa fa-exclamation-circle"></i> </a> ]
								<?php endif; ?>
						  	</td>
						  	<td align="center" >
								<?php if($hak_akses->role_delete == 1): ?>
									<a href="javascript:void(0)" onClick="hapus(<?php echo $data->id; ?>)" title="Hapus">
										[ <i class="fa fa-trash"></i> ]
									</a>
								<?php endif; ?>
								<?php if($hak_akses->role_delete != 1): ?>
									<a href="javascript:void(0)" title="Akses terbatas!" onClick="alert('Akses Terbatas!')"> 
										[ <i class="fa fa-exclamation-circle"></i> ]
									</a>
								<?php endif; ?>
                          	</td>
						</tr>
						<?php
							}
						?>
					  	</tbody>
					</table>
				</div>
				<div class="card-footer clearfix">
               		<div class="m-0 float-left">
                  		<span class="badge bg-success">Terverifikasi</span>
                  		<span class="badge bg-warning">Pending</span>
                	</div>
                	
                	<?php echo $links; ?>
                	
                	<div class="m-0 float-right" style="padding-top: 2px; padding-right: 10px;">
                		<?php echo $pagination_info; ?>
					</div>
              	</div>
			</div>
			
	  	</div>
	</section>
	
</div>

<div id="dialog-lihatPeta" title="Lihat Peta">
	
</div>

<div id="dialog-konfirmasi" title="Konfirmasi Laporan">
	
</div>

<div id="dialog-lihat_data" title="Lihat Laporan">
	
</div>

<?php

if(isset($_GET['cari'])) {
	//echo "<script>$(function(){ get_supplier('".$_GET['kantor']."', '".$_GET['pelapor']."'); });</script>";
}

?>

<script language="javascript">
	
	$(document).ready(function(){
		clearLocalStorage();
		
		$('#reset').click(function(){
			$('input[type=text]').val('');
			$('select[name=jenis_laporan]').val('');
		});
		
		$("#data_tabel").tableHeadFixer({"right" : 2}); 
	});
	
	function hapus(id) {
		if (confirm('Apakah data akan dihapus?')) {
			$.ajax({
				url   	  : '<?php echo base_url('laporan/lapor_kendala/deleteLaporan'); ?>',
				type  	 : 'POST',
				dataType : 'json',
				data : { 
					"id" : id
				}
			}).done(function(data){
				if(data) {
				  if (data.status == true) {
				  	 location.reload();
				  }
				}
			});
		}
	}

	function lihatPeta(Lat, Lng) {

		$('#dialog-lihatPeta').html('Memuat Halaman ...');
		$('#dialog-lihatPeta').load("<?php echo base_url('laporan/lapor_kendala/lihat_peta'); ?>?lat="+Lat+"&lng="+Lng+"");
		$('#dialog-lihatPeta').dialog({
			height: 460,
			width: 600,
			modal: true,
			open: function(event, ui) {
			  $(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
			},
			buttons: [{
				id    : "close",
				text  : "Tutup",
				click : function(){
					location.reload();
				}
			}]
		});

	}
	
	function lihat_data(id) {

		$('#dialog-lihat_data').html('Memuat Halaman ...');
		$('#dialog-lihat_data').load("<?php echo base_url('laporan/lapor_kendala/lihat_data'); ?>?id="+id+"");
		$('#dialog-lihat_data').dialog({
			height: 460,
			width: 800,
			modal: true,
			open: function(event, ui) {
			  $(".ui-dialog-titlebar-close", ui.dialog | ui).show();
			},
			buttons: [{
				id    : "close",
				text  : "Tutup",
				click : function(){
					$("#dialog-lihat_data").dialog('close');
							location.reload();
				}
			}]
		});
	}

	function konfirmasi(id, id_user) {

		$('#dialog-konfirmasi').html('Memuat Halaman ...');
		$('#dialog-konfirmasi').load("<?php echo base_url('laporan/lapor_kendala/konfirmasi'); ?>?id="+id+"");
		$('#dialog-konfirmasi').dialog({
			height: 460,
			width: 800,
			modal: true,
			open: function(event, ui) {
			  $(".ui-dialog-titlebar-close", ui.dialog | ui).show();
			},
			buttons: [{
				id    : "close",
				text  : "Tutup",
				click : function(){
					$("#dialog-konfirmasi").dialog('close');
							location.reload();
				}
			},{
				id    : "simpan",
				text  : "Simpan",
				click : function(){
					$.ajax({
						url   : '<?php echo base_url('laporan/lapor_kendala/simpan_konfirmasi'); ?>',
						type  : 'POST',
						data  : { 
							"id" 		 : id,
							"id_user" 	 : id_user,
							"keterangan" : $('#konfirmasi_ket').val()
						}
					}).done(function(data){
						if(data) {
							$("#simpan").text("Ubah data . . .");
							$("#dialog-konfirmasi").dialog('close');
							location.reload();
						}
					});
				}
			}]
		});

}
	
</script>