<style type="text/css">

	table#data_tabel_add tr td {
		padding: 5px;
		font-family:Tahoma, Verdana, Arial;
		font-size:13px;
	}
	
	table#data_tabel_add tr td input {
		height: 28px;
		vertical-align: middle;
	}
	
	table#data_tabel tr td {
		padding: 5px;
		font-family:Tahoma, Verdana, Arial;
		font-size:11px;
	}

	table#data_tabel tr td.verifikasi {
		color: #FFFFFF;
	}
	
	table#data_tabel tr td.verifikasi a {
		color: #FFFFFF;
	}
	
	table#data_tabel tr td.pending {
		color: #000000;
	}
	
	table#data_tabel tr th {
		font-family:Tahoma, Verdana, Arial;
		font-size:12px;
		padding: 5px;
		vertical-align: middle;
		background: #E9E9E9;
	}
	
	#parent {
		height: 300px;
	}

	#data_tabel {
		width: 100% !important;
	}
	
</style>

<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
		  		<div class="col-sm-6">
					<h1>Pengiriman Barang</h1>
		  		</div>
		  		<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="#">Dashboard</a></li>
						<li class="breadcrumb-item"><a href="#">Transaksi</a></li>
						<li class="breadcrumb-item active">Pengiriman Barang</li>
					</ol>
         		</div>
			</div>
			<hr>
	  	</div><!-- /.container-fluid -->
	</section>
	
	<section class="content">
    	<div class="container-fluid">
        <!-- SELECT2 EXAMPLE -->
			<div class="card card-default">
				<form name="form1" action="" method="get">
			  	<div class="card-body">
            		<div class="row">
              			<div class="col-md-12">
						  	<table width="100%" id="data_tabel_add" border="0">
							  	<tbody>
									<tr>
									  	<td width="20%">Kantor</td>
									  	<td width="2%">:</td>
									  	<td width="28%">
											<select class="form-control select2 col-9 input-sm" name="kantor" id="kantor">
                                          	  	<option value="">Pilih Kantor</option>
												<?php
													foreach($kantor as $dataKantor) {
														$sel = $dataKantor->id == $id_kantor ? "selected" : "";
														$isUtama = $dataKantor->status_kantor == "Utama" ? "(Utama)" : "";
														echo "<option value='".$dataKantor->id."' $sel>".$dataKantor->nama_kantor." ".$isUtama."</option>";
													}
												?>
										  	</select>
										</td>
									  	<td width="20%">Kode/Nama Barang</td>
									  	<td width="1%">:</td>
									  	<td width="29%">
											<select class="form-control select2 input-sm" name="barang" id="barang" onChange="get_detail_barang(this.value)" style="width: 203px;">
                                          	  	<option value="">Pilih Barang</option>
												<?php
													foreach($barang as $dataBarang) {
														echo "<option value='".$dataBarang['id']."#".$dataBarang['kode_barang']."'>".$dataBarang['kode_barang']." - ".$dataBarang['nama_barang']."</option>";
													}
												?>
										  	</select>
										</td>
									</tr>
									<tr>
									  	<td>Customer/Outlet</td>
									  	<td>:</td>
									  	<td>
											<select class="form-control select2 col-9 input-sm" name="customer" id="customer">
                                          	  	<option value="">Pilih Customer/Outlet</option>
												<?php
													foreach($customer as $dataCustomer) {
														echo "<option value='".$dataCustomer->id."'>".$dataCustomer->kode_customer." - ".$dataCustomer->nama_customer."</option>";
													}
												?>
										  	</select>
										</td>
									  	<td>Jumlah</td>
									  	<td>:</td>
									  	<td>
											<input type="text" class="col-3" name="jumlah" id="jumlah" style="text-align: right;" value="">
											<select class="form-control select2 col-6 input-sm" name="satuan" id="satuan">
                                          	  	<option value="">Pilih Satuan</option>
										  	</select>
										</td>
									</tr>
									<tr>
									  	<td>No. Order</td>
									  	<td>:</td>
									  	<td>
											<input type="text" class="col-9" value="<?php echo $autoOrderNumber; ?>" disabled>
											<input type="hidden" name="no_faktur" id="no_faktur" value="<?php echo $autoOrderNumber; ?>">
										</td>
									  	<td>&nbsp;</td>
									  	<td>&nbsp;</td>
									  	<td>
											<a href="javascript:void(0)" id="btn_add">
												[ Tambah ]
											</a>
										</td>
								  </tr>
									<tr>
									  	<td>Tanggal Transaksi</td>
									  	<td>:</td>
									  	<td>
											<input type="text" class="col-9" value="<?php echo date('d-m-Y'); ?>" disabled>
											<input type="hidden" class="col-9" name="tanggal_trans" id="tanggal_trans" value="<?php echo date('Y-m-d H:i:s'); ?>">
										</td>
									  	<td>&nbsp;</td>
									  	<td>&nbsp;</td>
									  	<td>&nbsp;</td>
								  </tr>
						    	</tbody>
						  	</table>
							<hr>
						  	<table width="100%" id="data_tabel" class="table" cellspacing="0" cellpadding="0">
								<thead>
							    	<tr>
							      		<th width="15%" style="text-align: center;">Kode Barang</th>
							      		<th width="46%">Nama Barang</th>
							      		<th width="20%" style="text-align: right;">Jumlah</th>
							      		<th width="12%">Satuan</th>
							      		<th width="7%" style="text-align: center;">#</th>
						        	</tr>
						    	</thead>
							  	<tbody id="d_barang">
							    	<tr id="d_not">
							      		<td colspan="5" align="center">
											Belum ada Barang
										</td>
						      		</tr>
						    	</tbody>
						  	</table>
							<hr>
						  	<table width="100%" id="data_tabel_add" border="0">
							  	<tbody>
							    	<tr>
							      		<td width="20%">Driver/Sopir</td>
							      		<td width="1%">:</td>
							      		<td width="79%">
											<select class="form-control select2 col-3 input-sm" name="driver" id="driver">
                                          	  	<option value="">Pilih Driver/Sopir</option>
												<?php
													foreach($driver as $dataDriver) {
														echo "<option value='".$dataDriver->id."'>".$dataDriver->Name."</option>";
													}
												?>
										  	</select>
										</td>
						        	</tr>
						    	</tbody>
						  	</table>
                        </div>
					</div>
				</div>
				<div class="card-footer clearfix">
					<input type="button" name="save" id="save" class="btn btn-primary" onClick="simpanData()" value="Simpan" />
				</div>
				</form>
			</div>
			<!-- Filter/Pencarian -->
			
			
	  	</div>
	</section>
</div>

<script language="javascript">
	
	$(document).ready(function() {
		$("#btn_add").on('click', function(){
			addBarangToTable();
		});
	});
	
	no = 0;
	function addBarangToTable() {
		
		var is_oke = true;
		
		if ($("#kantor").val() == "") {
			is_oke = false;
			alert('Kantor belum dipilih!');
		} else if ($("#barang").val() == "") {
			is_oke = false;
			alert('Belum ada barang yang dipilih!');
		} else if ($("#jumlah").val() == "") {
			is_oke = false;
			alert('Jumlah belum dimasukan!');
		} else if ($("#satuan").val() == "") {
			is_oke = false;
			alert('Satuan belum dimasukan!');
		}
		
		if (is_oke) {
			no++;
			ii = no;

			$("#d_not").remove();

			var onBarang = $("#barang option:selected").text().split(' - ');
			// Hitung barang jadi ke pcs/satuan terkecil
			var jumlah_pcs = remove_separator($('#jumlah').val()) * $('#satuan').val().split('#')[3];

			var d_barang = "\
				<tr class='t_row in_row_"+ii+"'>\
					<td align='center'>\
						<input type='hidden' class='id_index' value='"+ii+"'>\
						<input type='hidden' class='id_barang_"+ii+"' value='"+$("#barang").val().split('#')[0]+"'>\
						<input type='hidden' class='kode_barang_"+ii+"' value='"+onBarang[0]+"'>\
						"+onBarang[0]+"\
					</td>\
					<td>\
						"+onBarang[1]+"\
					</td>\
					<td align='right'>\
						<input type='hidden' value='"+jumlah_pcs+"' class='jumlah_barang_pcs_"+ii+"'>\
						<input type='hidden' value='"+$('#jumlah').val()+"' class='jumlah_barang_"+ii+"'>\
						"+$("#jumlah").val()+"\
					</td>\
					<td>\
						<input type='hidden' value='"+$('#satuan').val().split('#')[5]+"' class='id_satuan_"+ii+"'>\
						<input type='hidden' value='"+$('#satuan').val().split('#')[2]+"' class='nama_satuan_"+ii+"'>\
						"+$('#satuan').val().split('#')[2]+"\
					</td>\
					<td align='center'>\
						<a href='javascript:void(0)' onclick=\'detele_tabel("+ii+")\'>\
							[ <i class='fa fa-trash'></i> ]\
						</a>\
					</td>\
				</tr>\
			";

			$("#d_barang").append(d_barang);
			
			$('#barang').select2();
			$('#barang').val('').trigger('change');
			$('#jumlah').val('');
		}
	}
	
	function detele_tabel(i, kb){
		if($('.t_row').length > 0){
			$('.in_row_'+i).remove();
			$.each($('input.id_index'), function(index, element) {
				//hitung_total();
			});
		}
		
		if($('.t_row').length == 0){
			$("#d_barang").append("\
				<tr id='d_not'>\
					<td colspan='5' align='center'>\
						Belum ada Barang\
					</td>\
				</tr>\
			");
		}
	}
	
	function remove_separator(stringValue) {
		stringValue = stringValue.trim();
		var result = stringValue.replace(/[^0-9]/g, '');
		if (/[,\.]\d{2}$/.test(stringValue)) {
			result = result.replace(/(\d{2})$/, '.$1');
		}
		return parseFloat(result);
	}
	
	function get_detail_barang(kode_barang) {
		var onB = kode_barang.split('#');
		
		$.ajax({
		  	url   : '<?php echo base_url('master/master_barang/get_detail_barang_ajax'); ?>',
		  	type  : 'POST',
		  	dataType : "json",
		  	data  : { 
				"kantor" : $('#kantor').val(),
				"kode_barang" : onB[1]
		  	}
		}).done(function(data){
			if(data) {

			   	$('#satuan').html('<option value="">Pilih Satuan</option>')
			   	for(var i = 0; i<data.data_satuan.length; i++){
				   	$('#satuan').append("<option value='"+data.data_satuan[i].kode+"#"+data.data_satuan[i].hpp+"#"+data.data_satuan[i].nama_satuan+"#"+data.data_satuan[i].total_isi+"#"+data.data_satuan[i].id_barang+"#"+data.data_satuan[i].satuan+"'>"+data.data_satuan[i].nama_satuan+"</option>");
			   	}
			}
		});
	}
	
	function simpanData() {
		
		var is_oke = true;
		
		var data_save_arr = [];
		$.each($('input.id_index'), function(index, element) {
			var data = {
				'kode_barang'   : $('.id_barang_'+$(this).val()).val(),
				'jumlah'   		: $('.jumlah_barang_'+$(this).val()).val(),
				'jumlah_pcs'   	: $('.jumlah_barang_pcs_'+$(this).val()).val(),
				'satuan'   		: $('.id_satuan_'+$(this).val()).val()
			};
			data_save_arr.push(data);
		});
		
		if ($('#customer').val() == "") {
			is_oke = false;
			alert('Customer/Outlet belum dipilih!');
		} else if (data_save_arr.length == 0) {
			is_oke = false;
			alert('Belum ada barang yang dimasukan kedalam daftar!');
		} else if ($('#driver').val() == "") {
			is_oke = false;
			alert('Driver belum dipilih!');
		}
		
		if (is_oke) {
			$.ajax({
				'url'      : '<?php echo base_url('transaksi/order/simpan_order'); ?>',
				'data'     : {
					  "kantor"    	  : $('#kantor').val(),
					  "no_faktur"     : $('#no_faktur').val(),
					  "id_driver"     : $('#driver').val(),
					  "id_customer"   : $('#customer').val(),
					  'tanggal_trans' : $('#tanggal_trans').val(),
					  "detail_barang" : data_save_arr
				},
				'dataType' : 'json',
				'type'     : 'POST',
				'success'  : function(data){
				  	if(data){
						alert('Data berhasil disimpan!');
						location.reload(); 
				  	}
				}
			});
		}
		
	}
	
</script>