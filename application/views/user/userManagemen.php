<style type="text/css">

	table#data_tabel tr td {
		padding: 5px;
		font-family:Tahoma, Verdana, Arial;
		font-size:11px;
	}

	table#data_tabel tr td.verifikasi {
		color: #FFFFFF;
	}
	
	table#data_tabel tr td.pending {
		color: #000000;
	}
	
	table#data_tabel tr th {
		font-family:Tahoma, Verdana, Arial;
		font-size:12px;
		padding: 5px;
		vertical-align: middle;
	}
	#parent {
		height: 300px;
	}

	#data_tabel {
		width: 1100px !important;
	}
	
</style>

<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
		  		<div class="col-sm-6">
					<h1>Pengaturan Pengguna</h1>
		  		</div>
		  		<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="#">Pengaturan</a></li>
						<li class="breadcrumb-item active">User/Pengguna</li>
					</ol>
         		</div>
			</div>
			<hr>
	  	</div><!-- /.container-fluid -->
	</section>
	
	<section class="content">
    	<div class="container-fluid">
        <!-- SELECT2 EXAMPLE -->
			<div class="card card-default">
				<form name="form1" action="" method="get">
					<div class="card-header">
						<h3 class="card-title">
							Filter/Pencarian
						</h3>

						<div class="card-tools">
						  <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
						</div>
					</div>
					<div class="card-body">
						<div class="row">
							<div class="col-md-12">
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
									  	<td></td>
									  	<td></td>
									  	<td></td>
									</tr>
									<tr>
										<td height="35">Nama Karyawan</td>
										<td>:</td>
										<td>
											<input type="text" name="karyawan_src" value="<?php echo isset($_GET['karyawan_src']) && $_GET['karyawan_src'] != '' ? $_GET['karyawan_src'] : ''; ?>" class="col-3 input-sm">
										</td>
									</tr>
									<tr>
										<td width="17%" height="35">Jabatan</td>
										<td width="1%">:</td>
										<td width="82%">
											<select class="form-control select2 col-3 input-sm" name="jabatan_src">
												<option value="">Semua Jabatan</option>
												<?php
													foreach ($jabatan as $data) {
														$sel = $data->id == $_GET['jabatan_src'] ? 'selected' : '';
														echo "<option value='".$data->id."' $sel>".$data->PositionName."</option>";
													}
												?>
											</select>
										</td>
									</tr>
									<tr>
										<td width="17%" height="35">Email</td>
										<td width="1%">:</td>
										<td width="82%">
											<input type="text" name="email_src" value="<?php echo isset($_GET['email_src']) && $_GET['email_src'] != '' ? $_GET['email_src'] : ''; ?>" class="col-3 input-sm">
										</td>
									</tr>
									<tr>
										<td width="17%" height="35">Kantor Cabang/UP</td>
										<td width="1%">:</td>
										<td width="82%">
											<select class="form-control select2 col-3 input-sm" onChange="getLab(this.value)" name="kantor_src">
												<option value="">Semua Kantor</option>
												<?php
													foreach ($kantor as $data) {
														$sel = $data->id == $_GET['kantor_src'] ? 'selected' : '';
														echo "<option value='".$data->id."' $sel>".$data->nama_kanca."</option>";
													}
												?>
											</select>
										</td>
									</tr>
									<tr>
										<td width="17%" height="35">Lab</td>
										<td width="1%">:</td>
										<td width="82%">
											<select class="form-control select2 col-3 input-sm" name="lab_src" id="lab_src">
												<option value="">Semua Lab</option>
											</select>
										</td>
									</tr>
								</table>
							
							</div>
						</div>
					</div>
					<div class="card-footer clearfix">
						<input type="submit" name="cari" id="cari" class="btn btn-primary" value="Submit" />
					</div>
				</form>
			</div>
			<!-- Filter/Pencarian -->
			
			<div class="card card-default">
				<div class="card-header">
					<h3 class="card-title">
						Data Pengguna
					</h3>

					<div class="card-tools">
                      <?php if($hak_akses->role_create == 1): ?>
					  <a href="javascript:void(0)" onClick="tambahUser()" class="btn">
					  	<i class="fa fa-plus-circle"></i> Tambah Pengguna
					  </a>
                      <?php endif; ?>
					</div>
			  	</div>
			  	
			  	<div class="card-body p-0">
			  	
					<table width="100%" id="data_tabel" class="table" cellspacing="0" cellpadding="0">
						<thead>
						  <tr>
							<th width="58" height="40">No</th>
							<th width="200">Nama Karyawan</th>
							<th width="201">Jabatan</th>
							<th width="196">Email</th>
							<th width="200">Kantor Cabang/UP</th>
							<th width="185">Lab</th>
							<th width="77" style="text-align: center;">#</th>
							<th width="81" style="text-align: center;">#</th>
						  </tr>
						</thead>
						<tbody>
						<?php
						if(!empty($dataUser)){
							if(isset($_GET['tampil_data'])) {
								$per_page = $_GET['tampil_data'];
							} else {
								$per_page = 10;
							}

							if($this->uri->segment(4) != ""){
								$page = ($this->uri->segment(4));
								$poss = (($page-1) * $per_page);
							} else {
								$page = 1;
								$poss = 0;
							}

							$i = $poss;


							foreach ($dataUser as $data){
								$i++;
								
								$bgColor = '';
						?>
						  <tr bgcolor="<?php echo $bgColor; ?>">
								<td align="center"><?php echo $i; ?></td>
								<td><?php echo $data['Name']; ?></td>
								<td><?php echo $data['PositionName']; ?></td>
								<td><?php echo $data['email']; ?></td>
								<td><?php echo $data['nama_kanca']; ?></td>
								<td>
							  		<?php echo $data['nama_lab']; ?>
							  	</td>
								<td align="center">
									<?php if($hak_akses->role_update == 1): ?>
										[ <a href="javascript:void(0)" title="Ubah data" onClick="ubahUser('<?php echo $data['id']; ?>', '<?php echo $data['lab_id']; ?>')"><i class="fa fa-edit"></i></a> ]
									<?php else: ?>
										[ <a href="javascript:void(0)" title="Akses terbatas!" onClick="alert('Akses Terbatas!')"> <i class="fa fa-exclamation-circle"></i> </a> ]
									<?php endif; ?>
								</td>
								<td align="center">
									<?php if($hak_akses->role_delete == 1): ?>
										[ <a href="javascript:void(0)" title="Hapus" onclick="hapus_data('<?php echo $data['id']; ?>', '<?php echo $data['lab_id']; ?>')"><i class="fa fa-trash"></i></a> ]
									<?php else: ?>
										[ <a href="javascript:void(0)" title="Akses terbatas!" onClick="alert('Akses Terbatas!')"> <i class="fa fa-exclamation-circle"></i> </a> ]
									<?php endif; ?>
								</td>
						  </tr>
						<?php
							}
						} else {

						?>

						  <tr>
							<td colspan="11" align="center">Tidak ada data!</td>
						  </tr>

						<?php

						}
						?>
						</tbody>

					</table>
			  	
				</div>
				<div class="card-footer clearfix">
               		<!--<div class="m-0 float-left">
                  		<span class="badge bg-warning">Inactive</span>
                	</div>-->
                	
                	<?php echo $links; ?>
                	
                	<div class="m-0 float-right" style="padding-top: 2px; padding-right: 10px;">
                		<?php echo $pagination_info; ?>
					</div>
              	</div>
			</div>
		</div>
	</section>
</div>

<div id="dialog-tambah_users" title="Tambah User">
	
</div>

<div id="dialog-ubah_users" title="Ubah User">
	
</div>

<script language="javascript">

	$(document).ready(function() {
		$("#data_tabel").tableHeadFixer({"right" : 2}); 
	});
	
	function hapus_data(id, kantor) {
		if (confirm('Apakah data akan dihapus?')) {
			$.ajax({
				url   : '<?php echo base_url('settings/user/deleteUser'); ?>',
				type  : 'POST',
				dataType : 'json',
				data  : { 
					"id"     : id,
					"kantor" : kantor
				}
			}).done(function(data){
				if(data) {
				  if (data.status == true) {
				  	 location.reload();
				  }
				}
			});
		}
	}
	
	function readImage(inputElement) {
		var deferred = $.Deferred();

		var files = inputElement.get(0).files;
		if (files && files[0]) {
			var fr= new FileReader();
			fr.onload = function(e) {
				deferred.resolve(e.target.result);
			};
			fr.readAsDataURL( files[0] );
		} else {
			deferred.resolve(undefined);
		}

		return deferred.promise();
	}
	
	function getDataRole(view) {
		var inArr = [];
		view.each(function(){
			inArr.push($(this).val());
		});
		
		return inArr;
	}
	
	function ubahUser(id, kantor) {
		$('#dialog-ubah_users').html('Memuat Halaman ...');
		$('#dialog-ubah_users').load("<?php echo base_url('settings/user/changeUser'); ?>/"+id+"");
		$('#dialog-ubah_users').dialog({
			height: 530,
			width: 1000,
			modal: true,
			open: function(event, ui) {
			  	$(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
			  	$(".ui-dialog", ui.dialog | ui).css('z-index', 9999);
				$(event.target).parent().css('position', 'fixed');
				$(event.target).parent().css('top', '50%');
				$(event.target).parent().css('left', '50%');
				$(event.target).parent().css('transform', 'translate(-50%, -50%)');
				
				if ($.ui && $.ui.dialog && $.ui.dialog.prototype._allowInteraction) {
					var ui_dialog_interaction = $.ui.dialog.prototype._allowInteraction;
					$.ui.dialog.prototype._allowInteraction = function(e) {
						if ($(e.target).closest('.select2-dropdown').length) return true;
						return ui_dialog_interaction.apply(this, arguments);
					};
				}
			},
			buttons: [{
				id    : "tombol_ubah",
				text  : "Ubah",
				click : function(){
					
					var inputElement = $("input[name=foto_ubah]");
                    readImage(inputElement).done(function(base64){
                        
                        var foto = "";
                        if (base64 !== 'undefined') {
                            foto = base64;
                        } else {
                            foto = "";
                            console.log("Image Kosong!");
                        }
						
						var onMenu = [];
						var onMenuFull = [];
						$('input[name="menu[]"]:checked').each(function(){
							var inMenu = $(this).val();
							onMenu.push(inMenu);
							onMenuFull.push({
								'idMenu' 	    : inMenu,
								'role_create'   : $('input[class="r_create_'+inMenu+'"]:checked').length,
								'role_read'     : $('input[class="r_read_'+inMenu+'"]:checked').length,
								'role_update'   : $('input[class="r_update_'+inMenu+'"]:checked').length,
								'role_delete'   : $('input[class="r_delete_'+inMenu+'"]:checked').length,
								'role_download' : $('input[class="r_download_'+inMenu+'"]:checked').length
							});
							
						});
						
						var onHideMenu = [];
						$('input[name="hideMenu[]"]').each(function(){
							var inHideMenu = $(this).val();
							onHideMenu.push(inHideMenu);
						});
					
						var data_user = {
							'id'	   	  	  : $('#id').val(),
							'kantor'	   	  : $('#kantor_ubah').val(),
							'Name'		   	  : $('#karyawan_ubah').find(":selected").text(),
							'username'	   	  : $('#username_ubah').val(),
							'password' 	   	  : $('#repassword_ubah').val(),
							'karyawan_id'	  : $('#karyawan_ubah').val(),
							'Picture_lama'	  : $('#foto_lama').val(),
							'Picture'	   	  : foto
						};

						console.log(data_user);
						
						$.ajax({
							url   : '<?php echo base_url('settings/user/updateUser'); ?>',
							type  : 'POST',
							dataType : 'json',
							data  : { 
								"id"   : $('#id').val(),
								"info" : data_user,
								"menu" : onMenu,
								"menuFull" : onMenuFull,
								"hideMenu" : onHideMenu,
								"role_create" : getDataRole($('input[name="role_create[]"]:checked')),
								"hideRole_create" : getDataRole($('input[name="hideRole_create[]"]')),
								"role_read" : getDataRole($('input[name="role_read[]"]:checked')),
								"hideRole_read" : getDataRole($('input[name="hideRole_read[]"]')),
								"role_up" : getDataRole($('input[name="role_up[]"]:checked')),
								"hideRole_up" : getDataRole($('input[name="hideRole_up[]"]')),
								"role_delete" : getDataRole($('input[name="role_delete[]"]:checked')),
								"hideRole_delete" : getDataRole($('input[name="hideRole_delete[]"]')),
								"role_download" : getDataRole($('input[name="role_download[]"]:checked')),
								"hideRole_download" : getDataRole($('input[name="hideRole_download[]"]')),
							}
						}).done(function(data){
							if(data) {
							  	$("#tombol_ubah").text("Simpan data . . .");
							  	location.reload();
							}
						});
						
					});
					
				}
			},{
				id    : "close",
				text  : "Tutup",
				click : function(){
					location.reload();
				}
			}]
		});
	}
	
	function tambahUser() {

		$('#dialog-tambah_users').html('Memuat Halaman ...');
		$('#dialog-tambah_users').load("<?php echo base_url('settings/user/tambah_user'); ?>");
		$('#dialog-tambah_users').dialog({
			height: 530,
			width: 1000,
			modal: true,
			open: function(event, ui) {
			  	$(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
			  	$(".ui-dialog", ui.dialog | ui).css('z-index', 9999);
				$(event.target).parent().css('position', 'fixed');
				$(event.target).parent().css('top', '50%');
				$(event.target).parent().css('left', '50%');
				$(event.target).parent().css('transform', 'translate(-50%, -50%)');
				
				if ($.ui && $.ui.dialog && $.ui.dialog.prototype._allowInteraction) {
					var ui_dialog_interaction = $.ui.dialog.prototype._allowInteraction;
					$.ui.dialog.prototype._allowInteraction = function(e) {
						if ($(e.target).closest('.select2-dropdown').length) return true;
						return ui_dialog_interaction.apply(this, arguments);
					};
				}
			},
			buttons: [{
				id    : "simpan",
				text  : "Simpan",
				click : function(){
										
					var inputElement = $("input[name=foto]");
                    readImage(inputElement).done(function(base64){
                        
                        var foto = "";
                        if (base64 != 'undefined') {
                            foto = base64;
                        } else {
                            foto = "no";
                            console.log("Image Kosong!");
                        }
						
						var onMenu = [];
						$('input[name="menu[]"]:checked').each(function(){
							//console.log($(this).val());
							var inMenu = $(this).val();
							
							onMenu.push({
								'idMenu' 	    : inMenu,
								'role_create'   : $('input[class="r_create_'+inMenu+'"]:checked').length,
								'role_read'     : $('input[class="r_read_'+inMenu+'"]:checked').length,
								'role_update'   : $('input[class="r_update_'+inMenu+'"]:checked').length,
								'role_delete'   : $('input[class="r_delete_'+inMenu+'"]:checked').length,
								'role_download' : $('input[class="r_download_'+inMenu+'"]:checked').length,
							});
							
						});
						
						var data_user = {
							'kantor'	   	  : $('#kantor_add').val(),
							'Name'		   	  : $('#karyawan_add').find(":selected").text(),
							'username'	   	  : $('#username_add').val(),
							'password' 	   	  : $('#repassword_add').val(),
							'karyawan_id'	  : $('#karyawan_add').val(),
							'Picture'	   	  : foto
						};
						
						$.ajax({
							url   : '<?php echo base_url('settings/user/insertUser'); ?>',
							type  : 'POST',
							dataType : 'json',
							data  : { 
								"info" : data_user,
								"menu" : onMenu
							}
						}).done(function(data){
							if(data) {
							  $("#simpan").text("Simpan data . . .");
							  location.reload();
							}
						});
						
					});
					
				}
			},{
				id    : "close",
				text  : "Tutup",
				click : function(){
					location.reload();
				}
			}]
		});

	}
	
	function getLab(the_val){
		
		$.ajax({
			'type'     : "POST",
			'url'      : "<?php echo base_url('settings/user/getLab'); ?>",
			'dataType' : "json",
			'data' 	   : { "kanca_id" : the_val }
		}).done(function(data){
			if(data != ""){
				
				var dataOpt = '<option value="">Semua Lab</option>';
				for (var i = 0; i<data.length; i++) {
					dataOpt += '<option value="'+data[i].id+'">'+data[i].nama_lab+'</option>';
				}
				
				$('#lab_src').html(dataOpt);
			}
		});
	}


</script>