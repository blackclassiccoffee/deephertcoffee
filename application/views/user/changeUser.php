<section class="content-header">
  <h1>
    <i class="fa fa-users"></i> User Management 
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href=""><i class="fa fa-users"></i> User Management</a></li>
    <li class="active"><a href=""><i class="fa fa-users"></i> Ubah User</a></li>
  </ol>
</section>

<section class="content">
  <div class="row">
    <!-- left column -->
    <div class="col-md-4">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-user-plus"></i> Form | Ubah User</h3>
        </div><!-- /.box-header -->
        <!-- form start -->
        <form role="form" id="form-user" action="<?php echo base_url('user/updateUser'); ?>" enctype="multipart/form-data" method="POST">
          <div class="box-body">
            <div class="form-group">
              <label for="exampleInputEmail1">Nama Pengguna</label>
              <input type="hidden" class="form-control" id="id" required="required" name="id" value="<?php echo $idUser; ?>">
              <input type="text" class="form-control" id="nama_user" required="required" name="nama_user" value="<?php echo $namaLengkap; ?>">
            </div>
            <div class="form-group">
              <label for="exampleInputPassword1">Role</label>
              <select name="role" class="form-control select2" required="required">
                <option value="">....</option>
                <?php
                foreach ($roleUser as $data) {
					$sel = $idRole == $data->id ? "selected" : "";
                    echo "<option value='".$data->id."' $sel>".$data->role."</option>";
                }
                ?>
              </select>
            </div>
            <div class="form-group">
              <label for="exampleInputPassword1">Posisi Operasional</label>
              <select name="jabatan" class="form-control select2" required="required">
                <option value="">....</option>
                <?php
                foreach ($dataJabatan as $data) {
					$sel = $idJabatan == $data->id ? "selected" : "";
                	echo "<option value='".$data->id."' $sel>".$data->nama_jabatan."</option>";
                }
                ?>
              </select>
            </div>
            <div class="form-group">
              <label for="exampleInputPassword1">Kantor</label>
              <select name="kantor" class="form-control select2" required="required">
                <option value="">....</option>
                <?php
                foreach ($dataKantor as $data) {
				  $sel = $idToko == $data->id ? "selected" : "";
                  echo "<option value='".$data->id."' $sel>".$data->nama."</option>";
                }
                ?>
              </select>
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Username</label>
              <input type="text" class="form-control" id="username" required="required" onkeyup="checked_username(this.value)" name="username" value="<?php echo $username; ?>">
              <span class="help-block">
                <span style="float: right;" id="status_username">
                	
                </span>
              </span>
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Password Lama</label>
              <input type="password" class="form-control" id="password_lama" name="password_lama" >
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Password</label>
              <input type="password" class="form-control" id="password" name="password" >
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Konfirmasi Password</label>
              <input type="password" class="form-control" id="re_pass" onkeyup="checked_rePass(this.value)" name="re_pass">
              <span class="help-block">
              	<i class="fa fa-question-circle"></i> Ulangi Password diatas
                <span style="float: right;" id="status_pass">
                	
                </span>
              </span>
            </div>
            <div class="form-group">
              <label for="exampleInputFile">Foto</label>
              <input type="file" name="userfile" id="exampleInputFile">
            </div>
          </div><!-- /.box-body -->

          <div class="box-footer">
            <button type="submit" name="save" id="save" class="btn btn-primary">
            	<i class="fa fa-save"></i> Ubah data
            </button>
            <button type="submit" name="save" id="save" class="btn btn-warning">Batal</button>
            <button type="submit" name="save" id="save" class="btn btn-danger">
            	<i class="fa fa-trash"></i> Hapus
            </button>
          </div>
        </form>
      </div>
    </div>

    <div class="col-md-8">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-users"></i> Data User</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
          <table width="100%" class="display dataUser" id="dataUser">
              <thead>
                  <tr>
                      <th width="7%" class="with-checkbox"><input type="checkbox" name="inChecked" id="inChecked"></th>
                      <th width="16%">Nama Pengguna</th>
                      <th width="13%" >Role</th>
                      <th width="13%" >Posisi OP</th>
                      <th width="13%" >Kantor</th>
                      <th width="17%" class='hidden-350'>Username</th>
                      <th width="18%" class='hidden-480'>Last Login</th>
                      <th width="8%" class='hidden-1024'>#</th>
                  </tr>
              </thead>
              <tbody>
              <?php
                foreach ($dataUser as $data) {
				  switch($data->STATUS_ONLINE){
					  case 0 : $status_online = '<i class="fa fa-circle"></i>'; break;
					  case 1 : $status_online = '<i class="fa fa-circle text-green"></i>'; break;
					  case 2 : $status_online = '<i class="fa fa-circle text-yellow"></i>'; break;
				  }
				  
                  echo '<tr>
                          <td align="center"><input type="checkbox" name="check" value="'.$data->ID.'"></td>
                          <td>'.$status_online.' '.$data->namaLengkap.'</td>
                          <td>'.$data->role.'</td>
						  <td>'.$data->nama_jabatan.'</td>
						  <td>'.$data->nama_kantor.'</td>
                          <td>'.$data->USERNAME.'</td>
                          <td align="center">
						  	'.$data->LOG_TIME.'
							<br>
							<small class="label label-primary">
								<i class="fa fa-clock-o"></i> 
								'.$this->master->convert_time($data->LOG_TIME, false).'
							</small>
						  </td>
                          <td align="center">
                            <a href="'.site_url().'/user/changeUser/'.$data->ID.'">
                              <i class="fa fa-cog"></i>
                            </a>'
              ?>
              <?php
              if ($data->ID != "U1") {
                  echo '| <a href="'.site_url().'/user/deleteUser/'.$data->ID.'" onClick="return confirm(\'Are You Sure?\')">
                            <i class="fa fa-trash"></i>
                          </a>';
              }
              ?>
              <?php
                  echo ' | <a href="javascript:void(0)" onclick="changeMenu(\''.$data->ID.'\')">
                              <i class="fa fa-reorder"></i>
                           </a>
                        </td>
                      </tr>';
              }
              ?>
              </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>

<script type="text/javascript">

function changeMenu(id){
    window.open("<?php echo site_url('user/ubah_menu')?>/"+id, "Ubah Menu", "width=500, height=600");
}

function checked_rePass(the_pass){
	if(the_pass != $('#password').val()){
		if(the_pass == ""){
			$('#status_pass').html('');
		} else {
			$('#status_pass').html('<span class="text-red">Password tidak sama!</span>');
			$('#save').attr('disabled','disabled');
		}
	} else if(the_pass == $('#password').val()) {
		$('#status_pass').html('<span class="text-green"><i class="fa fa-check-circle"></i></span>');
		$('#save').removeAttr('disabled');
	}
}

function checked_username(the_val){
	$('#status_username').html('<img src="<?php echo base_url('assets/images/loading/default.gif'); ?>" width="20">');
	$.ajax({
		'type'     : "POST",
		'url'      : "<?php echo base_url('user/checked_username'); ?>",
		'dataType' : "json",
		'data' 	   : { "username" : the_val }
	}).done(function(data){
		if(data != ""){
			$('#status_username').html('<span class="text-red">Username tidak tersedia!</span>');
		} else {
			$('#status_username').html('<span class="text-green"><i class="fa fa-check-circle"></i></span>');
		}
	});
}

$( document ).ready(function() {
  
  $('form input').attr('autocomplete','off');
  
  if($('.dataUser').length > 0){
      var table = $('#dataUser').dataTable({
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        "order": [[ 1, "desc" ]],
        "aoColumnDefs" : [{
            'bSortable' : false,
            'aTargets' : [ 0 ]
        }],
        "pagingType": "simple_numbers",
        "fnInitComplete": function() {
            this.fnAdjustColumnSizing(true);
        },
        scrollX: "100%",
      });
  }

  $("thead tr th").css('text-align', 'center');
  $("thead tr th:eq(1)").css('text-align', 'left');

  $("#inChecked").click(function(e){
      $('input').prop('checked',this.checked);
  });
});
</script>