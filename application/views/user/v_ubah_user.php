<style type="text/css">

	table#add_data tr td {
		padding: 5px;
		font-family:Tahoma, Verdana, Arial;
		font-size:12px;
	}
	
	table#add_data tr th {
		font-family:Tahoma, Verdana, Arial;
		font-size:12px;
		padding: 5px;
		vertical-align: middle;
	}

	#add_data {
		width: 100% !important;
	}
	
	ul, li {
        list-style-type: none;
    }
	.title {
		font-size: 15px;
		padding: 10px 10px 10px 20px;
		background: #D5DB87;
	}
</style>

<table width="100%" border="0" id="add_data" cellspacing="0" cellpadding="0">
	<input type="hidden" name="id" id="id" value="<?php echo $data_user->id; ?>">
	<tr>
	  	<td>
			<div class="title">
				<i class="fa fa-list-ul"></i>
				Data Pengguna
			</div>
		</td>
	  	<td>
			<div class="title">
				<i class="fa fa-list-ul"></i>
				Hak Akses Menu
			</div>
		</td>
  </tr>
	<tr>
	  	<td width="42%" valign="top">
			<table width="98%">
				<tr>
					<td width="32%">Kantor</td>
					<td width="4%">:</td>
					<td width="64%">
						<select name="kantor_ubah" class="select2" onChange="getLab(this.value)" id="kantor_ubah" style="width: 100%;">
							<option value="0">Pilih Kantor</option>
							<?php
								foreach ($kantor as $data) {
									$sel = $data->id == $data_user->kanca_id ? "selected" : "";
									echo "<option value='".$data->id."' ".$sel.">".$data->nama_kanca."</option>";
								}
							?>
						</select>
					</td>
				</tr>
				<tr>
					<td width="32%">Lab</td>
					<td width="4%">:</td>
					<td width="64%">
						<select class="select2" id="lab_ubah" onChange="getKaryawan(this.value)" style="width: 100%;">
							<option value="">Pilih Lab</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>Nama Karyawan</td>
					<td>:</td>
					<td>
						<select class="select2" id="karyawan_ubah" style="width: 100%;">
							<option value="0">Pilih Nama Karyawan</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>Foto</td>
					<td>:</td>
					<td>
						<input type="hidden" name="foto_lama" id="foto_lama" value="<?php echo $data_user->foto; ?>">
      					<input type="file" name="foto_ubah">
					</td>
				</tr>
				<tr>
					<td colspan="3"><hr></td>
				</tr>
				<tr>
					<td>Username</td>
					<td>:</td>
					<td>
						<input type="text" name="username_ubah" id="username_ubah" value="<?php echo $data_user->email; ?>" onKeyUp="checked_username(this.value)" style="width: 100%;">
						<div id="status_username"></div>
					</td>
				</tr>
				<tr>
					<td>Password</td>
					<td>:</td>
					<td>
						<input type="password" name="password_ubah" id="password_ubah" style="width: 100%;">
					</td>
				</tr>
				<tr>
					<td>Ulangi Password</td>
					<td>:</td>
					<td>
						<input type="password" name="repassword_ubah" id="repassword_ubah" onKeyUp="checked_rePass(this.value)" style="width: 100%;">
						<div id="status_pass"></div>
					</td>
				</tr>
			</table>
		</td>
		<td width="58%" valign="top">
			<?php
                echo "<ul>";
                echo $menu;
                echo "</ul>";
            ?>
		</td>
	</tr>
</table>

<script language="javascript">
	
	$(document).ready(function() {
		$(".select2").select2().on('select2:opening', function(e) {
			var dataDrop = $(this).data('select2').$dropdown.find('.select2-dropdown');
			dataDrop.css('z-index', 9999);
		});
		
		getLab('<?php echo $data_user->kanca_id; ?>', '<?php echo $data_user->lab_id; ?>');
		getKaryawan('<?php echo $data_user->lab_id; ?>', '<?php echo $data_user->karyawan_id; ?>');
		
		$('input:checkbox[id^="x"]').change(function(e) {
			var checked = $(this).prop("checked"),
			container = $(this).parent(),
			siblings = container.siblings();

			container.find('input:checkbox[id^="x"]').prop({
				indeterminate: false,
				checked: checked
			});

			function checkSiblings(el) {
				var parent = el.parent().parent(),
				all = true;

				el.siblings().each(function() {
					return all = ($(this).children('input:checkbox[id^="x"]').prop("checked") === checked);
				});

				if (all && checked) {
					parent.children('input:checkbox[id^="x"]').prop({
						indeterminate: false,
						checked: checked
					});
					checkSiblings(parent);
				} else if (all && !checked) {
					parent.children('input:checkbox[id^="x"]').prop("checked", checked);
					parent.children('input:checkbox[id^="x"]').prop("indeterminate", (parent.find('input:checkbox[id^="x"]:checked').length > 0));
					checkSiblings(parent);
				} else {
					el.parents("li").children('input:checkbox[id^="x"]').prop({
						indeterminate: false,
						checked: true
					});
				}
			}

			checkSiblings(container);
		});
	});
	
	function checked_username(the_val){
		
		if (the_val != "") {
		
			$.ajax({
				'type'     : "POST",
				'url'      : "<?php echo base_url('settings/user/checked_username'); ?>",
				'dataType' : "json",
				'data' 	   : { "username" : the_val }
			}).done(function(data){
				if(data != ""){
					$('#status_username').html('<span class="text-red"><i class="fa fa-times-circle"></i> Username tidak tersedia!</span>');
					$('#simpan').attr("disabled", "disabled");
				} else {
					$('#status_username').html('<span class="text-green"><i class="fa fa-check-circle"></i> Username tersedia!</span>');
					$('#simpan').removeAttr('disabled');
				}
			});
			
		} else {
			$('#status_username').html('');
		}
	}
	
	function checked_rePass(the_pass){
		if(the_pass != $('#password_ubah').val()){
			if(the_pass == ""){
				$('#status_pass').html('');
			} else {
				$('#status_pass').html('<span class="text-red"><i class="fa fa-times-circle"></i> Password tidak sama!</span>');
				$('#simpan').attr('disabled','disabled');
			}
		} else if(the_pass == $('#password_ubah').val()) {
			$('#status_pass').html('');
			$('#simpan').removeAttr('disabled');
		}
	}
	
	function getLab(the_val, on_data){
		
		$.ajax({
			'type'     : "POST",
			'url'      : "<?php echo base_url('settings/user/getLab'); ?>",
			'dataType' : "json",
			'data' 	   : { "kanca_id" : the_val }
		}).done(function(data){
			if(data != ""){
				
				var dataOpt = '<option value="">Pilih Lab</option>';
				for (var i = 0; i<data.length; i++) {
					var sel = '';
					if (on_data != '') {
						sel = data[i].id == on_data ? 'selected' : '';
					}
					dataOpt += '<option value="'+data[i].id+'" '+sel+'>'+data[i].nama_lab+'</option>';
				}
				
				$('#lab_ubah').html(dataOpt);
			}
		});
	}
	
	function getKaryawan(the_val, on_data){
		
		$.ajax({
			'type'     : "POST",
			'url'      : "<?php echo base_url('settings/user/getKaryawan'); ?>",
			'dataType' : "json",
			'data' 	   : { "lab_id" : the_val }
		}).done(function(data){
			if(data != ""){
				
				var dataOpt = '<option value="">Pilih Nama Karyawan</option>';
				for (var i = 0; i<data.length; i++) {
					
					var sel = '';
					if (on_data != '') {
						sel = data[i].id == on_data ? 'selected' : '';
					}
					dataOpt += '<option value="'+data[i].id+'" '+sel+'>'+data[i].nama_karyawan+'</option>';
				}
				
				$('#karyawan_ubah').html(dataOpt);
			}
		});
	}
	
</script>
