<style type="text/css">
    ul, li {
        list-style-type: none;
    }
    #message {
        float: right;
    }
	.box-body {
		padding: 10px;
	}
</style>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>
</head>

<body>
	<form name="form1" action="<?php echo site_url();?>/settings/user/save_menu" method="POST">
        <div class="box-body">
            <input type="hidden" value="<?php echo $this->uri->segment('4');?>" name="idUser">
            <?php
                echo "<ul>";
                echo $menu;
                echo "</ul>";
            ?>
            <div class="box-footer">
              <button type="submit" name="save" class="btn btn-primary">Simpan</button>
              <input type="button" onClick="tutup()" class="btn" value="Keluar" name="keluar">
            </div>
        </div>
    </form>
</body>
</html>



<script type="text/javascript">
$('form').submit(function() {
  $(this).find("button[type='submit']").prop('disabled',true);
});
function suksesPesan($msg){
    var unique_id = $.gritter.add({
        title: 'Berhasil!',
        text: '<i class="icon-ok-sign"></i> '+$msg+'!',
        sticky: true,
        class_name: 'my-sticky-class'
    });

    setTimeout(function(){

        $.gritter.remove(unique_id, {
            fade: true,
            speed: 'slow'
        });

    }, 4000)


    return false;
}

function tutup(){
  //opener.location.reload();
  window.close();
}

$(function() {
<?php if ($this->session->flashdata('oke')) {$pesan = $this->session->flashdata('oke');?>
	        suksesPesan('<?php echo $pesan;?>');
<?php }?>

    // Apparently click is better chan change? Cuz IE?
    $('input:checkbox[id^="x"]').change(function(e) {
      var checked = $(this).prop("checked"),
          container = $(this).parent(),
          siblings = container.siblings();

      container.find('input:checkbox[id^="x"]').prop({
          indeterminate: false,
          checked: checked
      });

      function checkSiblings(el) {
          var parent = el.parent().parent(),
              all = true;

          el.siblings().each(function() {
              return all = ($(this).children('input:checkbox[id^="x"]').prop("checked") === checked);
          });

          if (all && checked) {
              parent.children('input:checkbox[id^="x"]').prop({
                  indeterminate: false,
                  checked: checked
              });
              checkSiblings(parent);
          } else if (all && !checked) {
              parent.children('input:checkbox[id^="x"]').prop("checked", checked);
              parent.children('input:checkbox[id^="x"]').prop("indeterminate", (parent.find('input:checkbox[id^="x"]:checked').length > 0));
              checkSiblings(parent);
          } else {
              el.parents("li").children('input:checkbox[id^="x"]').prop({
                  indeterminate: false,
                  checked: true
              });
          }
        }

        checkSiblings(container);
      });
    });

</script>