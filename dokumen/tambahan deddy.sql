/*
Navicat MySQL Data Transfer

Source Server         : Laragon MySQL
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : superdistri

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2017-01-26 21:48:36
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `master_kode_kunjungan`
-- ----------------------------
DROP TABLE IF EXISTS `master_kode_kunjungan`;
CREATE TABLE `master_kode_kunjungan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of master_kode_kunjungan
-- ----------------------------
INSERT INTO `master_kode_kunjungan` VALUES ('1', 'SENIN');
INSERT INTO `master_kode_kunjungan` VALUES ('2', 'SELASA');
INSERT INTO `master_kode_kunjungan` VALUES ('3', 'RABU');
INSERT INTO `master_kode_kunjungan` VALUES ('4', 'KAMIS');
INSERT INTO `master_kode_kunjungan` VALUES ('5', 'JUMAT');
INSERT INTO `master_kode_kunjungan` VALUES ('6', 'SABTU');
INSERT INTO `master_kode_kunjungan` VALUES ('7', 'MINGGU');

-- ----------------------------
-- Table structure for `master_tipe_customer`
-- ----------------------------
DROP TABLE IF EXISTS `master_tipe_customer`;
CREATE TABLE `master_tipe_customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of master_tipe_customer
-- ----------------------------
INSERT INTO `master_tipe_customer` VALUES ('1', 'tipe 1');
INSERT INTO `master_tipe_customer` VALUES ('2', 'tipe 2');
INSERT INTO `master_tipe_customer` VALUES ('3', 'isi ndewe');

-- ----------------------------
-- Table structure for `t_customer`
-- ----------------------------
DROP TABLE IF EXISTS `t_customer`;
CREATE TABLE `t_customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode_customer` varchar(100) NOT NULL,
  `id_tipe` int(11) DEFAULT NULL,
  `no_npwp` varchar(50) DEFAULT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `alamat` text,
  `no_telp` varchar(25) DEFAULT NULL,
  `id_prov` int(5) NOT NULL,
  `id_kota` varchar(50) DEFAULT NULL,
  `id_kec` int(10) NOT NULL,
  `hutang` double DEFAULT NULL,
  `kantor` int(3) DEFAULT NULL,
  `nama_pic` varchar(100) DEFAULT NULL,
  `no_telp_pic` varchar(100) DEFAULT NULL,
  `plafon_kredit` double DEFAULT NULL,
  `top` varchar(100) DEFAULT NULL,
  `kode_kunjungan` int(10) DEFAULT NULL,
  `namacs_wp` varchar(100) DEFAULT NULL,
  `npwp_wp` varchar(100) DEFAULT NULL,
  `alamat_cs` text,
  `tanggal_pendaftaran` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `status_aktif` int(1) DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1338 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of t_customer
-- ----------------------------
INSERT INTO `t_customer` VALUES ('1337', 'CST-001', '2', 'sadasd', 'asdasdasd', 'sadasd', 'asdasd', '1', '1492', '1672', '123123', '15', 'asdasd', 'sasdasd', '232321', '123123', '1', null, null, null, '2017-01-26 18:24:27', '1', '2017-01-26 16:24:27', null);

-- tambah menu customer
INSERT INTO `t_menu` ('nama_menu', 'link', 'segment', 'parent', 'icon', 'flag_aktif', 'sort') VALUES ('Customer', 'master/customer', 'customer', 128, 'fa fa-users', 1, 3);